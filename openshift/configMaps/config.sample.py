ADMIN_EGROUP = "webcast-team"
APP_PORT = 8080
SECRET_KEY = ""

# Debug and logging configuration
DEBUG = False
IS_LOCAL_INSTALLATION = False
TESTING = False

LOG_LEVEL = "PROD"
WEBAPP_LOGS = "/logs/production/webapp.log"
API_LOGS = "/logs/production/api.log"
CRON_LOGS = "/logs/production/cronjobs.log"
REMOTE_LOGS_URL = ""
REMOTE_LOGS_TYPE = ""
REMOTE_LOGS_PRODUCER = ""

# Whether or not to use the wsgi Proxy Fix
USE_PROXY = True

API_URL_PREFIX = "/api"
ALLOWED_API_IPS = [
    "188.184.36.202",
    "188.184.36.203",
    "188.185.65.83",
    "188.184.93.163",
]
FETCH_UPCOMING_API_KEY = ""

# Oauth config
CERN_OAUTH_CLIENT_ID = ""
CERN_OAUTH_CLIENT_SECRET = ""
CERN_OAUTH_AUTHORIZE_URL = "https://oauth.web.cern.ch/OAuth/Authorize"
CERN_OAUTH_TOKEN_URL = "https://oauth.web.cern.ch/OAuth/Token"


# DB Config
DB_NAME = ""
DB_PASS = ""
DB_PORT = 1234
DB_SERVICE = ""
DB_USER = ""

SQLALCHEMY_POOL_SIZE = 5
SQLALCHEMY_POOL_TIMEOUT = 10
SQLALCHEMY_POOL_RECYCLE = 120
SQLALCHEMY_MAX_OVERFLOW = 3

# Indico API
INDICO_URL = "https://indico.cern.ch"
INDICO_API_KEY = ""
INDICO_API_SECRET_KEY = ""

# WOWZA
WOWZA_ORIGIN_URLS = ["https://wowza01.cern.ch"]
WOWZA_EDGE_URL = "https://wowza.cern.ch"
WOWZA_SMIL_FOLDER = "/eos/media/av-sorenson/website/prod/smil"
WOWZA_USERNAME = ""
WOWZA_PASSWORD = ""

# CDS
CDS_SERVER = "https://cdsweb.cern.ch/record/"
CDS_API_URL = "https://cdsweb.cern.ch/search?p=300__a%3A%22Streaming+video%22+and+collection%3AIndico+and+%28collection%3ATALK+or+collection%3ARestricted_ATLAS_Talks+or+collection%3ARestricted_CMS_Talks%29+AND+8567_x%3Apngthumbnail&f=&action_search=Search&c=CERN+Document+Server&rg=12&sc=0&of=xm&sf=269__c&so=d"

# Images
IMAGES_FOLDER = "images"
PERMANENT_STREAM_IMAGES_FOLDER = "permanent"
EVENTS_IMAGES_FOLDER = "events"
UPLOADED_FILES_DEST = "/eos/media/av-sorenson/website/prod/production/uploads"
UPLOADED_IMAGES_DEST = "/eos/media/av-sorenson/website/prod/uploads/images"
UPLOADED_EVENTS_IMAGES_DEST = (
    "/eos/media/av-sorenson/website/prod/uploads/images/events"
)
TEMP_FILES_DEST = "/opt/app-root/src"

STATIC_FILES_PATH = "/opt/app-root/src/app/static-files"
DEFAULT_IMAGES_FOLDER = "images/default"
DEFAULT_CATEGORIES_FOLDER = "categories"
DEFAULT_EVENTS_FOLDER = "events"

CACHE_ENABLE = False
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_REDIS_PASSWORD = ""
CACHE_OAUTH_TIMEOUT = 7200


#
# Analytics
#
ANALYTICS_PIWIK_ID = ""

#
# Assets
#
BUILD_ASSETS = False
