stages:
  - test
  - build
  - generate_docker_image
  - redeploy

# cache using branch name
# https://gitlab.com/help/ci/caching/index.md
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - app/client/node_modules

test_backend:
  image:
    python:3.8
    # Will run the tests with coverage.
  stage: test
  before_script:
    - mkdir -p /opt/app-root/src/uploads/
    - pip install --upgrade pip
    - pip install -r requirements.txt
    - pip install -r requirements.dev.txt
  script:
    - cp app/config/config.sample.py app/config/config.py
    - pytest

lint_frontend:
  image: node:14.18
  stage: test
  before_script:
    - cd app/client
    - yarn install
  script:
    - yarn lint

test_frontend:
  image: node:14.18
  stage: test
  before_script:
    - cd app/client
    - yarn install
  script:
    - yarn test

build_frontend_test:
  image: node:14.18
  stage: build
  variables:
    REACT_APP_API_ENDPOINT: ${API_ENDPOINT_TEST}
    REACT_APP_IS_DEV_INSTALL: "true"
    REACT_APP_PIWIK_ID: "3722"
  before_script:
    - cd app/client
    - yarn install
  script:
    - yarn build
  artifacts:
    paths:
      - app/client/build # The application is built and packaged
  only:
    - test

build_frontend_qa:
  image: node:14.18
  stage: build
  variables:
    REACT_APP_API_ENDPOINT: ${API_ENDPOINT_QA}
    REACT_APP_IS_DEV_INSTALL: "true"
    REACT_APP_PIWIK_ID: "3722"
  before_script:
    - cd app/client
    - yarn install
  script:
    - yarn build
  artifacts:
    paths:
      - app/client/build # The application is built and packaged
  only:
    - qa

build_frontend_master:
  image: node:14.18
  stage: build
  variables:
    REACT_APP_API_ENDPOINT: ${API_ENDPOINT_MASTER}
    REACT_APP_IS_DEV_INSTALL: "false"
    REACT_APP_PIWIK_ID: "45"
  before_script:
    - cd app/client
    - yarn install
  script:
    - yarn build
  artifacts:
    paths:
      - app/client/build # The application is built and packaged
  only:
    - master

build_docker_backend:
  # This will create an image for the Webapp on the Gitlab CI registry
  # $CI_REGISTRY_IMAGE is an internal Gitlab CI variable
  stage: generate_docker_image
  only:
    - qa
    - master
    - test
  tags:
    - docker-image-build
  script: "echo 'Building Docker image ...'"
  variables:
    TO: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:latest

redeploy_test:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  environment:
    name: test
    url: https://webcast-test.web.cern.ch
  variables:
    GITLAB_NAMESPACE: webcast
    GITLAB_APP_NAME: webcast-website
    OPENSHIFT_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OPENSHIFT_NAMESPACE: webcast-test # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_APP_NAME: webapp # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_TEST
    IMAGE: test
  script:
    - "oc import-image $OPENSHIFT_APP_NAME --all --server=$OPENSHIFT_SERVER --namespace=$OPENSHIFT_NAMESPACE --from=gitlab-registry.cern.ch/$GITLAB_NAMESPACE/$GITLAB_APP_NAME/$IMAGE --confirm --token=$OPENSHIFT_TOKEN"
    # wait a bit for redeployment to happen then monitor the deployment status
    - "sleep 30s && oc rollout status dc/$OPENSHIFT_APP_NAME --server=$OPENSHIFT_SERVER --namespace $OPENSHIFT_NAMESPACE --token=$OPENSHIFT_TOKEN"
  only:
    - test # production or qa, the branch you want to publish

redeploy_qa:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  environment:
    name: qa
    url: https://webcast-qa.web.cern.ch
  variables:
    GITLAB_NAMESPACE: webcast
    GITLAB_APP_NAME: webcast-website
    OPENSHIFT_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OPENSHIFT_NAMESPACE: webcast-qa # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_APP_NAME: webapp # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_QA
    IMAGE: qa
  script:
    - "oc import-image $OPENSHIFT_APP_NAME --all --server=$OPENSHIFT_SERVER --namespace=$OPENSHIFT_NAMESPACE --from=gitlab-registry.cern.ch/$GITLAB_NAMESPACE/$GITLAB_APP_NAME/$IMAGE --confirm --token=$OPENSHIFT_TOKEN"
    # wait a bit for redeployment to happen then monitor the deployment status
    - "sleep 30s && oc rollout status dc/$OPENSHIFT_APP_NAME --server=$OPENSHIFT_SERVER --namespace $OPENSHIFT_NAMESPACE --token=$OPENSHIFT_TOKEN"
  only:
    - qa # production or qa, the branch you want to publish

redeploy_master:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  environment:
    name: production
    url: https://webcast.web.cern.ch
  variables:
    GITLAB_NAMESPACE: webcast
    GITLAB_APP_NAME: webcast-website
    OPENSHIFT_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OPENSHIFT_NAMESPACE: webcast # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_APP_NAME: webapp # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_MASTER
    IMAGE: master
  script:
    - "oc import-image $OPENSHIFT_APP_NAME --all --server=$OPENSHIFT_SERVER --namespace=$OPENSHIFT_NAMESPACE --from=gitlab-registry.cern.ch/$GITLAB_NAMESPACE/$GITLAB_APP_NAME/$IMAGE --confirm --token=$OPENSHIFT_TOKEN"
    # wait a bit for redeployment to happen then monitor the deployment status
    - "sleep 30s && oc rollout status dc/$OPENSHIFT_APP_NAME --server=$OPENSHIFT_SERVER --namespace $OPENSHIFT_NAMESPACE --token=$OPENSHIFT_TOKEN"
  only:
    - master # production or qa, the branch you want to publish
