# Openshift

> ⚠️ Secrets and service accounts are not restored. You need to create them manually. These include: 
> - Secret `gitlab-registry-auth`: To access the Gitlab Registry
> - Service Account `gitlabci-deployer`: To give Gitlab access to deploy in Openshift

## Install

### 1. Add the Openshift deploy key to Gitlab

> To grant access to the private Gitlab repository. If the repository is public, you can skip.

```bash
oc login https://api.paas.okd.cern.ch
oc project (webcast|webcast-qa|webcast-test) # 3 options
oc get secrets/sshdeploykey -o go-template='{{index .metadata.annotations "cern.ch/ssh-public-key"}}{{println}}'
```

### 2. Deploy a Docker image 

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user registry-editor -z gitlabci-deployer
```

```bash
oc policy add-role-to-user view -z gitlabci-deployer
```

```bash
oc serviceaccounts get-token gitlabci-deployer
```

2. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<QA|MASTER|TEST>`.
3. Generate a deploy token in Gitlab. This will generate a username and a password that will be added to Openshift later. 
   1. Needed rights for: read_registry
   2. The script is located on [/openshift/grant-registry-access.sh](/openshift/grant-registry-access.sh). Change the corresponging username/password.

- Imagestream being updated from Gitlab when `redeploy` stage finishes

### 4. Create the Redis pod for caching

Add Redis to the project from the OKD4 UI. 

Then, in Openshift select the template and deploy it (`Add to project` -> `Select from project`)

### 5. Create the EOS keytab secret

On OKD4 go to secrets and add a new one form YML, with the contents of `eos-credentials.yaml` file.

You will need to replace the `username` and `password` values with the correct ones.

### 6. Create the EOS volume

Follow the procedure here: https://paas.docs.cern.ch/3._Storage/eos/

- Name: `eos-volume`
- Size: `1MiB`
- Type: EOS
- Access mode: `RWX`

### 7. Restore the webapp

```bash
oc create -f openshift/webapp.yaml
```

or 

```bash
oc create -f openshift/webapp-qa.yaml
```

### 8. Update the config maps values:

Update the values in the `app-config` configmap to match the correct ones.

The secrets can be generated using python:

```python
import os
os.urandom(20)
```

### 9. Restore the daemon

```bash
oc create -f openshift/daemon.yaml
```

or

```bash
oc create -f openshift/daemon-qa.yaml
```

### 10. Restore the service and the route

```bash
oc create -f openshift/service-route.yaml
```

or

```bash
oc create -f openshift/service-route-qa.yaml
```


## Help

### Remove an already created template

```bash
oc delete template <TEMPLATE_NAME>
```

Example:
```bash
oc delete template webapp
```