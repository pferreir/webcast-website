# Webcast Website

[![pipeline status](https://gitlab.cern.ch/webcast/webcast-website/badges/openshift_master/pipeline.svg)](https://gitlab.cern.ch/webcast/webcast-website/commits/openshift_master)

[![coverage report](https://gitlab.cern.ch/webcast/webcast-website/badges/openshift_master/coverage.svg)](https://gitlab.cern.ch/webcast/webcast-website/commits/openshift_master)

## Operations documentation

- [https://dev-documentation.web.cern.ch/opsdoc/Webcast/webcast-website.html](https://dev-documentation.web.cern.ch/opsdoc/Webcast/webcast-website.html)
- Repository: [https://gitlab.cern.ch/collaborativeapps/opsdoc](https://gitlab.cern.ch/collaborativeapps/opsdoc)


## Preparing the Docker environment

Docker is needed.

### Cleaning Docker if an error occurs

```commandline
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

### First run - Creating the database

```bash
local$ docker-compose run web bash
docker$ flask init-db
```

## Building and running the Docker images

### Build

```commandline
docker-compose build
```

### Run

```commandline
docker-compose up
```

### Bash into the machine

```commandline
docker-compose run web bash
```

## Access the project using localhost

```
127.0.0.1:8080
```

or 

````
<hostname>:8080
````

## Database management

Create a database migration
```commandline
flask db migrate -d app/migrations
```

Apply a database migration
```commandline
flask db upgrade -d app/migrations
```

## Run tests

```commandline
pytest
```

If there is an error about `__pycache__` and `.pyc` files, delete them with the following command:

```
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
```

### Test WOWZA streams

* Webcast docs: `https://dev-documentation.web.cern.ch/doconverter/Webcast/HandyFFmpegCommands.html
`

You need ffmpeg to do the streaming to the wowza server and also a video to stream.

1. ffmpeg: https://ffmpeg.org/
1. Sample video (Big Bucks Bunny): https://peach.blender.org/download/ 

On mac, use this command:
```commandline
/Applications/ffmpeg/ffmpeg -re -i ~/Movies/Big_Buck_Bunny_1080p_surround_FrostWire.com.avi -acodec aac -strict -2 -ab 96k -vcodec libx264 -s 1920x1080 -b:v 5000k -preset medium -vprofile main -r 25 -f flv rtmp://tev:tev2016@wowzaqaorigin.cern.ch/liveoutside/test_event
```
* `liveoutside`: Is the application name
* `test_event`: Is the event ID (smil id will be: `test_event_all`, smil camera will be: `test_event_all.smil`)



# Openshift configuration for Webcast Website

The config folder includes the deployment configs for the application and also the services.

## Cronjobs

Then create the cronjob(s):

    oc replace -f cronjobs/

To delete all the existing jobs:

    oc delete jobs --all && oc delete cronjobs --all

NOTE:
It's important to add this environment variable if using python 3:
```
- name: LD_LIBRARY_PATH
    value: /opt/rh/rh-python34/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
```  

# Configuration parameters

* `SECRET_KEY`: Application secret key generated with `os.urandom(24)`.
* `APP_PORT`: Port where the application will run.
* `DEBUG`: Set to `True` to enable debug mode.
* `TESTING`: Set to `True` on testing environments
* `IS_LOCAL_INSTALLATION`: Set to `True` if the application will run locally (not on Openshift)

## Database configuration

* `DB_NAME`: Name of the database
* `DB_PASS`: Pass
* `DB_PORT`: Port
* `DB_SERVICE`: Service
* `DB_USER`: Username

# Oauth and security configuration

* `ADMIN_EGROUP`: e-group that will have access to the administration dashboard.
* `CERN_OAUTH_CLIENT_ID`: CERN Oauth client ID.
* `CERN_OAUTH_CLIENT_SECRET`: CERN Oauth client secret.
* `CERN_OAUTH_AUTHORIZE_URL`: CERN Oauth authorize URL.
* `CERN_OAUTH_TOKEN_URL`: CERN Oauth Token URL.
* `CERN_OAUTH_API_ME`: CERN Oauth API /Me URL.
* `USE_PROXY`: Need to be enabled to work with Oauth

## Logging
* `LOG_LEVEL`: Set the logging level. 
Available values `DEV` (Development purposes with DEBUG level).
`PROD` (Production purposes with INFO level)
* `WEBAPP_LOGS`: Location of the webapp logs file (including the file i.e.: /usr/src/logs/webapp.log
)

## Images and uploads

* `IMAGES_FOLDER`: Name of the images folder.
* `PERMANENT_STREAM_IMAGES_FOLDER`: Name of the default permanent stream images folder.
* `EVENTS_IMAGES_FOLDER`: Name of the events images folder.
* `UPLOADED_FILES_DEST`: Absolute path to uploads destination folder without ending slash.
* `UPLOADED_IMAGES_DEST`: Absolute path to images uploads folder without ending slash.
* `UPLOADED_EVENTS_IMAGES_DEST`: Absolute path to events images uploads folder without ending slash.
* `STATIC_FILES_PATH`: Absolute path for the static files folder.
* `DEFAULT_IMAGES_FOLDER`: Relative path to the images folder from the `STATIC_FILES_PATH`.
* `DEFAULT_CATEGORIES_FOLDER`:Name of the default categories images folder.
* `DEFAULT_EVENTS_FOLDER`: Name of the default events images folder.

## Indico

* `INDICO_URL`: Indico root URL with protocol.
* `INDICO_API_KEY`: Indico API Key
* `INDICO_API_SECRET_KEY`: Indico API secret


# Known problems

## Images not displayed properly

Ensure that the cronjob HOSTNAME is set up and that the route is redirecting http traffic to https.


## Timestamp invalid on Indico request

Probably the container date and time is not synchronized with the host. Restart docker and start it again.





