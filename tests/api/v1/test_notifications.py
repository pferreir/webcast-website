import json
import time

import datetime

from app.handlers.follow_ups import FollowUpHandler
from app.models.events import Event, FollowUp, EventStatus

from app.models.api import ApiKey
from flask import url_for, current_app

from app.api.v1.utils import generate_hmac_signature, is_signature_valid
from app.extensions import db
from app.models.audiences import AuthorizedUser, Audience
from tests import BaseTestCase
from httmock import urlmatch, response, HTTMock

from tests.api.v1 import wowza_mock, add_api_key, add_live_event, add_audience_and_authorized_users
from tests.helpers import indico_webcasts_mock


class ApiNotificationTest(BaseTestCase):

    def test_generate_hmac_signature(self):
        path = "/api/"
        params = (("test1", "test1"), ("test2", "test2"))
        secret_key = "12345"

        hmac_signature = generate_hmac_signature(path=path, params=params, secret_key=secret_key)

        self.assertNotEqual(hmac_signature, "")

    def test_is_hmac_signature_valid(self):
        """
        /api/v1/update-stream/?ak=68d795a69b354e22a9a5eb663b72e495&api=v1%2Fupdate-stream%2F&app_name=livehd&camera_stream=630357_camera&indico_id=630357&room_video_quality=HD&slides_stream=630357_slides&timestamp=1499169597&signature=972a634562ae94b1e1be34c160

        :return: 
        """
        add_api_key(self.client)

        api_object = ApiKey.query.get(1)

        path = "/api/"
        params = (
            ("ak", api_object.access_key),
            ("api", "stream_update"),
            ("timestamp", int(time.time())),
            ("indico_id", '1234'),
            ("room_video_quality", "HD"),
            ("appName", "livehd"),
            ("camera_stream", "1234_camera_all"),
        )

        hmac_signature = generate_hmac_signature(path=path, params=params, secret_key=api_object.secret_key)

        self.assertNotEqual(hmac_signature, "")

        signature_valid, message = is_signature_valid(api_object.access_key, hmac_signature, int(time.time()),
                                                      args=params, api_path=path)

        self.assertTrue(signature_valid, message)
        self.assertEqual(message, "valid")

    def test_is_hmac_signature_timeout(self):
        add_api_key(self.client)

        api_object = ApiKey.query.get(1)

        path = "/api/"
        params = (
            ("ak", api_object.access_key),
            ("api", "stream_update"),
            ("timestamp", int(time.time())),
            ("indico_id", '1234'),
            ("room_video_quality", "HD"),
            ("appName", "livehd"),
            ("camera_stream", "1234_camera_all"),
        )

        hmac_signature = generate_hmac_signature(path=path, params=params, secret_key=api_object.secret_key)

        self.assertNotEqual(hmac_signature, "")

        signature_valid, message = is_signature_valid(api_object.access_key, hmac_signature, int(time.time()) - 301,
                                                      args=params, api_path="/path/")

        self.assertFalse(signature_valid, message)
        self.assertEqual(message, "request timeout.")

    def test_api_stream_update(self):
        with HTTMock(wowza_mock):

            # Create an event and an Api Key
            add_audience_and_authorized_users(self.client)
            add_live_event(self.client)
            add_api_key(self.client)

            api_object = ApiKey.query.get(1)

            timestamp = int(time.time())
            query_args = (
                ("ak", api_object.access_key),
                ("api", "stream_update"),
                ("timestamp", timestamp),
                ("indico_id", '1234'),
                ("room_video_quality", "HD"),
                ("app_name", "livehd"),
                ("camera_stream", "1234_camera"),
                ("slides_stream", ""),
            )

            # generating a signature and checking that it is valid
            signature = generate_hmac_signature(path='/api/v1/update-stream/', params=query_args,
                                                secret_key=api_object.secret_key)
            signature_valid, message = is_signature_valid(api_object.access_key, signature, timestamp,
                                                          args=query_args)

            self.assertTrue(signature_valid, message)
            self.assertEqual(message, "valid")

            # making the request to the stream update notification
            api_url = url_for('api_v1.streamupdatenotification',
                              ak=api_object.access_key,
                              api="stream_update",
                              timestamp=timestamp,
                              signature=signature,
                              indico_id='1234',
                              room_video_quality='HD',
                              app_name='livehd',
                              camera_stream='1234_camera',
                              slides_stream='')

            #
            response = self.client.get(api_url)
            self.assertEqual(response.status_code, 200, response.data)

            expected_response = {'result': "ok"}

            self.assertEqual(json.loads(response.data.decode('utf-8')), expected_response)

    def test_follow_up_notification_not_found(self):
        origin_server = "http://test.com"
        app_name = "livehd"
        stream_name = "5555_camera_all"
        on_air = True

        api_url = url_for('api_v1.streamfollowupnotification',
                          originServer=origin_server,
                          streamName=stream_name,
                          appName=app_name,
                          onAir=on_air)

        self.assertNotEqual(api_url, "")
        response = self.client.get(api_url)

        expected_response = {
            'result': 'No Event or FollowUp found with indico_id: 5555'
        }

        self.assertEqual(response.status_code, 403, response.data)
        self.assertEqual(json.loads(response.data.decode('utf-8')), expected_response)

    def test_follow_up_notification_found(self):
        with HTTMock(indico_webcasts_mock):
            origin_server = current_app.config['WOWZA_ORIGIN_URLS'][0]
            app_name = "livehd"
            stream_name = "1234_camera"
            on_air = 'false'

            add_audience_and_authorized_users(self.client)
            add_live_event(self.client)
            event = Event.query.get(1)

            self.assertEqual(event.status, EventStatus.LIVE)

            followup_handler = FollowUpHandler()
            followup_handler.create_event_follow_ups(event)

            self.assertEqual(len(event.follow_ups), 2)

            self.assertEqual(event.follow_ups[0].stream_name, '1234_camera')
            self.assertEqual(event.follow_ups[1].stream_name, '1234_slides')

            for follow_up in event.follow_ups:
                self.assertTrue(follow_up.on_air, "All FollowUp's ust be on_air if event is LIVE")

            api_url = url_for('api_v1.streamfollowupnotification',
                              originServer=origin_server,
                              streamName=stream_name,
                              appName=app_name,
                              onAir=on_air)

            self.assertNotEqual(api_url, "")
            response = self.client.get(api_url)
            #
            expected_response = {
                'result': 'ok'
            }

            self.assertEqual(response.status_code, 200, response.data)
            self.assertEqual(json.loads(response.data.decode('utf-8')), expected_response)

            event = Event.query.get(1)
            self.assertEqual(len(event.follow_ups), 1)

            self.assertEqual(event.status, EventStatus.ARCHIVED)

            stream_name2 = "1234_slides"

            api_url = url_for('api_v1.streamfollowupnotification',
                              originServer=origin_server,
                              streamName=stream_name2,
                              appName=app_name,
                              onAir=on_air)

            self.assertNotEqual(api_url, "")
            response = self.client.get(api_url)
            #
            expected_response = {
                'result': 'ok'
            }

            self.assertEqual(response.status_code, 200, response.data)
            self.assertEqual(json.loads(response.data.decode('utf-8')), expected_response)

            event = Event.query.get(1)
            self.assertEqual(len(event.follow_ups), 0, "Even with contributions, the followups are always deleted")

            self.assertEqual(event.status, EventStatus.ARCHIVED)

    def test_follow_up_notification_no_contributions(self):
        with HTTMock(indico_webcasts_mock):
            origin_server = current_app.config['WOWZA_ORIGIN_URLS'][0]
            app_name = "livehd"
            stream_name = "101_camera"
            on_air = 'false'

            add_audience_and_authorized_users(self.client)
            add_live_event(self.client, indico_id='101')
            event = Event.query.get(1)

            self.assertEqual(event.status, EventStatus.LIVE)

            followup_handler = FollowUpHandler()
            followup_handler.create_event_follow_ups(event)

            self.assertEqual(len(event.follow_ups), 2)

            self.assertEqual(event.follow_ups[0].stream_name, '101_camera')
            self.assertEqual(event.follow_ups[1].stream_name, '101_slides')

            for follow_up in event.follow_ups:
                self.assertTrue(follow_up.on_air, "All FollowUp's ust be on_air if event is LIVE")

            api_url = url_for('api_v1.streamfollowupnotification',
                              originServer=origin_server,
                              streamName=stream_name,
                              appName=app_name,
                              onAir=on_air)

            self.assertNotEqual(api_url, "")
            response = self.client.get(api_url)
            #
            expected_response = {
                'result': 'ok'
            }

            self.assertEqual(response.status_code, 200, response.data)
            self.assertEqual(json.loads(response.data.decode('utf-8')), expected_response)

            event = Event.query.get(1)
            self.assertEqual(len(event.follow_ups), 1)

