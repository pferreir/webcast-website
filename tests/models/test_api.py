from flask import url_for

from app.extensions import db
from app.models.api import ApiKey
from tests import BaseTestCase


class ApiKeyTest(BaseTestCase):


    def test_generate_api_key_and_secret(self):

        api_key = ApiKey.generate_key()
        secret_key = ApiKey.generate_secret()

        self.assertNotEqual(api_key, "")
        self.assertNotEqual(str(secret_key), "")

        self.assertLessEqual(len(api_key), 255)
        self.assertLessEqual(len(str(secret_key)), 255)

        response = self.client.get(url_for('users.index'))
        self.assertEqual(response.status_code, 200)

    def test_add_api_key(self):
        new_api_key = ApiKey(
                         name="Example 1",
                         description="Description 1",
                         status=True,
                         resources='notification')
        db.session.add(new_api_key)
        db.session.commit()

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 1)


    def test_remove_api_key(self):
        """
        1. We add an ApiKey
        2. We remove it
        
        :return: 
        """
        new_api_key = ApiKey(
                         name="Example 1",
                         description="Description 1",
                         status=True,
                         resources='notification')
        db.session.add(new_api_key)
        db.session.commit()

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 1)

        db.session.delete(api_keys[0])
        db.session.commit()

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 0)
