import logging

logger = logging.getLogger('webapp.test')

RESPONSE_OK = {
    "drupal": "",
    "text": None,
    "result": "ok"
}

RESPONSE_ERROR = {
    "text": "indico_id is missing\nargs:  start_time end_time indico_id stream_name application_name date(YYYYMMDD) "
            "logdate(YYYYMMDD)\n \n                example: "
            "start_time=16:00&end_time=18:30&indico_id=99235&stream_name=slides&application_name=cmscentres&date"
            "=20110125[&logdate=20111018][&sharepoint=True]",
    "result": "error"
}
