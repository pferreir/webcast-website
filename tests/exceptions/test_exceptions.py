from tests import BaseTestCase

from app.exceptions import WebappException


class WebappExceptionTest(BaseTestCase):
    def test_create_webapp_exception(self):
        with self.assertRaises(WebappException) as context:
            raise WebappException("This is a sample exception")
