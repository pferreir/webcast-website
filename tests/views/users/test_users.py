import datetime

from app.extensions import db
from app.models.audiences import Audience, AuthorizedUser
from flask import url_for, current_app

from app.models.events import Event, EventStatus
from app.models.initializers.audiences import add_initial_authorized_users_to_database, \
    add_initial_audiences_to_database
from app.models.initializers.streams import add_initial_categories_to_database, \
    add_initial_permanent_streams_to_database
from tests import BaseTestCase


class UsersTest(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.insert_sample_data()

    def insert_sample_data(self):
        pass
        add_initial_categories_to_database()
        # add_initial_authorized_users_to_database()
        add_initial_audiences_to_database()
        add_initial_permanent_streams_to_database()

    def test_index_reachable(self):
        response = self.client.get(url_for('users.index'))
        self.assertEqual(response.status_code, 200)

    def test_permanent_webcasts_reachable(self):
        response = self.client.get(url_for('users.permanent_webcasts'))
        self.assertEqual(response.status_code, 200)

    def test_permanent_webcast_test_channel(self):
        response = self.client.get(url_for('users.permanent_webcast_test_channel'))
        self.assertEqual(response.status_code, 200)

    def test_permanent_webcast_category(self):
        response = self.client.get(url_for('users.permanent_webcasts_category_details', category_id=1))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_for('users.permanent_webcasts_category_details', category_id=4))
        self.assertEqual(response.status_code, 200)

    def test_permanent_webcast_play(self):
        response = self.client.get(url_for('users.play_webcast', stream_id=1))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_for('users.play_webcast', stream_id=11))
        self.assertEqual(response.status_code, 200)
