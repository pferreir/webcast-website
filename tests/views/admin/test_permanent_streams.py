from sqlalchemy.orm.exc import NoResultFound

from app.models.audiences import Audience
from app.models.streams import PermanentStream, Category
from flask import url_for, current_app

from tests import BaseTestCase


class AdminPermanentStreamsTest(BaseTestCase):
    def test_permanent_streams_rechabale(self):
        response = self.client.get(url_for('admin.streams.permanent_streams_home'))
        self.assertEqual(response.status_code, 200)

    def test_permanent_streams_add_reachable(self):
        response = self.client.get(url_for('admin.streams.permanent_streams_add'))
        self.assertEqual(response.status_code, 200)


    def test_permanent_streams_add(self):
        """
        
        :return: 
        """

        """
        A PermanentStreamForm needs an Audience and a Category. We have to create them first.
        """
        permanent_stream_category_form_data = {
            'name': 'Atlas',
            'priority': 1,
            'default_img': '/static-files/images/default/categories/atlas/category_atlas.png'
        }
        self.client.post(url_for('admin.streams.categories_add'), data=permanent_stream_category_form_data,
                         follow_redirects=True)

        category = Category.query.get(1)
        self.assertIsNotNone(category)

        audience_form_data = {"name": "Audience 2",
                               "default_application": "live",
                               "authorized_users": []}

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data,
                         follow_redirects=True)
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        permanent_stream_form_data1 = {
            "title": "Cloud Chamber",
            "audience": audience.id,
            "category": category.id,
            "default_img": "/static-files/images/default/categories/Microcosm/Cloud_Chamber.jpg",
            "custom_img": "",
            "player_src": "https://wowza.cern.ch/microcosm/cloud_chamber.smil/playlist.m3u8",
            "player_width": 1280,
            "player_height": 720,
            "authorized_users": []
        }


        self.client.post(url_for('admin.streams.permanent_streams_add'), data=permanent_stream_form_data1,
                         follow_redirects=True)

        permanent_streams = PermanentStream.query.all()
        self.assertEqual(len(permanent_streams), 1)

    def test_permanent_streams_delete(self):
        """

        :return: 
        """

        """
        A PermanentStreamForm needs an Audience and a Category. We have to create them first.
        """
        permanent_stream_category_form_data = {
            'name': 'Atlas',
            'priority': 1,
            'default_img': '/static-files/images/default/categories/atlas/category_atlas.png'
        }
        self.client.post(url_for('admin.streams.categories_add'), data=permanent_stream_category_form_data,
                         follow_redirects=True)

        category = Category.query.get(1)
        self.assertIsNotNone(category)

        audience_form_data = {"name": "Audience 2",
                              "default_application": "live",
                              "authorized_users": []}

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data,
                         follow_redirects=True)
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        permanent_stream_form_data1 = {
            "title": "Cloud Chamber",
            "audience": audience.id,
            "category": category.id,
            "default_img": "/static-files/images/default/categories/Microcosm/Cloud_Chamber.jpg",
            "custom_img": "",
            "player_src": "https://wowza.cern.ch/microcosm/cloud_chamber.smil/playlist.m3u8",
            "player_width": 1280,
            "player_height": 720,
            "authorized_users": []
        }

        self.client.post(url_for('admin.streams.permanent_streams_add'), data=permanent_stream_form_data1,
                         follow_redirects=True)

        permanent_streams = PermanentStream.query.all()
        self.assertEqual(len(permanent_streams), 1)

        self.client.post(url_for('admin.streams.permanent_streams_delete', permanent_stream_id=1), follow_redirects=True)

        permanent_streams = PermanentStream.query.all()
        self.assertEqual(len(permanent_streams), 0)


    def test_permanent_streams_edit(self):
        """

        :return: 
        """

        """
        A PermanentStreamForm needs an Audience and a Category. We have to create them first.
        """
        permanent_stream_category_form_data = {
            'name': 'Atlas',
            'priority': 1,
            'default_img': '/static-files/images/default/categories/atlas/category_atlas.png'
        }
        self.client.post(url_for('admin.streams.categories_add'), data=permanent_stream_category_form_data,
                         follow_redirects=True)

        category = Category.query.get(1)
        self.assertIsNotNone(category)

        audience_form_data = {"name": "Audience 2",
                              "default_application": "live",
                              "authorized_users": []}

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data,
                         follow_redirects=True)
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        permanent_stream_form_data1 = {
            "title": "Cloud Chamber",
            "audience": audience.id,
            "category": category.id,
            "default_img": "/static-files/images/default/categories/Microcosm/Cloud_Chamber.jpg",
            "custom_img": "",
            "player_src": "https://wowza.cern.ch/microcosm/cloud_chamber.smil/playlist.m3u8",
            "player_width": 1280,
            "player_height": 720,
            "authorized_users": [],
            "is_sharable": True,
            "is_embeddable": True,
            "ise_visible": True
        }

        permanent_stream_form_data2 = {
            "title": "Cloud Chambre",
            "audience": audience.id,
            "category": category.id,
            "default_img": "1234",
            "custom_img": "",
            "player_src": "https://wowza.cern.ch/microcosm/cloud_chamber.smil/playlist.m3u8",
            "player_width": 1280,
            "player_height": 720,
            "authorized_users": []
        }

        self.client.post(url_for('admin.streams.permanent_streams_add'), data=permanent_stream_form_data1,
                         follow_redirects=True)

        permanent_stream = PermanentStream.query.filter_by(title='Cloud Chamber').one()

        self.client.post(url_for('admin.streams.permanent_streams_edit', permanent_stream_id=permanent_stream.id), data=permanent_stream_form_data2,
                         follow_redirects=True)

        try:
            permanent_stream = PermanentStream.query.filter_by(title='Cloud Chambre').one()
            self.assertEqual(permanent_stream.default_img, "1234")
            self.assertFalse(permanent_stream.is_sharable)
            self.assertFalse(permanent_stream.is_embeddable)
        except NoResultFound:
            self.assertTrue(False, "PermanentStream was not found with the new title")