from flask import url_for
from sqlalchemy.orm.exc import NoResultFound

from app.models.audiences import AuthorizedUser, Audience
from tests import BaseTestCase


class AdminAudiencesTest(BaseTestCase):
    def test_audiences_reachable(self):
        response = self.client.get(url_for('admin.authorization.audiences'))
        self.assertEqual(response.status_code, 200)

    def test_authorized_users_reachable(self):
        response = self.client.get(url_for('admin.authorization.authorized_users'))
        self.assertEqual(response.status_code, 302)

    def test_audiences_add_reachable(self):
        response = self.client.get(url_for('admin.authorization.audiences_add'))
        self.assertEqual(response.status_code, 200)

    def test_authorized_users_add_reachable(self):
        response = self.client.get(url_for('admin.authorization.authorized_users_add'))
        self.assertEqual(response.status_code, 200)

    def test_authorized_users_add(self):
        authorized_user_form_data1 = {"name": "Authorized User 1"}
        authorized_user_form_data2 = {"name": "Authorized User 2"}
        authorized_user_form_data3 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)
        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data2,
                         follow_redirects=True)
        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data3,
                         follow_redirects=True)

        users = AuthorizedUser.query.all()
        self.assertEqual(len(users), 3)

    def test_authorized_users_delete(self):
        """
        1. Add three AuthorizedUser
        2. Delete the first one
        3. The deleted one shouldn be found
        4. The count must be 2
        
        :return: 
        """
        authorized_user_form_data1 = {"name": "Authorized User 1"}
        authorized_user_form_data2 = {"name": "Authorized User 2"}
        authorized_user_form_data3 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)
        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data2,
                         follow_redirects=True)
        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data3,
                         follow_redirects=True)

        users = AuthorizedUser.query.all()
        self.assertEqual(len(users), 3)

        self.client.post(url_for('admin.authorization.authorized_users_delete', authorized_user_id=1),
                         follow_redirects=True)
        users = []
        try:
            AuthorizedUser.query.filter_by(name="Authorized User 1").one()
            self.assertTrue(False, "AuthorizedUser was not deleted successfully")
        except NoResultFound:
            users = AuthorizedUser.query.all()
            self.assertEqual(len(users), 2)

    def test_authorized_users_edit(self):
        """
        1. Add three AuthorizedUser
        2. Delete the first one
        3. The edit one should be found with the new name

        :return: 
        """
        authorized_user_form_data1 = {"name": "Authorized User 1"}
        authorized_user_form_data2 = {"name": "Authorized User 2"}
        authorized_user_form_data3 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)
        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data2,
                         follow_redirects=True)

        users = AuthorizedUser.query.all()
        self.assertEqual(len(users), 2)

        self.client.post(url_for('admin.authorization.authorized_users_edit', authorized_user_id=1),
                         data=authorized_user_form_data3,
                         follow_redirects=True)
        try:
            AuthorizedUser.query.filter_by(name="Authorized User 1").one()
            self.assertTrue(False, "AuthorizedUser was not edited successfully")
        except NoResultFound:
            pass

        try:
            AuthorizedUser.query.filter_by(name="Authorized User 3").one()
        except NoResultFound:
            self.assertTrue(False, "AuthorizedUser was not found with new name")

    def test_audiences_add(self):

        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {"name": "Audience 1",
                               "default_application": "atlas",
                               "authorized_users": [authorized_user.id]}

        audience_form_data2 = {"name": "Audience 2",
                               "default_application": "live",
                               "authorized_users": []}

        """
        name = form.name.data
        default_application = form.default_application.data
        authorized_users = form.authorized_users.data
        """

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data1,
                         follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 1)

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data2,
                         follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 2)

        audience1 = Audience.query.get(1)
        self.assertEqual(len(audience1.authorized_users), 1)

        audience2 = Audience.query.get(2)
        self.assertEqual(len(audience2.authorized_users), 0)

    def test_audiences_delete(self):
        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {"name": "Audience 1",
                               "default_application": "atlas",
                               "authorized_users": [authorized_user.id]}

        audience_form_data2 = {"name": "Audience 2",
                               "default_application": "live",
                               "authorized_users": []}

        """
        name = form.name.data
        default_application = form.default_application.data
        authorized_users = form.authorized_users.data
        """

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data1,
                         follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 1)

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data2,
                         follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 2)

        self.client.post(url_for('admin.authorization.audiences_delete', audience_id=1), follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 1)

        # Even if the Audience was deleted, the AuthorizedUser should've not
        users = AuthorizedUser.query.all()
        self.assertEqual(len(users), 1)

    def test_audiences_edit(self):
        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(url_for('admin.authorization.authorized_users_add'), data=authorized_user_form_data1,
                         follow_redirects=True)

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {"name": "Audience 1",
                               "default_application": "atlas",
                               "authorized_users": [authorized_user.id]}

        audience_form_data2 = {"name": "Audience 2",
                               "default_application": "live",
                               "authorized_users": []}

        audience_form_data3 = {"name": "Audience 3",
                               "default_application": "permatlas",
                               "authorized_users": []
                               }

        audience_form_data4 = {
                               "authorized_users": [authorized_user.id]
                               }

        """
        name = form.name.data
        default_application = form.default_application.data
        authorized_users = form.authorized_users.data
        """

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data1,
                         follow_redirects=True)

        audiences = Audience.query.all()
        self.assertEqual(len(audiences), 1)

        self.client.post(url_for('admin.authorization.audiences_add'), data=audience_form_data2,
                         follow_redirects=True)

        # Edit the existing audience.
        # Change its name
        # Change default application
        # Remove the AuthorizedUser

        self.client.post(url_for('admin.authorization.audiences_edit', audience_id=1), data=audience_form_data3,
                         follow_redirects=True)
        try:
            Audience.query.filter_by(name="Audience 1").one()
            self.assertTrue(False, "Audience was not edited successfully")
        except NoResultFound:
            pass

        try:
            audience = Audience.query.filter_by(name="Audience 3").one()
            self.assertEqual(len(audience.authorized_users), 0)
            self.assertEqual(audience.default_application, "permatlas")
        except NoResultFound:
            self.assertTrue(False, "AuthorizedUser was not found with new name")


        # Add an AuthorizedUser only won't work because the form requires the other attributes to be filled in.
        self.client.post(url_for('admin.authorization.audiences_edit', audience_id=1), data=audience_form_data4,
                         follow_redirects=True)

        audience = Audience.query.filter_by(name="Audience 3").one()
        self.assertEqual(len(audience.authorized_users), 0)

        # Add an AuthorizedUser only won't work because the form requires the other attributes to be filled in.
        self.client.post(url_for('admin.authorization.audiences_edit', audience_id=1), data=audience_form_data1,
                         follow_redirects=True)

        audience = Audience.query.filter_by(name="Audience 1").one()
        self.assertEqual(len(audience.authorized_users), 1)


