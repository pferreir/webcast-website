import json
import os

from flask import url_for, current_app

from tests import BaseTestCase

# EXPECTED_GET_IMAGES_RESULT = {'result': ['/static-files/images/default/events/event_noimage.jpg',
#                                          '/static-files/images/default/events/event_noimage_small.jpg',
#                                          '/static-files/images/default/events/tedx_cern.jpg',
#                                          '/static-files/images/default/events/tedx_cern_small.jpg',
#                                          '/static-files/images/default/events/event_noimage_reflected.png',
#                                          '/static-files/images/default/events/tedx_cern.png',
#                                          '/static-files/images/default/events/tedx_cern_reflected.png']}

EXPECTED_GET_IMAGES_RESULT = {'result': []}


class AdminHomeTest(BaseTestCase):
    def test_admin_home_reachable(self):
        response = self.client.get(url_for('admin.index'))
        self.assertEqual(response.status_code, 200)

    def test_get_images_path(self):

        default_images_folders = [x[0] for x in os.walk(
            os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                         current_app.config.get('DEFAULT_EVENTS_FOLDER')))]

        default_images_folders = ['/static-files/images/default/events/']

        response = self.client.get(url_for('admin._get_images_path', images_path=default_images_folders[0]))
        self.assertEqual(response.status_code, 200)

        decoded_response = json.loads(response.data.decode('utf-8'))

        self.assertEqual(decoded_response, EXPECTED_GET_IMAGES_RESULT)
