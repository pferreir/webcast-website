from flask_testing import TestCase
from app.app_factory import create_app
from app.extensions import db


class BaseTestCase(TestCase):
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')

    def create_app(self):

        application = create_app("config/config.test.py")

        return application

    def setUp(self):
        # with self.app.app_context():
        db.create_all()

    def tearDown(self):
        # with self.app.app_context():
        db.session.remove()
        db.drop_all()
