import json

from flask import current_app
from httmock import urlmatch, response, HTTMock

EVENTS_DETAILS_JSON = [
    {
        "folders": [
            {
                "_type": "folder",
                "attachments": [
                    {
                        "_type": "attachment",
                        "description": "",
                        "content_type": "application/pdf",
                        "id": 2176100,
                        "size": 114945,
                        "modified_dt": "2017-03-01T10:36:39.009477+00:00",
                        "title": "CERNEPSeminar28.03.17.pdf",
                        "download_url": "https://indico.cern.ch/event/586436/attachments/1420240/2176100/CERNEPSeminar28.03.17.pdf",
                        "filename": "CERNEPSeminar28.03.17.pdf",
                        "is_protected": False,
                        "type": "file"
                    }
                ],
                "title": None,
                "is_protected": False,
                "default_folder": True,
                "id": 1420240,
                "description": ""
            }
        ],
        "startDate": {
            "date": "2017-03-28",
            "tz": "Europe/Zurich",
            "time": "11:00:00"
        },
        "_type": "Conference",
        "hasAnyProtection": False,
        "endDate": {
            "date": "2017-03-28",
            "tz": "Europe/Zurich",
            "time": "12:00:00"
        },
        "description": "<p>Precision physics requires appropriate inclusion of higher order effects and the knowledge of very precise input parameters of the electroweak Standard Model. One of the basic input parameters is the effective QED coupling constant &alpha;(s) which depends on the energy scale because of charge screening by vacuum polarization. Hadronic non-perturbative effects limits the accuracy of &alpha;(s) from low energy to the Z mass scale. We present the measurement of the running of the QED coupling constant in the time-like region 0.6 &lt; &radic;s &lt; 0.975 GeV with the KLOE detector at DA&Phi;NE , using the ISR differential cross section d&sigma;(e<sup>+</sup>e<sup>&minus;</sup> &rarr; &mu;<sup>+</sup>&mu;<sup>&minus;</sup> &gamma;)/d&radic;s. The result shows a clear contribution of the &rho;&minus;&omega; resonances to the photon propagator with a significance of the hadronic contribution to the running of &alpha;(s) of more than 5&sigma;. It represents the first measurement of the running of &alpha;(s) in this energy region and the strongest direct evidence achieved in both time- and space-like regions by a single experiment. For the first time, also the real and imaginary part of &Delta;&alpha;(s) have been extracted, showing clearly the importance of the role of the imaginary part. From a fit to the real part of &Delta;&alpha;(s) and assuming lepton universality, the branching ratio BR(&omega; &rarr; &mu;<sup>+</sup>&mu;<sup>&minus;</sup>) = (6.6 &plusmn;1.4stat &plusmn;1.7syst) &middot; 10<sup>&minus;5</sup> has been obtained.</p>",
        "roomMapURL": "https://maps.cern.ch/mapsearch/mapsearch.htm?n=['500/1-001']",
        "creator": {
            "affiliation": "CERN",
            "_type": "Avatar",
            "last_name": "Lourenco",
            "email": "carlos.lourenco@cern.ch",
            "emailHash": "a01fc1ada4584baf2ceee9d5c639839b",
            "_fossil": "conferenceChairMetadata",
            "fullName": "Lourenco, Carlos",
            "first_name": "Carlos",
            "id": "2527"
        },
        "material": [

        ],
        "visibility": {
            "id": "",
            "name": "Everywhere"
        },
        "roomFullname": "500-1-001 - Main Auditorium",
        "references": [

        ],
        "allowed": {
            "users": [

            ],
            "groups": [

            ]
        },
        "address": "",
        "modificationDate": {
            "date": "2017-02-27",
            "tz": "Europe/Zurich",
            "time": "11:12:06.201099"
        },
        "timezone": "Europe/Zurich",
        "creationDate": {
            "date": "2016-11-03",
            "tz": "Europe/Zurich",
            "time": "12:31:51.601744"
        },
        "id": "586436",
        "category": "EP Seminar",
        "room": "Main Auditorium",
        "title": "Measurement of the running of the fine structure constant below 1 GeV with the KLOE detector",
        "url": "https://indico.cern.ch/event/586436/",
        "note": {

        },
        "chairs": [
            {
                "_type": "ConferenceChair",
                "last_name": "Venanzoni",
                "affiliation": "INFN",
                "db_id": 519525,
                "_fossil": "conferenceChairMetadata",
                "fullName": "Venanzoni, Graziano",
                "id": "519525",
                "first_name": "Graziano",
                "emailHash": "16eef1969e6ed99352b6f68ee3fc3c4f",
                "person_id": 2979360,
                "email": "graziano.venanzoni@lnf.infn.it"
            }
        ],
        "location": "CERN",
        "_fossil": "conferenceMetadata",
        "type": "simple_event",
        "categoryId": 3247
    }
]

WEBCAST_DETAILS = {
    "count": 1,
    "additionalInfo": {

    },
    "_type": "HTTPAPIResult",
    "url": "https://indico.cern.ch/export/event/586436.json?ak=43f1b7f0-2c47-475e-8ded-e9974f8b5fd9&timestamp=1490280109&signature=2d21772e6f090002424246087052428a6679ab72",
    "results": EVENTS_DETAILS_JSON,
    "ts": 1490280109
}

EVENT_1_JSON = {
    'audience': 'No restriction',
    '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
    'room': 'Salle Curie',
    'services': [
        'webcast',
        'recording'
    ],
    'event_id': 100,
    'status': 'A',
    'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
    'endDate': {
        'date': '2017-03-11',
        'tz': 'UTC',
        'time': '14:19:00'
    },
    'location': 'CERN',
    'room_full_name': '40-S2-C01 - Salle Curie',
    'title': 'ATLAS Weekly - Measurement of the $tbar{t}gamma$ production cross section at  $sqrt{s} = 8$ Tekern -0.1em V in $20.2;mathrm{fb}^{-1}$ of $pp$ collision  data collected with the ATLAS detector',
    'startDate': {
        'date': '2017-03-14',
        'tz': 'UTC',
        'time': '14:15:00'
    }
}

EVENTS_JSON = [
    EVENT_1_JSON,
    {
        'audience': 'ATLAS collaborators only',
        '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
        'room': 'Social Room',
        'services': [
            'webcast',
            'recording'
        ],
        'event_id': 1234,
        'status': 'A',
        'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
        'endDate': {
            'date': '2017-03-14',
            'tz': 'UTC',
            'time': '14:19:00'
        },
        'location': 'CERN',
        'room_full_name': '40-S2-C01 - Salle Curie',
        'title': 'ATLAS Weekly - Measurement of the $tbar{t}gamma$ production cross section at  $sqrt{s} = 8$ Tekern -0.1em V in $20.2;mathrm{fb}^{-1}$ of $pp$ collision  data collected with the ATLAS detector',
        'startDate': {
            'date': '2017-03-15',
            'tz': 'UTC',
            'time': '14:15:00'
        }
    },
    {
        'audience': 'No restriction',
        '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
        'room': 'Salle Curie',
        'services': [
            'webcast',
            'recording'
        ],
        'event_id': 300,
        'status': 'A',
        'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
        'endDate': {
            'date': '2017-04-14',
            'tz': 'UTC',
            'time': '14:19:00'
        },
        'location': 'CERN',
        'room_full_name': '40-S2-C01 - Salle Curie',
        'title': 'ATLAS Weekly - Measurement of the $tbar{t}gamma$ production cross section at  $sqrt{s} = 8$ Tekern -0.1em V in $20.2;mathrm{fb}^{-1}$ of $pp$ collision  data collected with the ATLAS detector',
        'startDate': {
            'date': '2017-03-14',
            'tz': 'UTC',
            'time': '14:15:00'
        }
    },
    {
        'audience': 'ATLAS collaborators only',
        '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
        'room': 'Social Room',
        'services': [
            'webcast',
            'recording'
        ],
        'event_id': 1234,
        'status': 'A',
        'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
        'endDate': {
            'date': '2017-03-25',
            'tz': 'UTC',
            'time': '15:19:00'
        },
        'location': 'CERN',
        'room_full_name': '40-S2-C01 - Salle Curie',
        'title': 'ATLAS Weekly - Measurement of the $tbar{t}gamma$ production cross section at  $sqrt{s} = 8$ Tekern -0.1em V in $20.2;mathrm{fb}^{-1}$ of $pp$ collision  data collected with the ATLAS detector',
        'startDate': {
            'date': '2017-03-25',
            'tz': 'UTC',
            'time': '14:15:00'
        }
    }
]

MULTIPLE_INDICO_WEBCASTS = {
    "count": 2,
    "additionalInfo": {

    },
    "_type": "HTTPAPIResult",
    "url": "https://indico.cern.ch/export/event/586436.json?ak=43f1b7f0-2c47-475e-8ded-e9974f8b5fd9&timestamp=1490280109&signature=2d21772e6f090002424246087052428a6679ab72",
    "results": EVENTS_JSON
}


# http://indico.corn/export/event/622398.json
@urlmatch(netloc=r'(.*\.)?indico\.corn(.*)', path="/export/event/")
def indico_event_details_mock(url, request):
    headers = {'content-type': 'application/json'}

    return response(200, json.dumps(WEBCAST_DETAILS), headers, None, 5, request)


@urlmatch(netloc=r'(.*\.)?indico\.corn(.*)', path="/export/webcast-recording.json")
def indico_webcasts_mock(url, request):
    headers = {'content-type': 'application/json'}

    return response(200, json.dumps(MULTIPLE_INDICO_WEBCASTS), headers, None, 5, request)
