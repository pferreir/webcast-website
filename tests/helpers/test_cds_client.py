import os
from flask import current_app
from pathlib2 import Path
from app.models.events import CDSRecord
from app.plugins.cds_client import CdsClient
from tests import BaseTestCase
from httmock import urlmatch, response, HTTMock


# http://indico.corn/export/event/622398.json
@urlmatch(netloc=r'(.*\.)?cdsweb\.cern\.ch(.*)')
def cds_recent_request(url, request):
    headers = {'content-type': 'text/xml'}
    dir = os.path.dirname(__file__)
    parent = os.path.abspath(os.path.join(dir, os.pardir))

    contents = Path(os.path.join(parent, "samples", "cds.xml")).read_text()

    return response(200, contents, headers, None, 5, request)


class CdsClientTest(BaseTestCase):

    def test_fetch_recent_events(self):
        with HTTMock(cds_recent_request):

            result = CdsClient().fetch_recent_events()
            self.assertNotEqual(result, "")

            self.assertEqual(len(result), 12)

    def test_get_recent_events(self):
        with HTTMock(cds_recent_request):

            CdsClient().get_recent_events()
            cds_records = CDSRecord.query.all()
            self.assertEqual(len(cds_records), 12)

    # def test_get_recent_events(self):
    #     with HTTMock(cds_recent_request):
    #
    #         CdsClient.get_recent_events()
    #         cds_records = CDSRecord.query.all()
    #         self.assertEqual(len(cds_records), 12)


