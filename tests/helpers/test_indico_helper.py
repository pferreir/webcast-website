from httmock import HTTMock
from sqlalchemy.orm.exc import NoResultFound

from app.handlers.events import EventUpcomingHandler
from app.models.events import Event
from app.services.indico import IndicoService
from tests import BaseTestCase
from tests.helpers import indico_event_details_mock, indico_webcasts_mock, EVENTS_JSON


class IndicoServiceTest(BaseTestCase):
    def test_indico_fetch_upcoming_events(self):
        with HTTMock(indico_webcasts_mock):
            indico_service = IndicoService()
            result = indico_service.fetch_upcoming_events()

            self.assertEqual(result, EVENTS_JSON)

    def test_indico_get_next_contribution(self):
        with HTTMock(indico_webcasts_mock):
            indico_id = 1234
            indico_service = IndicoService()
            result = indico_service.get_next_contribution(indico_id=indico_id)

            self.assertEqual(result, None)

    def test_create_event_from_json(self):
        with HTTMock(indico_event_details_mock):

            event_json = {
              'audience': 'ATLAS collaborators only',
              '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
              'room': 'Salle Curie',
              'services': [
                'webcast',
                'recording'
              ],
              'event_id': 622398,
              'status': 'A',
              'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
              'endDate': {
                'date': '2017-03-14',
                'tz': 'UTC',
                'time': '14:19:00'
              },
              'location': 'CERN',
              'room_full_name': '40-S2-C01 - Salle Curie',
              'title': 'ATLAS Weekly - Measurement of the $tbar{t}gamma$ production cross section at  $sqrt{s} = 8$ Tekern -0.1em V in $20.2;mathrm{fb}^{-1}$ of $pp$ collision  data collected with the ATLAS detector',
              'startDate': {
                'date': '2017-03-14',
                'tz': 'UTC',
                'time': '14:15:00'
              }
            }
            event_upcoming_handler = EventUpcomingHandler()
            event_upcoming_handler.create_event_from_json(event_json)

            try:
                event = Event.query.get(1)
                self.assertIsNotNone(event)
                self.assertEqual(event.indico_id, '622398')
            except NoResultFound:
                self.assertTrue(False, "Event was not created")





