/* eslint-disable import/no-cycle */
import {
  configureStore,
  ConfigureStoreOptions,
  combineReducers,
  Middleware,
} from "@reduxjs/toolkit";
import { cdsRecordsApi } from "api/cds-records-api";
import { userApi } from "api/user-api";
import { webcastsApi } from "api/webcasts-api";
import auth from "reducers/auth";

const rootReducer = combineReducers({
  auth,
  [cdsRecordsApi.reducerPath]: cdsRecordsApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [webcastsApi.reducerPath]: webcastsApi.reducer,
});

const middlewares: Middleware[] = [
  webcastsApi.middleware,
  userApi.middleware,
  cdsRecordsApi.middleware,
];

export const createStore = (
  options?: ConfigureStoreOptions["preloadedState"] | undefined,
) =>
  configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(middlewares),
    devTools: process.env.NODE_ENV !== "production",
    ...options,
  });
const store = createStore();
export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof rootReducer>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
export type AppStore = ReturnType<typeof createStore>;
