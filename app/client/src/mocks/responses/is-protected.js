export const isProtectedProtectedResponse = {
  success: true,
  result: {
    is_restricted: true,
  },
};

export const isProtectedNotProtectedResponse = {
  success: true,
  result: {
    is_restricted: false,
  },
};

export default {
  isProtectedProtectedResponse,
  isProtectedNotProtectedResponse,
};
