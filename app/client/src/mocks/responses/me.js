export const userMe = {
  email: "jonh.doe@cern.ch",
  first_name: "John",
  last_name: "Doe",
  uid: "username",
  roles: ["cern-accounts-primary", "test-webcast-externals"],
};

export default userMe;
