import React from "react";
import ReactPiwik from "react-piwik";
import { useSelector } from "react-redux";
import { Router, Route, Switch } from "react-router-dom";
// eslint-disable-next-line import/no-extraneous-dependencies
import { createBrowserHistory } from "history";
import LoggingIn from "./components/LoginIn/LoginIn";
import LoginPrompt from "./components/LoginPrompt";
import config from "./config";
import HomePage from "./pages/HomePage/HomePage";
import PermanentWebcasts from "./pages/PermanentWebcasts";
import WebcastPage from "./pages/WebcastPage/WebcastPage";
import { isLoginWindowOpen } from "./selectors/auth";
import PreviewPage from "pages/PreviewPage/PreviewPage";

const { PIWIK_ID } = config;

const piwik = new ReactPiwik({
  url: "piwik.web.cern.ch",
  siteId: Number(PIWIK_ID),
});

const history = createBrowserHistory();

function App() {
  const loggingIn = useSelector(isLoginWindowOpen);

  return (
    <Router history={piwik.connectToHistory(history)}>
      <Switch>
        <Route exact path="/" component={HomePage} />
        {/* Beware order matters */}
        <Route exact path="/event/i:indicoId" component={WebcastPage} />
        <Route exact path="/view/:code/:indicoId" component={PreviewPage} />
        <Route path="/permanent-webcasts" component={PermanentWebcasts} />
        <Route render={() => <div>This page does not exist</div>} />
      </Switch>
      <LoginPrompt />
      {loggingIn && <LoggingIn />}
    </Router>
  );
}

export default App;
