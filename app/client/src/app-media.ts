import { createMedia } from "@artsy/fresnel";

export const breakPoints = {
  mobile: 320,
  tablet: 768,
  computer: 992,
  largeScreen: 1200,
  widescreen: 1920,
};

const AppMedia = createMedia({
  breakpoints: breakPoints,
});

const mediaStyles = AppMedia.createMediaStyle();
const { Media } = AppMedia;

export { mediaStyles, AppMedia, Media };
