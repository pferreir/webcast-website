import React, { PropsWithChildren } from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router";
import type { PreloadedState } from "@reduxjs/toolkit";
import { render } from "@testing-library/react";
import type { RenderOptions } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { createStore, AppStore, RootState } from "../store";
import { AppMedia, mediaStyles } from "app-media";

// This type interface extends the default options for render from RTL, as well
// as allows the user to specify other things such as initialState, store. For
// future dependencies, such as wanting to test with react-router, you can extend
// this interface to accept a path and route and use those in a <MemoryRouter />
interface ExtendedRenderOptions extends Omit<RenderOptions, "queries"> {
  preloadedState?: PreloadedState<RootState>;
  store?: AppStore;
  route?: string;
  history?: ReturnType<typeof createMemoryHistory>;
}

function renderWithProviders(
  ui: React.ReactElement,
  {
    preloadedState = {},
    route = "/",
    store = createStore(preloadedState),
    history = createMemoryHistory({ initialEntries: [route] }),
    ...renderOptions
  }: ExtendedRenderOptions = {},
) {
  function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
    const { MediaContextProvider } = AppMedia;

    return (
      <MediaContextProvider>
        <style>{mediaStyles}</style>
        <Provider store={store}>
          <MemoryRouter>{children}</MemoryRouter>
        </Provider>
      </MediaContextProvider>
    );
  }
  return {
    store,
    ...render(ui, { wrapper: Wrapper, ...renderOptions }),
    history,
  };
}

export { renderWithProviders };

export default renderWithProviders;
