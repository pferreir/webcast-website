import { RootState } from "store";

export const getToken = (state: RootState) => state.auth.token;
export const getLoginWindowId = (state: RootState) => state.auth.windowId;
export const isLoginWindowOpen = (state: RootState) => !!state.auth.windowId;
export const isLoggedIn = (state: RootState) => !!state.auth.token;
export const isAcquiringToken = (state: RootState) =>
  !!state.auth.acquiringToken;
