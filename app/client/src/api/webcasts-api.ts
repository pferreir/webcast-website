// eslint-disable-next-line import/no-cycle
import { baseApi } from "./api";
import {
  IProtectedWebcastCheck,
  IWebcastApiResponse,
  IWebcastCategoryListApiResponse,
  IWebcastListApiResponse,
} from "types/webcast";

type WebcastType = "live" | "recent" | "upcoming";

// Define a service using a base URL and expected endpoints
export const webcastsApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getWebcasts: builder.query<IWebcastListApiResponse, string>({
      query: (webcastType: WebcastType) => `webcasts/?type=${webcastType}`,
    }),
    getWebcastById: builder.query<IWebcastApiResponse, string>({
      query: (webcastId: string) => `webcasts/${webcastId}`,
    }),
    getPublicWebcastById: builder.query<IWebcastApiResponse, string>({
      query: (webcastId: string) => `webcasts/public/${webcastId}`,
    }),
    getIsWebcastProtected: builder.query<IProtectedWebcastCheck, string>({
      query: (webcastId: string) => `webcasts/${webcastId}/is-protected`,
    }),
    getPermanentWebcastById: builder.query<IWebcastApiResponse, string>({
      query: (webcastId: string) => `webcasts/permanent-webcasts/${webcastId}`,
    }),
    getPermanentWebcastCategories: builder.query<
      IWebcastCategoryListApiResponse,
      void
    >({
      query: () => `webcasts/permanent-categories/`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useGetWebcastByIdQuery,
  useGetWebcastsQuery,
  useGetPublicWebcastByIdQuery,
  useGetIsWebcastProtectedQuery,
  useGetPermanentWebcastByIdQuery,
  useGetPermanentWebcastCategoriesQuery,
} = webcastsApi;
