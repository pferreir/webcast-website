// eslint-disable-next-line import/no-cycle
import { baseApi } from "./api";
import { ICdsRecordListApiResponse } from "types/cds-record";

// Define a service using a base URL and expected endpoints
export const cdsRecordsApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getCdsRecords: builder.query<ICdsRecordListApiResponse, void>({
      query: () => `webcasts/cds-records`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetCdsRecordsQuery } = cdsRecordsApi;
