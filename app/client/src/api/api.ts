// eslint-disable-next-line import/no-cycle
import {
  BaseQueryFn,
  createApi,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from "@reduxjs/toolkit/dist/query/react";
import { tokenExpired, tokenNeeded } from "actions/token";
import config from "config";
// eslint-disable-next-line import/no-cycle
import { RootState } from "store";

const prepareApiHeaders = (headers: Headers, { getState }: any) => {
  const { token } = (getState() as RootState).auth;
  if (token) {
    headers.set("Authorization", `Bearer ${token}`);
  }
  return headers;
};

const baseQuery = fetchBaseQuery({
  baseUrl: `${config.api.ENDPOINT}/api/private/v1/`,
  prepareHeaders: prepareApiHeaders,
});

interface IApiError {
  error: string;
}

const baseQueryWithAuth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    const errorResponse = result.error.data as IApiError;
    if (errorResponse && errorResponse.error) {
      if (errorResponse.error === "token_expired") {
        console.log("Asking user to login again");
        api.dispatch(tokenExpired());
      } else if (errorResponse.error === "token_invalid") {
        console.log("Asking user to login");
        api.dispatch(tokenNeeded());
      }
    }
  }
  return result;
};

export const baseApi = createApi({
  reducerPath: "api",
  baseQuery: baseQueryWithAuth,
  endpoints: () => ({}),
});

export default prepareApiHeaders;
