import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import { checkInitialToken, subscribeTokenChanges } from "./auth";
import reportWebVitals from "./reportWebVitals";
import store from "./store";
import "semantic-ui-css/semantic.min.css";
import "video.js/dist/video-js.css";
import "pure-react-carousel/dist/react-carousel.es.css";
import "./index.css";
import { AppMedia, mediaStyles } from "app-media";

if (process.env.NODE_ENV === "production") {
  console.log = () => {};
  console.debug = () => {};
}

if (process.env.NODE_ENV !== "production") {
  // eslint-disable-next-line global-require
  const axe = require("@axe-core/react");
  axe(React, ReactDOM, 1000);
}

const { MediaContextProvider } = AppMedia;

checkInitialToken(store);
subscribeTokenChanges(store);

ReactDOM.render(
  <React.StrictMode>
    <style>{mediaStyles}</style>
    <MediaContextProvider>
      <Provider store={store}>
        <App />
      </Provider>
    </MediaContextProvider>
  </React.StrictMode>,
  document.getElementById("root"),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
