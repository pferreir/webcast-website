export const TOKEN_EXPIRED = "Expired token needs refresh";
export const TOKEN_NEEDED = "Login required to send request";

export const ADD_ERROR = "Error occurred";
export const REMOVE_ERROR = "Remove error";
export const CLEAR_ERRORS = "Clear all the errors";

export function tokenExpired() {
  return { type: TOKEN_EXPIRED };
}

export function tokenNeeded() {
  return { type: TOKEN_NEEDED };
}
