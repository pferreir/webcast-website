import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  loginPromptAborted,
  loginWindowClosed,
  loginWindowOpened,
  userLogin,
  userLogout,
} from "./auth";
import { tokenExpired, tokenNeeded } from "./token";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

function successLogin() {
  return {
    type: "User logged in",
    token: "12345",
  };
}

function successLogout() {
  return [
    {
      type: "User logged out",
    },
  ];
}

const oldWindowLocation = window.location;

beforeAll(() => {
  delete window.location;
  window.location = { ...oldWindowLocation, assign: jest.fn() };
});

afterAll(() => {
  window.location = oldWindowLocation;
});

afterEach(() => {
  jest.clearAllMocks();
});

describe("Sync auth actions tests", () => {
  it("opens a login window", () => {
    const store = mockStore({});
    const result = store.dispatch(loginWindowOpened("1234") as any);
    expect(result).toEqual({ id: "1234", type: "Login window opened" });
  });

  it("closes a login window", () => {
    const store = mockStore({});
    const result = store.dispatch(loginWindowClosed());
    expect(result).toEqual({ id: undefined, type: "Login window closed" });
  });

  it("aborts a login window", () => {
    const store = mockStore({});
    const result = store.dispatch(loginPromptAborted());
    expect(result).toEqual({ id: undefined, type: "User refused to login" });
  });

  it("expires token", () => {
    const store = mockStore({});
    const result = store.dispatch(tokenExpired());
    expect(result).toEqual({
      id: undefined,
      type: "Expired token needs refresh",
    });
  });

  it("requires token", () => {
    const store = mockStore({});
    const result = store.dispatch(tokenNeeded());
    expect(result).toEqual({
      id: undefined,
      type: "Login required to send request",
    });
  });
});

describe("Async auth actions tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("logs in a user", () => {
    const store = mockStore({
      auth: { token: "12345" },
      api: { executeQuery: { pending: false } },
    });

    // Return the promise
    return store.dispatch(userLogin("12345") as any).then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual(successLogin());
    });
  });

  it("logs out a user", () => {
    const store = mockStore({});

    // Return the promise
    return store.dispatch(userLogout() as any).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(successLogout());
    });
  });
});
