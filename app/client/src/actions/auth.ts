import config from "../config";
// eslint-disable-next-line import/no-cycle
import { userApi } from "api/user-api";
// eslint-disable-next-line import/no-cycle
import { AppDispatch } from "store";

export const LOGIN_WINDOW_OPENED = "Login window opened";
export const LOGIN_WINDOW_CLOSED = "Login window closed";
export const LOGIN_PROMPT_ABORTED = "User refused to login";
export const USER_LOGIN = "User logged in";
export const USER_LOGOUT = "User logged out";
export const USER_RECEIVED = "User info received";

export function loginWindowOpened(id: string) {
  return { type: LOGIN_WINDOW_OPENED, id };
}

export function loginWindowClosed() {
  return { type: LOGIN_WINDOW_CLOSED };
}

export function loginPromptAborted() {
  return { type: LOGIN_PROMPT_ABORTED };
}

export function userLogin(token: string) {
  return async (dispatch: AppDispatch) => {
    dispatch({ type: USER_LOGIN, token });
    dispatch(userApi.endpoints.getMe.initiate());
  };
}

export function userLogout() {
  return async (dispatch: AppDispatch) => {
    dispatch({ type: USER_LOGOUT });
    const hostname = `${window.location.protocol}//${window.location.host}`;
    window.location.href = `${config.api.ENDPOINT}/logout/?next_url=${hostname}`;
  };
}
