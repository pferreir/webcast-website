import { IAppConfig } from "types/config";

const dev: IAppConfig = {
  api: {
    ENDPOINT: "http://localhost:5000",
  },
  IS_DEV_INSTALL: process.env.REACT_APP_IS_DEV_INSTALL === "true",
  PIWIK_ID: "3722",
  USE_VIDEOJS: false,
  SINGLE_STREAMS: true,
};

const prod: IAppConfig = {
  api: {
    ENDPOINT: process.env.REACT_APP_API_ENDPOINT
      ? process.env.REACT_APP_API_ENDPOINT
      : "",
  },
  IS_DEV_INSTALL: process.env.REACT_APP_IS_DEV_INSTALL === "true",
  PIWIK_ID: process.env.REACT_APP_PIWIK_ID
    ? process.env.REACT_APP_PIWIK_ID
    : "0",
  USE_VIDEOJS: false,
  SINGLE_STREAMS: true,
};

const testConfig: IAppConfig = {
  api: {
    ENDPOINT: "http://localhost:5000",
  },
  IS_DEV_INSTALL: true,
  PIWIK_ID: "0",
  USE_VIDEOJS: false,
  SINGLE_STREAMS: true,
};

let tempConfig = testConfig;

if (process.env.NODE_ENV === "production") {
  tempConfig = prod;
}
if (process.env.NODE_ENV === "development") {
  tempConfig = dev;
}

const appConfig = tempConfig;

export default appConfig;
