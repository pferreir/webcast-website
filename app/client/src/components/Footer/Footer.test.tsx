import React from "react";
import { screen } from "@testing-library/react";
import { Footer } from "./Footer";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("Footer component tests", () => {
  it("renders all the footer texts", () => {
    renderWithProviders(<Footer />);
    let element = screen.getByText(/About/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Webcast Service/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Webcast Test Channel/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Subscribe/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Live events/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Upcoming events/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Contact$/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Report an issue/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Contact CERN/i);
    expect(element).toBeInTheDocument();
  });
});
