import React from "react";
import { screen } from "@testing-library/react";
import OldWebBanner from "./OldWebBanner";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("OldWebBanner component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(<OldWebBanner />);
    const element = screen.getByText(
      /If you want to access the old website, it is still available here/i,
    );
    expect(element).toBeInTheDocument();
  });
});
