import React from "react";
import { Menu, Icon } from "semantic-ui-react";

export default function OldWebBanner() {
  return (
    <Menu color="blue" inverted widths={1} className="blueMenu">
      <Menu.Item>
        <Icon name="exclamation triangle" />{" "}
        <a href="https://webcast.web.cern.ch/old/" title="Old webcast website">
          If you want to access the old website, it is still available here
        </a>
      </Menu.Item>
    </Menu>
  );
}
