import React from "react";
import { screen } from "@testing-library/react";
import DevBanner from "./DevBanner";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("DevBanner component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(<DevBanner />);
    const element = screen.getByText(/This is a DEV Installation/i);
    expect(element).toBeInTheDocument();
  });
});
