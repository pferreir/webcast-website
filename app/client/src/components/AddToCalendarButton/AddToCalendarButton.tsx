import React from "react";
import PropTypes from "prop-types";
import { Icon } from "semantic-ui-react";

interface IProps {
  indicoId: string;
  displayIcon?: boolean;
  displayAddIcon?: boolean;
}

export default function AddToCalendarButton({
  indicoId,
  displayIcon = true,
  displayAddIcon = false,
}: IProps) {
  return (
    <>
      {displayIcon ? <Icon name="calendar" /> : null}
      {displayAddIcon ? <Icon name="add" /> : null}
      <a
        href={`https://indico.cern.ch/event/${indicoId}/event.ics`}
        title="Add the event to your calendar"
      >
        Add to calendar
      </a>
    </>
  );
}

AddToCalendarButton.propTypes = {
  indicoId: PropTypes.string.isRequired,
  displayIcon: PropTypes.bool,
  displayAddIcon: PropTypes.bool,
};

AddToCalendarButton.defaultProps = {
  displayIcon: null,
  displayAddIcon: null,
};
