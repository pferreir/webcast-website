import React from "react";
import { screen } from "@testing-library/react";
import AddToCalendarButton from "./AddToCalendarButton";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("AddToCalendarButton component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(<AddToCalendarButton indicoId="12345" />);
    const element = screen.getByText(/Add to calendar/i);
    expect(element).toBeInTheDocument();
  });
});
