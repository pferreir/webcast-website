import React from "react";
import { screen } from "@testing-library/react";
import LoginIn from "./LoginIn";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("LoginIn component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(<LoginIn />);
    const element = screen.getByText(/Logging in.../i);
    expect(element).toBeInTheDocument();
  });
});
