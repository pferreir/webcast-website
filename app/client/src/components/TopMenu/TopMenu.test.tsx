import React from "react";
import { screen } from "@testing-library/react";
import TopMenu from "./TopMenu";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("TopMenu component tests", () => {
  it("renders all the texts", () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    renderWithProviders(<TopMenu />, { preloadedState });
    let element = screen.getByText(/CERN/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Accelerating Science/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Login/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Directory/i);
    expect(element).toBeInTheDocument();
  });
});
