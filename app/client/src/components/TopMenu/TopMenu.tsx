import React from "react";
import { useSelector } from "react-redux";
import { Menu, Container } from "semantic-ui-react";
import { useGetMeQuery } from "api/user-api";
import { Media } from "app-media";
import { useAuthentication } from "auth";
import { isLoggedIn } from "selectors/auth";

export default function TopMenu() {
  const isUserLoggedIn = useSelector(isLoggedIn);
  const { login, logout } = useAuthentication();
  const { data: user } = useGetMeQuery();
  if (isUserLoggedIn) {
    return (
      <Menu inverted fixed="top" style={{ display: "block" }}>
        <Container fluid>
          <Menu.Item
            name="CERN"
            as="a"
            href="https://home.cern"
            title="CERN Home"
          />
          <Menu.Item
            as={Media}
            greaterThanOrEqual="tablet"
            name="Accelerating Science"
          />
          <Menu.Menu position="right">
            <Menu.Item name="loggedin">
              Logged in as:{" "}
              {user ? `${user.first_name} ${user.last_name} (${user.uid})` : ""}
            </Menu.Item>
            <Menu.Item name="logout" onClick={logout}>
              Logout
            </Menu.Item>
            <Menu.Item
              as="a"
              href="https://cern.ch/directory"
              title="CERN Directory"
            >
              Directory
            </Menu.Item>
          </Menu.Menu>
        </Container>
      </Menu>
    );
  }

  return (
    <Menu inverted fixed="top" style={{ display: "block" }}>
      <Container fluid>
        <Menu.Item
          name="CERN"
          as="a"
          href="https://home.cern"
          title="CERN Home"
        />
        <Menu.Item name="Accelerating Science" />
        <Menu.Menu position="right">
          <Menu.Item name="signup" onClick={login}>
            Login
          </Menu.Item>
        </Menu.Menu>
        <Menu.Item
          as="a"
          href="https://cern.ch/directory"
          title="CERN Directory"
        >
          Directory
        </Menu.Item>
      </Container>
    </Menu>
  );
}
