import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Modal } from "semantic-ui-react";
import { loginPromptAborted } from "../actions/auth";
import { useAuthentication } from "../auth";
import { isAcquiringToken, isLoggedIn } from "../selectors/auth";

export default function LoginPrompt() {
  const acquiringToken = useSelector(isAcquiringToken);
  const loggedIn = useSelector(isLoggedIn);
  const { login, logout } = useAuthentication();
  const history = useHistory();
  const dispatch = useDispatch();

  if (!acquiringToken) {
    return null;
  }
  if (loggedIn) {
    // refreshing expired token
    return (
      <Modal
        open
        size="mini"
        header="Your session expired"
        content="Please log in again to confirm your identity"
        actions={[
          { key: "login", content: "Login", positive: true, onClick: login },
          { key: "logout", content: "Logout", onClick: logout },
        ]}
      />
    );
  }
  // (fresh) login required
  return (
    <Modal
      open
      size="mini"
      header="Login required"
      content="You need to log in to access this page"
      actions={[
        { key: "login", content: "Login", positive: true, onClick: login },
        {
          key: "cancel",
          content: "Cancel",
          onClick: () => {
            history.push("/");
            dispatch(loginPromptAborted());
          },
        },
      ]}
    />
  );
}
