import React, { useState, useEffect } from "react";

interface IProps {
  cameraUrl?: string;
  slidesUrl?: string;
  playerUrl: string;
  streamType: string;
  eventType: string;
  hasCode?: boolean;
}

export default function WebcastIframe({
  cameraUrl,
  slidesUrl,
  playerUrl,
  streamType,
  eventType,
  hasCode = false,
}: IProps) {
  const iframeId = "webcastIframe";
  const [queryParams, setQueryParams] = useState("");
  useEffect(() => {
    function receiveMessage(event: any) {
      if (event.data === "give-me-sources") {
        console.log("Player asking for resources...");
        const exists = new RegExp("cern.ch$").test(event.origin);
        // console.log(exists, event.origin);
        if (!exists) {
          return;
        }
        // console.log('Received callback!');
        console.log("Sending sources to iframe");
        const myFrame: any = document.getElementById(iframeId);
        myFrame.contentWindow.postMessage(
          {
            slidesUrl,
            cameraUrl,
          },
          myFrame.src,
        );
      }
    }

    // console.log('OTHERUSEEFFECT');
    // console.log(eventType);
    let tempParams = "";

    if (streamType.toLowerCase() === "camera") {
      // console.log('IS ONLY CAMERA');
      tempParams += "&camera=true";
    } else if (streamType.toLowerCase() === "slides") {
      tempParams += "&slides=true";
    }

    if (eventType.toLowerCase() === "live" || hasCode) {
      // console.log('is live!');
      tempParams += "&live=true";
    }
    setQueryParams(tempParams);

    window.addEventListener("message", receiveMessage, false);

    return () => {
      window.removeEventListener("message", receiveMessage);
    };
  }, [cameraUrl, slidesUrl, streamType, eventType, hasCode]);

  return (
    <div
      id="Container"
      className="video-container"
      data-testid="main-container"
    >
      <iframe
        data-testid="iframe"
        title="Main webcast Iframe"
        id={iframeId}
        width="100%"
        height="100%"
        src={`${playerUrl}/?mode=webcast${queryParams}`}
        frameBorder="0"
        allow="fullscreen"
        className="iframePaella"
      />
    </div>
  );
}

WebcastIframe.defaultProps = {
  slidesUrl: null,
  hasCode: false,
  cameraUrl: null,
};
