import React from "react";
import { render, screen } from "@testing-library/react";
import WebcastIframe from "./WebcastIframe";

describe("WebcastIframe component tests", () => {
  it("renders properly the iframe for upcoming", () => {
    render(
      <WebcastIframe
        streamType="Camera"
        eventType="Upcoming"
        playerUrl="https://webcast-test.web.cern.ch"
        cameraUrl="https://path.to.smil.file"
      />,
    );
    let element = screen.getByTestId("main-container");
    expect(element).toBeInTheDocument();
    element = screen.getByTestId("iframe");
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute(
      "src",
      "https://webcast-test.web.cern.ch/?mode=webcast&camera=true",
    );
  });
  it("renders properly the iframe for live", () => {
    render(
      <WebcastIframe
        streamType="Camera"
        eventType="Live"
        playerUrl="https://webcast-test.web.cern.ch"
        cameraUrl="https://path.to.smil.file"
      />,
    );
    let element = screen.getByTestId("main-container");
    expect(element).toBeInTheDocument();
    element = screen.getByTestId("iframe");
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute(
      "src",
      "https://webcast-test.web.cern.ch/?mode=webcast&camera=true&live=true",
    );
  });
  it("renders properly the iframe for live with camera and slides", () => {
    render(
      <WebcastIframe
        streamType="Camera & Slides"
        eventType="Live"
        playerUrl="https://webcast-test.web.cern.ch"
        cameraUrl="https://path.to.smil.file"
        slidesUrl="https://path.to.smil.file.for.slides"
      />,
    );
    let element = screen.getByTestId("main-container");
    expect(element).toBeInTheDocument();
    element = screen.getByTestId("iframe");
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute(
      "src",
      "https://webcast-test.web.cern.ch/?mode=webcast&live=true",
    );
  });
  it("renders properly the iframe for live with slides", () => {
    render(
      <WebcastIframe
        streamType="Slides"
        eventType="Live"
        playerUrl="https://webcast-test.web.cern.ch"
        slidesUrl="https://path.to.smil.file.for.slides"
      />,
    );
    let element = screen.getByTestId("main-container");
    expect(element).toBeInTheDocument();
    element = screen.getByTestId("iframe");
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute(
      "src",
      "https://webcast-test.web.cern.ch/?mode=webcast&slides=true&live=true",
    );
  });
});
