import React from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import {
  Segment,
  Dimmer,
  Loader,
  Header,
  Icon,
  Label,
} from "semantic-ui-react";
import { useGetPermanentWebcastByIdQuery } from "api/webcasts-api";
import ShareWebcastSection from "components/ShareWebcastSection/ShareWebcastSection";
import WebcastIframe from "components/WebcastIframe/WebcastIframe";
import { IWebcastCategory } from "types/webcast";

interface IProps {
  selectedCategory: IWebcastCategory;
}

interface IParams {
  webcastId: string;
}

export default function PermanentWebcastDetails({ selectedCategory }: IProps) {
  const { webcastId } = useParams<IParams>();
  const { data, isError, isLoading } =
    useGetPermanentWebcastByIdQuery(webcastId);

  if (isLoading) {
    return (
      <Segment basic textAlign="center">
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    );
  }

  if (isError) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the webcast details
      </Segment>
    );
  }

  if (data && data.result) {
    return (
      <div>
        <Header as="h3">
          <Icon name="video" />
          <Header.Content>
            {data.result.title} <Label>{selectedCategory.name}</Label>
          </Header.Content>
        </Header>
        <p>
          <Link to={`/permanent-webcasts/${selectedCategory.id}`}>
            <Icon name="arrow left" /> Back to category
          </Link>
        </p>

        <WebcastIframe
          cameraUrl={data.result.stream_src}
          eventType="live"
          streamType="camera"
          playerUrl={data.result.player_url}
        />
        <ShareWebcastSection webcast={data.result} inverted={false} />
      </div>
    );
  }
  return null;
}
