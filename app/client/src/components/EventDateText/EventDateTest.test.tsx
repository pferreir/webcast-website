import React from "react";
import { screen } from "@testing-library/react";
import EventDateText from "./EventDateText";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("EventDateText component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(
      <EventDateText
        date="2021-11-16"
        timezone="Europe/Zurich"
        time="17:15:00"
      />,
    );
    let element = screen.getByText(/16-11-2021 17:15/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Europe\/Zurich/i);
    expect(element).toBeInTheDocument();
  });
});
