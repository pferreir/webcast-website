import React from "react";
import { DateTime } from "luxon";
import { Icon } from "semantic-ui-react";

interface IProps {
  date: string;
  time: string;
  timezone: string | null;
}

export default function EventDateText({ date, time, timezone = null }: IProps) {
  return (
    <span>
      <Icon name="clock" />{" "}
      {DateTime.fromISO(`${date}T${time}`).toFormat("dd-MM-yyyy HH:mm")} (
      {timezone}){" "}
    </span>
  );
}
