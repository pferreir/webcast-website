import React from "react";
import { screen } from "@testing-library/react";
import ShareWebcastSection from "./ShareWebcastSection";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("DevBanner component tests", () => {
  it("renders all the texts", () => {
    renderWithProviders(
      <ShareWebcastSection
        webcast={{
          embed_code: "This is the embed URL",
          title: "",
          streams: [],
        }}
      />,
    );
    const element = screen.getByText(/This is the embed URL/i);
    expect(element).toBeInTheDocument();
  });
});
