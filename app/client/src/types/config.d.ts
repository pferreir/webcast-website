interface IApiConfig {
  ENDPOINT: string;
}

export interface IAppConfig {
  api: IApiConfig;
  IS_DEV_INSTALL: boolean;
  PIWIK_ID: string;
  USE_VIDEOJS: boolean;
  SINGLE_STREAMS: boolean;
}
