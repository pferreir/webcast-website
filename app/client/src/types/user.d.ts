export interface IUser {
  email: string;
  first_name: string;
  last_name: string;
  uid: string;
  roles: string[];
}
