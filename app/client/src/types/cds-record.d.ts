export interface ICdsRecord {
  title: string;
  image_url: string;
  speakers: string;
  published_date: string;
  link: string;
}

export interface ICdsRecordListApiResponse {
  results: ICdsRecord[];
  success: boolean;
}
