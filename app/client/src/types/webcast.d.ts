export interface IStream {}

export interface IWebcast {
  [key: string]: any;
  embed_code: string;
  title: string;
  streams: Stream[];
}

export interface IWebcastCategory {
  id: number;
  name: string;
  streams: IWebcast[];
}

export interface IWebcastCategoryListApiResponse {
  results: IWebcastCategory[];
  success: boolean;
}

export interface IWebcastApiResponse {
  result: IWebcast;
  success: boolean;
}

export interface IWebcastListApiResponse {
  results: IWebcast[];
  success: boolean;
}

interface IProtectedWebcastResult {
  is_restricted: boolean;
}

export interface IProtectedWebcastCheck {
  success: boolean;
  result: IProtectedWebcastResult;
}
