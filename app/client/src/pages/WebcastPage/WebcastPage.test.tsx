import React from "react";
import Router from "react-router-dom";
import { screen, waitFor } from "@testing-library/react";
import WebcastPage from "./WebcastPage";
import { renderWithProviders } from "utils/test-rtk-utils";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("WebcastPage component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders all the texts", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1160270" });

    await waitFor(() => {
      renderWithProviders(<WebcastPage />, { preloadedState });
    });

    let element = screen.getByText(/Upcoming Webcasts/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Recent Webcasts/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Permanent Webcasts/i);
    expect(element).toBeInTheDocument();
  });
});
