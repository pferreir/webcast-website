import React from "react";
import { Segment, Grid, Container } from "semantic-ui-react";
import WebcastProtectionCheck from "./components/WebcastProtectionCheck/WebcastProtectionCheck";
import TopMenu from "components/TopMenu/TopMenu";
import ResponsiveContainer from "pages/ResponsiveContainer";

export default function WebcastPage() {
  return (
    <ResponsiveContainer>
      <TopMenu />
      <Segment basic>
        <Container>
          <Grid columns={1}>
            <WebcastProtectionCheck />
          </Grid>
        </Container>
      </Segment>
    </ResponsiveContainer>
  );
}
