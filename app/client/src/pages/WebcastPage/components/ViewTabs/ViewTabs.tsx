import React, { useEffect } from "react";
import { Icon, Menu, Tab } from "semantic-ui-react";
import videojs from "video.js";
import { VideoJS } from "../VideoJS/VideoJS";
import WebcastIframe from "components/WebcastIframe/WebcastIframe";
import config from "config";
import { IWebcast } from "types/webcast";

interface IProps {
  code?: string;
  webcast: IWebcast;
}

interface ISinglePlayerProps {
  code?: string;
  webcast: IWebcast;
  stream: "camera" | "slides";
}

export const SinglePlayerComponent = ({
  webcast,
  code,
  stream,
}: ISinglePlayerProps) => {
  const { USE_VIDEOJS } = config;
  console.log("stream", stream);

  const [cameraUrl, setCameraUrl] = React.useState<string | undefined>(
    undefined,
  );
  const [slidesUrl, setSlidesUrl] = React.useState<string | undefined>(
    undefined,
  );
  const [streamUrl, setStreamUrl] = React.useState<string | undefined>(
    undefined,
  );

  useEffect(() => {
    if (stream === "camera") {
      setCameraUrl(webcast.stream.camera_src);
      setSlidesUrl(undefined);
      setStreamUrl(webcast.stream.camera_src);
    } else {
      setCameraUrl(undefined);
      setSlidesUrl(webcast.stream.slides_src);
      setStreamUrl(webcast.stream.slides_src);
    }
  }, [stream, webcast.stream.camera_src, webcast.stream.slides_src]);

  const commonOptions = {
    autoplay: false,
    responsive: true,
    liveui: webcast.stream.type.toLowerCase() === "live" || !!code,
    controls: true,
    fluid: true,
    html5: {
      vhs: {
        overrideNative: !videojs.browser.IS_SAFARI,
      },
      nativeAudioTracks: false,
      nativeVideoTracks: false,
    },
  };

  if (USE_VIDEOJS) {
    return (
      <VideoJS
        options={{
          // lookup the options in the docs for more options
          ...commonOptions,
          sources: [
            {
              src: streamUrl,
              type: "application/x-mpegURL",
            },
          ],
        }}
      />
    );
  }
  return (
    <WebcastIframe
      playerUrl={webcast.stream.player_url}
      streamType={stream}
      cameraUrl={cameraUrl}
      slidesUrl={slidesUrl}
      eventType={webcast.type}
      hasCode={!!code}
    />
  );
};

export default function ViewTabs({ webcast, code }: IProps) {
  const { SINGLE_STREAMS } = config;

  const tabPanes = [
    {
      key: "1",
      menuItem: (
        <Menu.Item key="1">
          <Icon name="video" />
          <Icon name="film" /> Main view
        </Menu.Item>
      ),
      render: () => (
        <Tab.Pane inverted>
          <WebcastIframe
            playerUrl={webcast.stream.player_url}
            streamType={webcast.stream.type}
            cameraUrl={webcast.stream.camera_src}
            slidesUrl={webcast.stream.slides_src}
            eventType={webcast.type}
            hasCode={!!code}
          />
        </Tab.Pane>
      ),
    },
    {
      key: "2",
      menuItem: (
        <Menu.Item key="2">
          <Icon name="video" /> Camera
        </Menu.Item>
      ),
      render: () => (
        <Tab.Pane inverted>
          <SinglePlayerComponent
            webcast={webcast}
            code={code}
            stream="camera"
          />
        </Tab.Pane>
      ),
    },
    {
      key: "3",
      menuItem: (
        <Menu.Item key="3">
          <Icon name="film" /> Slides
        </Menu.Item>
      ),
      render: () => (
        <Tab.Pane inverted>
          <SinglePlayerComponent
            webcast={webcast}
            code={code}
            stream="slides"
          />
        </Tab.Pane>
      ),
    },
  ];

  const getPanes = () => {
    const panes = [];
    if (webcast && webcast.stream) {
      if (webcast.stream.type.toLowerCase() === "camera & slides") {
        panes.push(tabPanes[0]);
        if (SINGLE_STREAMS) {
          panes.push(tabPanes[1]);
          panes.push(tabPanes[2]);
        }
      } else if (webcast.stream.type.toLowerCase() === "camera") {
        if (SINGLE_STREAMS) {
          panes.push(tabPanes[1]);
        } else {
          panes.push(tabPanes[0]);
        }
      } else if (webcast.stream.type.toLowerCase() === "slides") {
        if (SINGLE_STREAMS) {
          panes.push(tabPanes[2]);
        } else {
          panes.push(tabPanes[0]);
        }
      }
    }

    return panes;
  };

  if (!webcast || !webcast.stream) {
    return <div>Loading...</div>;
  }

  return (
    <Tab
      menu={{ inverted: true }}
      panes={getPanes()}
      webcast={webcast}
      code={code}
    />
  );
}

SinglePlayerComponent.defaultProps = {
  code: undefined,
};

ViewTabs.defaultProps = {
  code: undefined,
};
