// @ts-nocheck
import React from "react";
import { Segment, List, Image, Icon } from "semantic-ui-react";
import { breakPoints } from "app-media";
import AddToCalendarButton from "components/AddToCalendarButton/AddToCalendarButton";
import EventDateText from "components/EventDateText/EventDateText";
import { getWidth } from "utils/width";

interface IParams {
  webcast: IWebcast;
}

export default function WebcastInfoSection({ webcast }: IParams) {
  const currentWidth = getWidth();
  return (
    <Segment inverted>
      <List inverted horizontal={currentWidth > breakPoints.mobile} relaxed>
        <List.Item>
          <a href={webcast.indico_link} title="Event Indico Link">
            <Image
              src="/images-client/indico.svg"
              alt="Indico Logo"
              size="tiny"
              className="indicoImageLink"
            />
          </a>
        </List.Item>
        <List.Item>
          <Icon name="location arrow" size="big" className="imaged" />
          <List.Content>
            <List.Header>Location</List.Header>
            {webcast.room_name ? webcast.room_name : "-"}
          </List.Content>
        </List.Item>
        <List.Item>
          <Icon name="bullhorn" size="big" className="imaged" />
          <List.Content>
            <List.Header>Speaker(s)</List.Header>
            {webcast.speakers ? webcast.speakers : "-"}
          </List.Content>
        </List.Item>
        <List.Item>
          <Icon name="calendar" size="big" className="imaged" />
          <List.Content>
            <List.Header>Date</List.Header>
            <EventDateText
              date={webcast.startDate.day}
              time={webcast.startDate.time}
              timezone={webcast.startDate.tz}
            />
            <AddToCalendarButton
              indicoId={webcast.indico_id}
              displayIcon={false}
              displayAddIcon
            />
          </List.Content>
        </List.Item>
      </List>
    </Segment>
  );
}
