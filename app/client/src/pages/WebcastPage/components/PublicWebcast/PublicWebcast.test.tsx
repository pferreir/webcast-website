import React from "react";
import { screen, waitFor } from "@testing-library/react";
import PublicWebcast from "./PublicWebcast";
import { renderWithProviders } from "utils/test-rtk-utils";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("PublicWebcast component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders the loader", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(<PublicWebcast indicoId="1127820" />, {
        preloadedState,
      });
    });

    const element = screen.getByTestId(/loader/i);
    expect(element).toBeInTheDocument();
  });

  it("renders the webcast texts", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    // jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1234" });

    await waitFor(() => {
      renderWithProviders(<PublicWebcast indicoId="1127820" />, {
        preloadedState,
      });
    });

    await waitFor(() => {
      const element = screen.getByText(
        /Nonperturbative Methods in Quantum Field Theory/i,
      );
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Speaker/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/iframe/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getAllByText(/Europe\/Zurich/i);
      expect(element).toHaveLength(2);
    });
    await waitFor(() => {
      const element = screen.getByText(/Speaker/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/This event will start on/i);
      expect(element).toBeInTheDocument();
    });
    // element = screen.getByText(/Recent Webcasts/i);
    // expect(element).toBeInTheDocument();
    // element = screen.getByText(/Permanent Webcasts/i);
    // expect(element).toBeInTheDocument();
  });
});
