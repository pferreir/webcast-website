import React, { useEffect, useState } from "react";
import WebcastContent from "../WebcastContent/WebcastContent";
import WebcastErrorContent from "../WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "../WebcastLoadingContent/WebcastLoadingContent";
import { useGetPublicWebcastByIdQuery } from "api/webcasts-api";
import { IWebcast } from "types/webcast";

interface Props {
  indicoId: string;
}

export default function PublicWebcast({ indicoId }: Props) {
  const [webcast, setWebcast] = useState<IWebcast | undefined>(undefined);

  const {
    data: webcastData,
    error,
    isFetching,
  } = useGetPublicWebcastByIdQuery(`i${indicoId}`);

  useEffect(() => {
    // Set the webcast after fetching
    if (webcastData && webcastData.result) {
      setWebcast(webcastData.result);
    }
  }, [webcastData]);

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
    if (webcast) {
      document.title = `${webcast.title}- CERN Webcast Website`;
    }
  }, [webcast]);

  return (
    <div data-testid="public-webcast">
      {error && <WebcastErrorContent error={error} />}
      {isFetching && <WebcastLoadingContent />}
      {webcast && <WebcastContent webcast={webcast} />}
    </div>
  );
}
