import React from "react";
import { DateTime } from "luxon";
import {
  Grid,
  Header,
  Icon,
  Message,
  Placeholder,
  Segment,
} from "semantic-ui-react";
import AbstractSection from "../AbstractSection/AbstractSection";
import ViewTabs from "../ViewTabs/ViewTabs";
import WebcastInfoSection from "../WebcastInfoSection/WebcastInfoSection";
import ShareWebcastSection from "components/ShareWebcastSection/ShareWebcastSection";
import { IWebcast } from "types/webcast";

interface Props {
  webcast: IWebcast;
  code?: string;
}

interface NotStartedProps {
  day: string;
  time: string;
  tz: string;
}

export const PrivateWebcastMessage = () => (
  <Message color="yellow">
    <span role="img" aria-label="Warning icon">
      ⚠️{" "}
    </span>
    <strong>This is a private view of the event.</strong>
  </Message>
);

export const WebcastHeader = ({
  webcast,
  code,
}: {
  webcast: IWebcast;
  code: string | undefined;
}) => (
  <Grid.Column>
    {code && <PrivateWebcastMessage />}
    <Header inverted as="h2" dividing>
      <Icon name="video" />
      <Header.Content>
        {webcast.title}
        {webcast.speakers && (
          <Header.Subheader>by {webcast.speakers}</Header.Subheader>
        )}
      </Header.Content>
    </Header>
  </Grid.Column>
);

export const WebcastNotLiveContent = ({ day, time, tz }: NotStartedProps) => (
  <Segment inverted>
    <Message info>
      <Icon name="info" /> This event will start on{" "}
      {DateTime.fromISO(`${day}T${time}`).toFormat("dd-MM-yyyy HH:mm")} ({tz}) .
      You will need to reload the page.
    </Message>

    <Placeholder fluid inverted style={{ maxHeight: 360 }}>
      <Placeholder.Image rectangular />
    </Placeholder>
  </Segment>
);

export default function WebcastContent({ webcast, code }: Props) {
  return (
    <>
      <Grid.Row>
        <WebcastHeader webcast={webcast} code={code} />
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          {webcast.extra_html !== "" && webcast.extra_html !== null && (
            <Message info>
              <Icon name="info" />{" "}
              <span
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{
                  __html: webcast.extra_html,
                }}
              />
            </Message>
          )}
          {webcast.type.toLowerCase() === "live" ||
          webcast.type.toLowerCase() === "recent" ||
          (webcast.type.toLowerCase() === "upcoming" && code) ? (
            <ViewTabs webcast={webcast} code={code} />
          ) : (
            <WebcastNotLiveContent
              day={webcast.startDate.day}
              time={webcast.startDate.time}
              tz={webcast.startDate.tz}
            />
          )}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <WebcastInfoSection webcast={webcast} />
          <AbstractSection webcast={webcast} inverted />
          <ShareWebcastSection webcast={webcast} inverted />
        </Grid.Column>
      </Grid.Row>
    </>
  );
}

WebcastContent.defaultProps = {
  code: undefined,
};
