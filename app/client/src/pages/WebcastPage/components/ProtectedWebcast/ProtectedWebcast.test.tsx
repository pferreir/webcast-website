import React from "react";
import { screen, waitFor } from "@testing-library/react";
import { rest } from "msw";
import ProtectedWebcast from "./ProtectedWebcast";
import config from "config";
import { server } from "mocks/server";
import { renderWithProviders } from "utils/test-rtk-utils";

const {
  api: { ENDPOINT },
} = config;

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("ProtectedWebcast component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders the loader", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(<ProtectedWebcast indicoId="1160270" />, {
        preloadedState,
      });
    });

    const element = screen.getByTestId(/loader/i);
    expect(element).toBeInTheDocument();
  });

  it("renders the webcast texts", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    // jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1234" });

    await waitFor(() => {
      renderWithProviders(<ProtectedWebcast indicoId="1160270" />, {
        preloadedState,
      });
    });

    await waitFor(() => {
      const element = screen.getByText(/ATLAS Weekly/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Speaker/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/iframe/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getAllByText(/Europe\/Zurich/i);
      expect(element).toHaveLength(2);
    });
    await waitFor(() => {
      const element = screen.getByText(/Speaker/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/This event will start on/i);
      expect(element).toBeInTheDocument();
    });
    // element = screen.getByText(/Recent Webcasts/i);
    // expect(element).toBeInTheDocument();
    // element = screen.getByText(/Permanent Webcasts/i);
    // expect(element).toBeInTheDocument();
  });

  it("renders the error message", async () => {
    server.use(
      // Model any response overrides you need.
      rest.get(
        `${ENDPOINT}/api/private/v1/webcasts/i1160270`,
        (req, res, ctx) => res(ctx.status(401)),
      ),
    );

    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(<ProtectedWebcast indicoId="1160270" />, {
        preloadedState,
      });
    });
    await waitFor(() => {
      const element = screen.getByText(
        /You need to log in to access this webcast/i,
      );
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Unable to fetch the selected webcast/i);
      expect(element).toBeInTheDocument();
    });
  });
});
