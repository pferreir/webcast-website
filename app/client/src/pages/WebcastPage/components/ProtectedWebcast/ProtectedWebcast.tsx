import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import WebcastContent from "../WebcastContent/WebcastContent";
import WebcastErrorContent from "../WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "../WebcastLoadingContent/WebcastLoadingContent";
import { useGetWebcastByIdQuery } from "api/webcasts-api";
import { isLoggedIn } from "selectors/auth";
import { IWebcast } from "types/webcast";

interface Props {
  indicoId: string;
}

export default function ProtectedWebcast({ indicoId }: Props) {
  const [webcast, setWebcast] = useState<IWebcast | undefined>(undefined);
  const [allowed, setAllowed] = useState<boolean>(true);

  const isUserLoggedIn = useSelector(isLoggedIn);

  const {
    data: protectedWebcast,
    error,
    isFetching,
    refetch,
  } = useGetWebcastByIdQuery(`i${indicoId}`);

  useEffect(() => {
    // Set the webcast after fetching
    if (protectedWebcast && protectedWebcast.result) {
      if (protectedWebcast.success) {
        setWebcast(protectedWebcast.result);
      } else {
        setAllowed(false);
      }
    }
  }, [protectedWebcast]);

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
    if (webcast) {
      document.title = `${webcast.title}- CERN Webcast Website`;
    }
  }, [webcast]);

  useEffect(() => {
    // Refetch the webcast after the user logs in
    if (isUserLoggedIn) {
      refetch();
    }
  }, [isUserLoggedIn, refetch]);

  return (
    <div data-testid="protected-webcast">
      {error && <WebcastErrorContent error={error} />}
      {!allowed && <WebcastErrorContent error={{ status: 403, data: {} }} />}
      {isFetching && <WebcastLoadingContent />}
      {webcast && <WebcastContent webcast={webcast} />}
    </div>
  );
}
