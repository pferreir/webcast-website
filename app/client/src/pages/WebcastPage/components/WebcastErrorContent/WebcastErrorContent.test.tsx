import React from "react";
import { screen, waitFor } from "@testing-library/react";
import WebcastErrorContent from "./WebcastErrorContent";
import { renderWithProviders } from "utils/test-rtk-utils";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("WebcastErrorContent component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders the login error", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(
        <WebcastErrorContent error={{ status: 401, data: "Error" }} />,
        {
          preloadedState,
        },
      );
    });

    const element = screen.getByText(
      /You need to log in to access this webcast/i,
    );
    expect(element).toBeInTheDocument();
  });
  it("renders the normal error", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(
        <WebcastErrorContent error={{ status: 500, data: "Error" }} />,
        {
          preloadedState,
        },
      );
    });

    const element = screen.getByText(/Unable to fetch the selected webcast/i);
    expect(element).toBeInTheDocument();
  });
});
