import React from "react";
import { SerializedError } from "@reduxjs/toolkit";
import { FetchBaseQueryError } from "@reduxjs/toolkit/dist/query";
import { Grid, Icon, Message, Segment } from "semantic-ui-react";

interface Props {
  error: FetchBaseQueryError | SerializedError | undefined;
}

export default function WebcastErrorContent({ error }: Props) {
  return (
    <Grid.Row>
      <Grid.Column>
        <Segment inverted textAlign="center">
          <Message error>
            <Message.Header>
              <Icon name="warning sign" />
              Unable to fetch the selected webcast
            </Message.Header>
            <Message.Content>
              {error && "status" in error && error.status === 401 && (
                <p>You need to log in to access this webcast</p>
              )}
              {error && "status" in error && error.status === 403 && (
                <p>You don&apos;t have permission to access this webcast</p>
              )}
            </Message.Content>
          </Message>
        </Segment>
      </Grid.Column>
    </Grid.Row>
  );
}
