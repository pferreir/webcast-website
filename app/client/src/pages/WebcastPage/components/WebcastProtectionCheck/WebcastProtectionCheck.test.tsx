import React from "react";
import Router from "react-router-dom";
import { screen, waitFor } from "@testing-library/react";
import { rest } from "msw";
import WebcastProtectionCheck from "./WebcastProtectionCheck";
import config from "config";
import { server } from "mocks/server";
import { renderWithProviders } from "utils/test-rtk-utils";

const {
  api: { ENDPOINT },
} = config;

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("WebcastProtectionCheck component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders the loader", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1160270" });

    await waitFor(() => {
      renderWithProviders(<WebcastProtectionCheck />, {
        preloadedState,
      });
    });

    const element = screen.getByText(/Checking webcast restrictions.../i);
    expect(element).toBeInTheDocument();
  });

  it("renders the public webcast texts", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1127820" });

    await waitFor(() => {
      renderWithProviders(<WebcastProtectionCheck />, {
        preloadedState,
      });
    });

    await waitFor(() => {
      const element = screen.getByText(
        /Nonperturbative Methods in Quantum Field Theory/i,
      );
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByTestId(/public-webcast/i);
      expect(element).toBeInTheDocument();
    });
  });

  it("renders the private webcast texts", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };
    jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1160270" });

    await waitFor(() => {
      renderWithProviders(<WebcastProtectionCheck />, {
        preloadedState,
      });
    });

    await waitFor(() => {
      const element = screen.getByText(/ATLAS Weekly/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByTestId(/protected-webcast/i);
      expect(element).toBeInTheDocument();
    });
  });

  it("renders the error message", async () => {
    jest.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1160270" });

    server.use(
      // Model any response overrides you need.
      rest.get(
        `${ENDPOINT}/api/private/v1/webcasts/i1160270/is-protected`,
        (req, res, ctx) => res(ctx.status(500)),
      ),
    );

    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(<WebcastProtectionCheck />, {
        preloadedState,
      });
    });
    await waitFor(() => {
      const element = screen.getByText(/Error getting webcast restrictions./i);
      expect(element).toBeInTheDocument();
    });
  });
});
