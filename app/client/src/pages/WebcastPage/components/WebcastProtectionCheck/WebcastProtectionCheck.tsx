import React from "react";
import { useParams } from "react-router-dom";
import { Grid } from "semantic-ui-react";
import ProtectedWebcast from "../ProtectedWebcast/ProtectedWebcast";
import PublicWebcast from "../PublicWebcast/PublicWebcast";
import { useGetIsWebcastProtectedQuery } from "api/webcasts-api";

interface IParams {
  indicoId: string;
}

export default function WebcastProtectionCheck() {
  const { indicoId } = useParams<IParams>();

  const {
    data: isProtectedData,
    error,
    isFetching,
  } = useGetIsWebcastProtectedQuery(`i${indicoId}`);

  return (
    <Grid.Row>
      <Grid.Column>
        {error && <div>Error getting webcast restrictions.</div>}
        {isFetching && <div>Checking webcast restrictions...</div>}
        {isProtectedData &&
          isProtectedData.result &&
          isProtectedData.result.is_restricted && (
            <ProtectedWebcast indicoId={indicoId} />
          )}
        {isProtectedData &&
          isProtectedData.result &&
          !isProtectedData.result.is_restricted && (
            <PublicWebcast indicoId={indicoId} />
          )}
      </Grid.Column>
    </Grid.Row>
  );
}
