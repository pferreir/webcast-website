/* eslint-disable jsx-a11y/media-has-caption */
import React from "react";
import videojs from "video.js";
import qualityLevels from "videojs-contrib-quality-levels";
import qualitySelector from "videojs-hls-quality-selector";

interface IVideoJSProps {
  onReady?: Function;
  options: any;
}

export const VideoJS = ({ options, onReady }: IVideoJSProps) => {
  const videoRef = React.useRef<HTMLVideoElement | null>(null);
  const playerRef = React.useRef<videojs.Player | null>(null);

  React.useEffect(() => {
    // make sure Video.js player is only initialized once
    if (!playerRef.current) {
      const videoElement = videoRef.current;
      if (!videoElement) return;

      if (!videojs.getPlugin("qualityLevels")) {
        videojs.registerPlugin("qualityLevels", qualityLevels);
      }
      if (!videojs.getPlugin("hlsQualitySelector")) {
        videojs.registerPlugin("hlsQualitySelector", qualitySelector);
      }
      // eslint-disable-next-line no-multi-assign
      const player = videojs(videoElement, options, () => {
        console.log("player is ready");
        // eslint-disable-next-line no-unused-expressions
        if (onReady) onReady(player);
      });
      playerRef.current = player;
    } else {
      // you can update player here [update player through props]
      const player = playerRef.current;
      // player.autoplay(options.autoplay);
      player.src(options.sources);
    }
  }, [onReady, options, videoRef]);

  // Dispose the Video.js player when the functional component unmounts
  React.useEffect(() => {
    const player = playerRef.current;
    if (player) player.hlsQualitySelector({ displayCurrentQuality: true });
    return () => {
      if (player) {
        console.log("Unmount player");
        player.dispose();
        playerRef.current = null;
      }
    };
  }, [playerRef]);

  return (
    <div data-vjs-player>
      <video ref={videoRef} className="video-js vjs-big-play-centered" />
    </div>
  );
};

VideoJS.defaultProps = {
  onReady: undefined,
};

export default VideoJS;
