import React from "react";
import { screen, waitFor } from "@testing-library/react";
import WebcastLoadingContent from "./WebcastLoadingContent";
import { renderWithProviders } from "utils/test-rtk-utils";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
}));

describe("WebcastLoadingContent component tests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders the loader", async () => {
    const preloadedState = {
      auth: {
        token: false,
      },
    };

    await waitFor(() => {
      renderWithProviders(<WebcastLoadingContent />, {
        preloadedState,
      });
    });

    const element = screen.getByTestId(/loader/i);
    expect(element).toBeInTheDocument();
  });
});
