import React from "react";
import PropTypes from "prop-types";
import { Container } from "semantic-ui-react";
import DesktopContainer from "./HomePage/components/DesktopContainer";
import MobileContainer from "./HomePage/components/MobileContainer";
import { Media } from "app-media";
import DevBanner from "components/DevBanner/DevBanner";

interface IProps {
  children: React.ReactNode;
}

const ResponsiveContainer = ({ children }: IProps) => {
  return (
    <>
      <Media greaterThanOrEqual="tablet">
        <DesktopContainer>
          <Container fluid>
            <DevBanner />
          </Container>
          {children}
        </DesktopContainer>
      </Media>
      <Media lessThan="tablet">
        <MobileContainer>
          <Container fluid>
            <DevBanner />
          </Container>
          {children}
        </MobileContainer>
      </Media>
    </>
  );
};

ResponsiveContainer.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
};

export default ResponsiveContainer;
