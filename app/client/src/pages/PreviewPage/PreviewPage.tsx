import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Segment, Grid, Container } from "semantic-ui-react";
import { useGetPublicWebcastByIdQuery } from "api/webcasts-api";
import TopMenu from "components/TopMenu/TopMenu";
import ResponsiveContainer from "pages/ResponsiveContainer";
import WebcastContent from "pages/WebcastPage/components/WebcastContent/WebcastContent";
import WebcastErrorContent from "pages/WebcastPage/components/WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "pages/WebcastPage/components/WebcastLoadingContent/WebcastLoadingContent";
import { IWebcast } from "types/webcast";

interface IParams {
  indicoId: string;
  code?: string;
}

export default function PreviewPage() {
  const { indicoId, code } = useParams<IParams>();
  const [webcast, setWebcast] = useState<IWebcast | undefined>(undefined);

  const {
    data: webcastData,
    isFetching,
    error,
  } = useGetPublicWebcastByIdQuery(`p${code}-${indicoId}`);

  useEffect(() => {
    if (webcastData && webcastData.result) {
      setWebcast(webcastData.result);
    }
  }, [webcastData]);

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
    if (webcast) {
      document.title = `${webcast.title}- CERN Webcast Website`;
    }
  }, [webcast]);

  return (
    <ResponsiveContainer>
      <TopMenu />
      <Segment basic>
        <Container>
          <Grid columns={1}>
            <Grid.Row>
              <Grid.Column>
                {error && <WebcastErrorContent error={error} />}
                {isFetching && <WebcastLoadingContent />}
                {webcast && <WebcastContent webcast={webcast} code={code} />}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Segment>
    </ResponsiveContainer>
  );
}
