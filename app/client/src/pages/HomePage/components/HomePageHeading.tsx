import React from "react";
import { Container } from "semantic-ui-react";
import WebcastCarousel from "./WebcastCarousel";

const HomepageHeading = () => (
  <Container
    style={{
      padding: "2em 0 0 0",
    }}
  >
    <WebcastCarousel />
  </Container>
);

export default HomepageHeading;
