import React from "react";
import { useHistory } from "react-router";
import { DateTime } from "luxon";
import PropTypes from "prop-types";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  Dot,
} from "pure-react-carousel";
import {
  Header,
  Button,
  Icon,
  Container,
  Segment,
  Loader,
  SemanticSIZES,
} from "semantic-ui-react";
import { useGetWebcastsQuery } from "api/webcasts-api";
import { Media } from "app-media";
import { readMore } from "utils/string_utils";
/**
 * https://github.com/express-labs/pure-react-carousel#readme
 * @param {*} param0
 */

interface IDotGroupProps {
  slides: any[];
  size: SemanticSIZES | undefined;
}

const CustomDotGroup = ({ slides, size }: IDotGroupProps) => (
  <Container textAlign="center">
    <Button.Group size={size}>
      {slides.map((slide: any, index: number) => (
        <Button
          as={Dot}
          key={index.toString()}
          icon="circle"
          slide={index.toString()}
          inverted
          basic
          className="sliderDot"
        />
      ))}
    </Button.Group>
  </Container>
);

interface IWebcastSlideProps {
  webcast: any;
}

const WebcastSlide = ({ webcast }: IWebcastSlideProps) => {
  const history = useHistory();

  const goToWebcast = (indicoId: string) => {
    history.push(`/event/i${indicoId}`);
  };

  return (
    <>
      <Header
        as="h2"
        content={webcast.title}
        onClick={() => goToWebcast(webcast.indico_id)}
        title={`Watch ${webcast.title} webcast`}
        className="live-header-link"
        inverted
        style={{
          fontWeight: "normal",
        }}
      />
      <Media greaterThan="tablet">
        <div className="slideText">
          <p>
            <Icon name="clock" />
            {DateTime.fromISO(
              `${webcast.startDate.day}T${webcast.startDate.time}`,
            ).toFormat("dd-MM-yyyy HH:mm")}{" "}
            ({webcast.startDate.tz})
          </p>
          <p>
            {webcast.title.split("").length > 10
              ? readMore(webcast.abstract, 20)
              : readMore(webcast.abstract, 30)}
          </p>
        </div>
      </Media>
      <Media greaterThan="mobile" className="sliderButton">
        <Button
          primary
          size="big"
          className="cernPrimary"
          title="Watch this webcast"
          onClick={() => goToWebcast(webcast.indico_id)}
        >
          {webcast.restricted ? <Icon name="lock" /> : ""} Watch webcast
          <Icon name="arrow right" />
        </Button>
      </Media>
    </>
  );
};

const WebcastCarousel = () => {
  const { data, error, isLoading } = useGetWebcastsQuery("live");

  if (isLoading) {
    return <Loader active inverted inline="centered" />;
  }

  if (error) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the Live webcasts
      </Segment>
    );
  }

  if (data && data.results.length === 1) {
    return (
      <div key={0}>
        <WebcastSlide webcast={data.results[0]} />
      </div>
    );
  }

  if (data && data.results && data.results.length > 1) {
    return (
      <CarouselProvider
        naturalSlideWidth={1160}
        naturalSlideHeight={250}
        totalSlides={data.results.length}
        isPlaying
        infinite
      >
        <Slider aria-label="Live webcasts carousel">
          {data &&
            data.results.map((webcast, index) => (
              <Slide key={index.toString()} index={index}>
                <WebcastSlide webcast={webcast} />
              </Slide>
            ))}
        </Slider>

        <ButtonBack>
          <Icon name="chevron left" size="big" />
        </ButtonBack>
        <ButtonNext>
          <Icon name="chevron right" size="big" />
        </ButtonNext>
        <CustomDotGroup slides={data.results} size="mini" />
      </CarouselProvider>
    );
  }
  return null;
};

WebcastSlide.propTypes = {
  webcast: PropTypes.shape({
    title: PropTypes.string,
    abstract: PropTypes.string,
    indico_id: PropTypes.string,
    startDate: PropTypes.shape({
      day: PropTypes.string,
      time: PropTypes.string,
      tz: PropTypes.string,
    }),
    restricted: PropTypes.bool,
  }).isRequired,
};

export default WebcastCarousel;
