import React, { ReactNode, useState } from "react";
import { NavLink } from "react-router-dom";
import {
  Visibility,
  Segment,
  Menu,
  Container,
  Header,
  Image,
  Dropdown,
} from "semantic-ui-react";
import { Footer } from "components/Footer/Footer";

interface IProps {
  headerComponent?: ReactNode | null;
  children: ReactNode[];
}
const DesktopContainer = ({ headerComponent, children }: IProps) => {
  const [fixed, setFixed] = useState(false);
  return (
    <>
      <header role="banner">
        <Visibility
          once={false}
          onBottomPassed={() => setFixed(true)}
          onBottomPassedReverse={() => setFixed(false)}
        >
          <Segment
            inverted
            textAlign="center"
            style={{
              padding: "2em 0em",
              backgroundSize: "cover",
              backgroundImage: "url(/images-client/lhc-blur.jpg)",
              backgroundPosition: "center center",
              marginTop: 40,
            }}
            vertical
          >
            <nav role="navigation">
              <Menu
                inverted={!fixed}
                pointing={!fixed}
                secondary={!fixed}
                size="large"
                className="mainMenu"
              >
                <Container>
                  <Menu.Item>
                    <Header as="h1" inverted>
                      <Image
                        src="/images-client/Logo-Outline-web-White@200.png"
                        alt="CERN Logo"
                      />
                      <Header.Content>
                        CERN
                        <Header.Subheader>Webcast Website</Header.Subheader>
                      </Header.Content>
                    </Header>
                  </Menu.Item>
                  <Menu.Item position="right">
                    <Menu.Item as={NavLink} exact to="/">
                      Home
                    </Menu.Item>
                    <Dropdown
                      item
                      text="Webcasts"
                      aria-label="Expand webcasts section"
                    >
                      <Dropdown.Menu>
                        <Dropdown.Item as="a" href="/#live-webcasts">
                          Live Webcasts
                        </Dropdown.Item>
                        <Dropdown.Item as="a" href="/#upcoming-webcasts">
                          Upcoming Webcasts
                        </Dropdown.Item>
                        <Dropdown.Item as="a" href="/#recent-webcasts">
                          Recent Webcasts
                        </Dropdown.Item>
                        <Dropdown.Item as={NavLink} to="/permanent-webcasts">
                          Permanent Webcasts
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                    <Menu.Item
                      as="a"
                      href="https://cds.cern.ch"
                      title="CERN Document Server"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      CDS Archive
                    </Menu.Item>
                  </Menu.Item>
                </Container>
              </Menu>
            </nav>
            {headerComponent}
          </Segment>
        </Visibility>
      </header>
      <main role="main">{children}</main>

      <footer role="contentinfo">
        <Footer />
      </footer>
    </>
  );
};

DesktopContainer.defaultProps = {
  headerComponent: null,
};

export default DesktopContainer;
