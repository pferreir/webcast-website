import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import {
  Sidebar,
  Menu,
  Segment,
  Container,
  Icon,
  Header,
} from "semantic-ui-react";
import { Footer } from "components/Footer/Footer";
import config from "config";
// import { getWidth } from "utils/width";

/**
 *
 * @param {*} props
 */
const MobileContainer = (props) => {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const { children, headerComponent } = props;
  return (
    <Sidebar.Pushable>
      <Sidebar
        as={Menu}
        animation="push"
        inverted
        onHide={() => setSidebarOpen(false)}
        vertical
        visible={sidebarOpen}
      >
        <Menu.Item as={NavLink} exact to="/" title="Home">
          Home
        </Menu.Item>
        <Menu.Item
          as={NavLink}
          exact
          to="/permanent-webcasts"
          title="Permanent Webcasts"
        >
          Permanent Webcasts
        </Menu.Item>
        <Menu.Item
          as="a"
          href="https://cds.cern.ch"
          title="CERN Document Server"
        >
          CDS Archive
        </Menu.Item>
      </Sidebar>

      <Sidebar.Pusher dimmed={sidebarOpen}>
        <Segment
          inverted
          textAlign="center"
          className={`App ${config.IS_DEV_INSTALL ? "paddedApp" : ""}`}
          style={{
            // minHeight: headerComponent ? 350 : 0,
            padding: "1em 0em",
            backgroundSize: "cover",
            backgroundImage: "url(/images-client/lhc-blur.jpg)",
            backgroundPosition: "center center",
          }}
          vertical
        >
          <Container>
            <Menu inverted pointing secondary size="large" className="mainMenu">
              <Menu.Item onClick={() => setSidebarOpen(true)}>
                <Icon name="sidebar" />
              </Menu.Item>
              <Menu.Item>
                <Header as="h1" inverted textAlign="center">
                  <Header.Content>
                    CERN <small>Webcast Website</small>
                  </Header.Content>
                </Header>
              </Menu.Item>
            </Menu>
          </Container>
          {headerComponent}
        </Segment>

        {children}

        <footer role="contentinfo">
          <Footer />
        </footer>
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  );
};

MobileContainer.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  headerComponent: PropTypes.node,
};

MobileContainer.defaultProps = {
  headerComponent: null,
};

export default MobileContainer;
