import React, { useState, useEffect } from "react";
// eslint-disable-next-line import/no-extraneous-dependencies
import { useHistory } from "react-router-dom";
// eslint-disable-next-line import/no-extraneous-dependencies
import { chunk } from "lodash";
import {
  Grid,
  Item,
  Button,
  Icon,
  Segment,
  Dimmer,
  Loader,
  Image,
} from "semantic-ui-react";
import { useGetWebcastsQuery } from "api/webcasts-api";
import EventDateText from "components/EventDateText/EventDateText";
import { IWebcast } from "types/webcast";

interface IProps {
  columns: number;
}

export default function RecentWebcasts({ columns }: IProps) {
  const history = useHistory();
  const { data, error, isLoading } = useGetWebcastsQuery("recent");
  const [rows, setRows] = useState<any>([]);

  useEffect(() => {
    if (data && data.results) {
      setRows(chunk(data.results, columns));
    }
  }, [columns, setRows, data]);

  const goToWebcast = (indicoId: string) => {
    history.push(`/event/i${indicoId}`);
  };

  const getWebcastColum = (webcast: IWebcast, index: number) => (
    <Grid.Column key={`recent-${index}`}>
      <Item.Group>
        <Item>
          <Item.Image
            size="tiny"
            src={webcast.img}
            alt={`Image of ${webcast.title}`}
          />
          <Item.Content>
            <Item.Header as="h3" style={{ fontSize: "1.2em" }}>
              {webcast.title}
            </Item.Header>
            {webcast.speakers && <Item.Meta>by {webcast.speakers}</Item.Meta>}
            <Item.Description>
              <EventDateText
                date={webcast.startDate.day}
                time={webcast.startDate.time}
                timezone={webcast.startDate.tz}
              />
            </Item.Description>
            <Item.Extra className="accExtra">
              <Button
                primary
                size="tiny"
                floated="right"
                className="cernPrimary"
                onClick={() => goToWebcast(webcast.indico_id)}
              >
                {webcast.restricted ? <Icon name="lock" /> : ""} Watch again
                <Icon name="chevron right" />
              </Button>
              <a href={webcast.indico_link} title="Event link">
                <Image
                  src="/images-client/indico.svg"
                  alt="Indico Logo"
                  size="tiny"
                />
              </a>
            </Item.Extra>
          </Item.Content>
        </Item>
      </Item.Group>
    </Grid.Column>
  );

  if (isLoading) {
    return (
      <Segment basic textAlign="center">
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    );
  }

  if (error) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the recent webcasts
      </Segment>
    );
  }

  return (
    <>
      {rows && rows.length > 0 && (
        <Grid columns={2} divided>
          {rows.map((cols: IWebcast[], index: number) => (
            <Grid.Row key={`recent-col-${index.toString()}`}>
              {cols.map((webcast: IWebcast, index2: number) =>
                getWebcastColum(webcast, index2),
              )}
            </Grid.Row>
          ))}
        </Grid>
      )}
      {rows.length === 0 && (
        <Segment basic textAlign="center">
          There are no recent webcasts.
        </Segment>
      )}
    </>
  );
}
