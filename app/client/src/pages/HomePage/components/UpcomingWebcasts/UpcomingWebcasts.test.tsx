/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/react-in-jsx-scope */
import { screen } from "@testing-library/react";
import UpcomingWebcasts from "./UpcomingWebcasts";
import { mockWebcastList } from "mocks/responses/webcasts";
import { renderWithProviders } from "utils/test-rtk-utils";

describe("UpcomingWebcasts component tests", () => {
  it("renders all the texts", async () => {
    const preloadedState = {
      auth: {
        token: "12345",
        acquiringToken: false,
        windowId: null,
      },
    };
    renderWithProviders(<UpcomingWebcasts />, { preloadedState });
    let element = await screen.findByText(mockWebcastList.results[0].title);
    expect(element).toBeInTheDocument();
    element = screen.getByText(mockWebcastList.results[1].title);
    expect(element).toBeInTheDocument();

    let elements = screen.getAllByText(/Add to calendar/);
    expect(elements.length).toBe(6);

    elements = screen.getAllByText(/Europe\/Zurich/);
    expect(elements.length).toBe(6);

    elements = screen.getAllByText(/More Information/);
    expect(elements.length).toBe(6);

    elements = screen.getAllByText(/by/);
    expect(elements.length).toBe(4);
  });
});
