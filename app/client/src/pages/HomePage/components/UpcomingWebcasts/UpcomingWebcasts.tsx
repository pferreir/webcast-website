import React from "react";
import { useHistory } from "react-router";
import {
  Item,
  Button,
  Icon,
  Image,
  Segment,
  Loader,
  Dimmer,
} from "semantic-ui-react";
import { useGetWebcastsQuery } from "api/webcasts-api";
import AddToCalendarButton from "components/AddToCalendarButton/AddToCalendarButton";
import EventDateText from "components/EventDateText/EventDateText";
import { readMore } from "utils/string_utils";

export default function UpcomingWebcasts() {
  const { data, error, isLoading } = useGetWebcastsQuery("upcoming");
  const history = useHistory();

  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (error) {
    return <div>Error: {JSON.stringify(error)}</div>;
  }

  const goToWebcast = (indicoId: string) => {
    console.log(`Load webcast ${indicoId}`);
    history.push(`/event/i${indicoId}`);
  };

  if (isLoading) {
    return (
      <Segment basic textAlign="center">
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    );
  }

  if (error) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the upcoming webcasts
      </Segment>
    );
  }

  if (data && data.results.length === 0) {
    return (
      <Segment basic textAlign="center">
        There are no upcoming webcasts.
      </Segment>
    );
  }

  return (
    <Item.Group divided>
      {data &&
        data.results.map((webcast, index) => (
          <Item key={`upcoming-${index.toString()}`}>
            <Item.Image
              size="small"
              src={webcast.img}
              alt={`Image of ${webcast.title}`}
            />
            <Item.Content>
              <Item.Header as="h3" style={{ fontSize: "1.5em" }}>
                {webcast.title}
              </Item.Header>
              {webcast.speakers && <Item.Meta>by {webcast.speakers}</Item.Meta>}
              <Item.Description>
                <EventDateText
                  date={webcast.startDate.day}
                  time={webcast.startDate.time}
                  timezone={webcast.startDate.tz}
                />
                <AddToCalendarButton indicoId={webcast.indico_id} />
              </Item.Description>
              {!webcast.restricted ? (
                <Item.Extra className="accExtra">
                  {readMore(webcast.abstract, 40)}
                </Item.Extra>
              ) : null}
              <Item.Extra className="accExtra">
                <Button
                  primary
                  floated="right"
                  className="cernPrimary"
                  onClick={() => goToWebcast(webcast.indico_id)}
                >
                  {webcast.restricted ? <Icon name="lock" /> : ""} More
                  Information
                  <Icon name="chevron right" />
                </Button>
                <a href={webcast.indico_link} title="Event link">
                  <Image
                    src="/images-client/indico.svg"
                    alt="Indico Logo"
                    size="tiny"
                  />
                </a>
              </Item.Extra>
            </Item.Content>
          </Item>
        ))}
    </Item.Group>
  );
}
