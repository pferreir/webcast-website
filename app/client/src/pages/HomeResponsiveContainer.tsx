import React, { ReactNode, useEffect } from "react";
import PropTypes from "prop-types";
import { Container } from "semantic-ui-react";
import HomepageHeading from "./HomePage/components/HomePageHeading";
import MobileContainer from "./HomePage/components/MobileContainer";
import { Media } from "app-media";
import DevBanner from "components/DevBanner/DevBanner";
import DesktopContainer from "pages/HomePage/components/DesktopContainer";

interface IProps {
  children: ReactNode;
}

const HomeResponsiveContainer = ({ children }: IProps) => {
  useEffect(() => {
    document.body.style.backgroundColor = "white";
  }, []);
  return (
    <>
      <Media greaterThanOrEqual="tablet">
        <DesktopContainer headerComponent={<HomepageHeading />}>
          <Container fluid>
            <DevBanner />
          </Container>
          {children}
        </DesktopContainer>
      </Media>
      <Media lessThan="tablet">
        <MobileContainer headerComponent={<HomepageHeading />}>
          <Container>
            <DevBanner />
          </Container>
          {children}
        </MobileContainer>
      </Media>
    </>
  );
};

HomeResponsiveContainer.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
};

export default HomeResponsiveContainer;
