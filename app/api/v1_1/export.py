import traceback
from datetime import date, timedelta

import logging
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from flask import current_app, make_response, jsonify, request
from flask_restful import Resource, reqparse

from app.api.v1.utils import is_signature_valid
from app.common.utils import str2bool
from app.models.events import Event

api_logger = logging.getLogger('webapp.api')


class ExportAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('ak', type=str, required=True,
                                   help='No Api Key provided', location='args')
        self.reqparse.add_argument('api', type=str, required=True,
                                   help='No API type', location='args')
        self.reqparse.add_argument('timestamp', type=int, required=True,
                                   help='No Timestamp provided', location='args')

        self.reqparse.add_argument('signature', type=str, required=True,
                                   help='No signature provided', location='args')

        # Event info
        self.reqparse.add_argument('event', type=str, required=False,
                                   help='No event id provided', location='args')

        self.reqparse.add_argument('from', type=str, required=False,
                                   help='No from provided', location='args')

        self.reqparse.add_argument('to', type=str, required=False,
                                   help='No to provided', location='args')

        self.reqparse.add_argument('indico_cat', type=str, required=False,
                                   help='No indico_cat provided', location='args')

        self.reqparse.add_argument('custom', type=str, required=False,
                                   help='No custom provided', location='args')

        self.reqparse.add_argument('only_public', type=str, required=False,
                                   help='No only_public provided', location='args')

    def get(self):
        """
        Will update the values of a LiveStream

        :return: The JSON response
        """
        api_logger.debug("Export API...")
        api_logger.info(
            "New Export api request from '{}' to '{}' with query string '{}'".format(
                request.environ.get('REMOTE_ADDR', 'Unkwnown'),
                request.environ.get('PATH_INFO', 'Unknown'),
                request.environ.get('QUERY_STRING', 'Unknown')
            ))

        args = self.reqparse.parse_args()
        timestamp = args.get('timestamp')
        api_key = args.get('ak')
        api_type = args.get('api')
        signature = args.get('signature')

        event = args.get('event')
        from_date = args.get('from')
        to_date = args.get('to')
        indico_cat = args.get('indico_cat')
        custom = args.get('custom')
        only_public = args.get('only_public')

        new_list = ()
        if api_key:
            new_list += (("ak", api_key),)
        if api_type:
            new_list += (("api", api_type),)
        if timestamp:
            new_list += (("timestamp", timestamp),)
        query = Event.query
        if event:
            new_list += (("event", event),)
            query = query.filter_by(indico_id=event)
        if from_date:
            new_list += (("from", from_date),)
        if to_date:
            new_list += (("to", to_date),)
        if indico_cat:
            new_list += (("indico_cat", indico_cat),)
            api_logger.debug(indico_cat)
            if '+' in indico_cat:
                categories = indico_cat.split('+')
                query = query.filter(Event.indico_category_id.in_(categories))
            else:
                query = query.filter_by(indico_category_id=indico_cat)
            api_logger.debug(query)
        if custom:
            new_list += (("custom", custom),)
        if only_public:  # Not protected
            new_list += (("only_public", only_public),)
            if only_public in ["true", "false"]:
                is_protected = str2bool(only_public)
                # If is_public == true, then return only events that are not protected (is_protected= false)
                query.filter_by(is_protected=(not is_protected))
            else:
                return make_response(jsonify(valid=False, message="Error using API: only_public value is not valid"),
                                     403)

        api_logger.debug("Returning events between {} and {}".format(from_date, to_date))

        if from_date and to_date:

            try:
                to_date = self.process_date_arg(to_date)
                from_date = self.process_date_arg(from_date)

                converted_from_date = parse(from_date)
                converted_to_date = parse(to_date)

                api_logger.debug(
                    "Returning events between {} and {}".format(converted_from_date, converted_to_date))

                query = query.filter(Event.start_date >= converted_from_date, Event.start_date <= converted_to_date)

            except Exception as e:
                return make_response(jsonify(valid=False, message="Error using API: {}".format(e)), 403)

        try:
            api_logger.debug("Checking if signature is valid...")
            api_path = '/api/v1.1/export/'
            api_logger.debug("-------------------")
            api_logger.debug(api_key)
            api_logger.debug(signature)
            api_logger.debug(timestamp)
            api_logger.debug(new_list)
            api_logger.debug(api_path)
            api_logger.debug("-------------------")

            signature_valid, message = is_signature_valid(api_key, signature, timestamp, args=new_list,
                                                          api_path=api_path)

            if signature_valid:
                api_logger.debug("Signature is valid.")
                # api_logger.debug(query)
                events = query.all()
                results_list = []
                for event in events:
                    if event.is_restricted() and str2bool(only_public):
                        api_logger.debug("Event id: {} is restricted.".format(event.id))
                        continue
                    results_list.append(event.to_json(use_indico_date=True))

                return_dict = {
                    'count': len(events),
                    'results': results_list
                }

                return make_response(jsonify(return_dict), 200)

            return make_response(jsonify(valid=False, message="Error using API: {}".format(message)), 403)

        except Exception as e:
            return make_response(jsonify(valid=False, message="Error using API: {}. {}".format(e, traceback.print_exc())), 403)

    def process_date_arg(self, to_date):
        if to_date == 'yesterday':
            to_date = (date.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
        if to_date == 'today':
            to_date = date.today().strftime('%Y-%m-%d')
        if to_date == 'tomorrow':
            to_date = (date.today() + relativedelta(days=1)).strftime('%Y-%m-%d')
        if to_date.endswith("d") or to_date.endswith("m"):
            if to_date.startswith("-"):
                sign = to_date[0]
                number_of_units = to_date[1:][:-1]
            else:
                sign = '+'
                number_of_units = to_date[:-1]
            unit = to_date[-1:]

            if sign == '-' and unit == 'm':
                to_date = (date.today() - relativedelta(months=int(number_of_units))).strftime('%Y-%m-%d')
            if sign == '+' and unit == 'm':
                to_date = (date.today() + relativedelta(months=int(number_of_units))).strftime('%Y-%m-%d')
            if sign == '-' and unit == 'd':
                to_date = (date.today() - relativedelta(days=int(number_of_units))).strftime('%Y-%m-%d')
            if sign == '+' and unit == 'd':
                to_date = (date.today() + relativedelta(days=int(number_of_units))).strftime('%Y-%m-%d')
        return to_date
