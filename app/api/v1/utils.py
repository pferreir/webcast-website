import hashlib
import hmac
from urllib.parse import urlencode

import time

import logging
from sqlalchemy.orm.exc import NoResultFound

from app.models.api import ApiKey

api_logger = logging.getLogger('webapp.api')


def generate_hmac_signature(path=None, params=None, secret_key=None):
    """
    Generates a hmac signature from the given parameters

    :param path: 
    :param params: 
    :param secret_key: 
    :return: 
    """
    items = sorted(params, key=lambda x: x[0].lower())

    url = '%s?%s' % (path, urlencode(items))
    signature = hmac.new(bytearray(secret_key, "UTF-8"), bytearray(url, "UTF-8"), hashlib.sha1).hexdigest()

    return signature


def is_signature_valid(api_key, signature, timestamp, args, api_path='/api/v1/update-stream/'):
    """
    Checks if a hmac signature is valid or not
    
    :param api_key: 
    :param signature: 
    :param timestamp: 
    :param args: 
    :param api_path: 
    :return: 
    """
    # Verify Timestamp difference cannot be > 300
    if int(time.time()) - timestamp > 300:
        error_message = "request timeout."
        api_logger.debug("Error: request timeout")
        return False, error_message
    api_logger.debug("Timestamp -> ok")

    api_object = None
    try:
        # Get the API Key from the database and check if it is active
        api_object = ApiKey.query.filter_by(access_key=api_key).one()
    except NoResultFound:
        return False, "Api Key is incorrect"

    if not api_object.status:
        error_message = "Api Key is not valid"
        api_logger.debug("Error: api key is not valid")

        return False, error_message

    api_logger.debug("Api Key -> ok")

    # Check if we can access this resource with that key
    args_dict = dict((x, y) for x, y in args)
    if api_object.resources != args_dict['api']:
        error_message = "Api Key is not valid for this resource"
        api_logger.debug("Error: {}".format(error_message))

        return False, error_message

    api_logger.debug("Resources -> ok")

    # Generate a signature using the secret key, API path and url params
    new_signature = generate_hmac_signature(path=api_path, params=args, secret_key=api_object.secret_key)
    # Compare the generated hash and verify it matches the signature
    if new_signature != signature:
        error_message = "Signature is not valid"
        api_logger.debug("Error: Signature is not valid")
        return False, error_message

    api_logger.debug("Signature is valid")
    return True, "valid"
