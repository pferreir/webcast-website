import flask_restful
from flask import jsonify


class HelloWorld(flask_restful.Resource):
    """
        Example class to access the API
    """

    def get(self):
        """ Return example JSON

            :returns: example JSON
        """
        return jsonify({
            'result': {
                "id": 1,
                "text": "Hello protected world!"
            }
        })