from flask import Flask, Blueprint
import flask_restful

from app.api.v1.events_fetch import UpdateUpcomingEventsAPI
from app.api.v1.notifications import StreamFollowUpNotification, StreamUpdateNotification
from app.api.v1.examples import HelloWorld
from app.api.v1.export import ExportAPI

API_VERSION_V1 = 1
API_VERSION = API_VERSION_V1

api_v1_bp = Blueprint('api_v1', __name__)
api_v1 = flask_restful.Api(api_v1_bp)

api_v1.add_resource(StreamFollowUpNotification, '/notification/')
api_v1.add_resource(StreamUpdateNotification, '/update-stream/')
api_v1.add_resource(ExportAPI, '/export/')
api_v1.add_resource(UpdateUpcomingEventsAPI, '/fetch-upcoming/')

api_v1.add_resource(HelloWorld, '/hello/')  # Example
