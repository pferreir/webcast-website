from flask import g
import logging
from app.api.decorators import require_token
from app.api.api_authorizations import authorizations
from flask_restx import Namespace, Resource, fields

logger = logging.getLogger('webapp.api.users')

namespace = Namespace('users', description='Users related operations', authorizations=authorizations,
                security=['Bearer Token'])

def allow_anonymous(fn):
    fn._allow_anonymous = True
    return fn


me_model = namespace.model('MeModel', {
    'email': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
    'uid': fields.String,
    'roles': fields.List(fields.String)
})

@namespace.doc(security=['Bearer Token'])
@namespace.route('/me')
class MeEndpoint(Resource):
    @namespace.doc('me')
    @namespace.marshal_with(me_model)
    @require_token
    def get(self):
        user = g.user
        logger.info("Api getMe endpoint")
        if user:
            return g.user
        else:
            return "No user set", 401
