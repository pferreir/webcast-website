import logging
import re
import hashlib
import base64
import datetime
import pytz


from flask import g, request
from flask_restx import Namespace, Resource, reqparse
from sqlalchemy import desc, asc

from app.common.authentication.event_authentication import is_user_authorized
from app.extensions import cache
from app.handlers.cds import CdsHandler

from app.handlers.events import EventHandler
from app.models.events import Event, EventStatus
from app.api.decorators import require_token
from app.api.api_authorizations import authorizations

from app.models.streams import Category, PermanentStream
from app.config import config


logger = logging.getLogger("webapp.api.webcasts")

namespace = Namespace(
    "webcasts",
    description="Webcasts related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

FIVE_MINUTES_IN_SECONDS = 60 * 5
THIRTY_SECONDS = 30


@cache.memoize(THIRTY_SECONDS)
def get_upcoming_webcasts_for_user():
    """
    Get all the upcoming webcasts

    :return: A list of event
    :rtype: list
    """
    logger.info("Get webcasts upcoming (Not cached)")
    events = (
        Event.query.filter(Event.status == EventStatus.UPCOMING)
        .order_by(asc(Event.start_date))
        .filter_by(is_visible=True)
        .limit(6)
        .all()
    )
    results_list = []
    for event in events:
        results_list.append(event.to_json_v2(use_indico_date=True))

    return_dict = {"success": True, "count": len(events), "results": results_list}
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def get_live_webcasts_for_user():
    """
    Get all the live webcasts

    :return: A list of Event
    :rtype: list
    """
    logger.info("Get webcasts (Not cached)")
    events = (
        Event.query.filter(Event.status == EventStatus.LIVE)
        .order_by(desc(Event.status == EventStatus.LIVE))
        .order_by(asc(Event.start_date))
        .filter_by(is_visible=True)
        .all()
    )
    results_list = []
    for event in events:
        results_list.append(event.to_json_v2(use_indico_date=True))

    return_dict = {"success": True, "count": len(events), "results": results_list}
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def get_recent_webcasts_for_user():
    """
    Get all the recent webcasts

    :return: A list of Event as dict
    :rtype: list
    """
    logger.info("Get recent webcasts (Not cached)")
    # Seven days for the DVR
    since = datetime.datetime.now() - datetime.timedelta(days=7)
    events = (
        Event.query.filter(Event.status == EventStatus.ARCHIVED)
        .filter(Event.start_date >= since)
        .order_by(desc(Event.start_date))
        .filter_by(is_visible=True)
        .all()
    )
    results_list = []
    for event in events:
        results_list.append(event.to_json_v2(use_indico_date=True))

    return_dict = {"success": True, "count": len(events), "results": results_list}
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def get_cds_records():
    """
    Get all the recent CDS records
    :return: A list of CDSRecords
    :rtype: list
    """
    logger.info("Get CDS records (Not cached)")
    # Seven days for the DVR
    cds_recrods = CdsHandler().get_most_recent_cds_records(6).all()
    results_list = []
    for record in cds_recrods:
        results_list.append(record.to_json_v2())

    return_dict = {"success": True, "count": len(cds_recrods), "results": results_list}
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def get_webcast_for_user(username, indico_id, code=None, client_ip=None):
    """
    Get a webcast for the current logged in user if the latter has access.

    :param code:
    :type code:
    :param username: The user username
    :type username:  str
    :param indico_id: Indico ID of the webcast event
    :type indico_id: int
    :return: The webcast details or an empty dict
    :rtype: dict
    """
    logger.info("Get webcast for user (Not cached)")
    logger.debug(f"Get webcast for user {username} (Not cached)")
    if indico_id == "0":
        event = EventHandler().create_test_channel()
        result = event.to_json_v2_with_streams(use_indico_date=True)
        return_dict = {"success": True, "result": result}
    else:
        event = (
            Event.query.filter_by(indico_id=indico_id).order_by(Event.id.desc()).first()
        )
        return_dict = {"success": False, "result": {}}

        if code and event.random_string != code:
            return return_dict

        if is_user_authorized(event):
            result = event.to_json_v2_with_streams(use_indico_date=True)
            return_dict = {"success": True, "result": result}

    _buildSecureTokenURLs(return_dict["result"], 1, client_ip)
    logger.info(return_dict)
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def get_permanent_webcast_for_user(username, webcast_id):
    """
    Get a permanent webcast for the current user if this user has access to it.

    :param username: The username of the current logged in user
    :type username: str
    :param webcast_id: The ID of the webcast
    :type webcast_id: int
    :return: The dictionary with the permanent webcast details and 'success' True, otherwise an empty dict with success 'False'
    :rtype: dict
    """
    logger.info("Get permanent webcast for user (Not cached)")
    logger.debug(f"Get permanent webcast for user {username} (Not cached)")

    webcast = PermanentStream.query.get(webcast_id)

    if is_user_authorized(webcast):
        return_dict = {"success": True, "result": webcast.to_json_details()}
    else:
        return_dict = {"success": False, "result": {}}
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def check_webcast_protected(indico_id):
    """
    Check if a webcast is protected or not
    :param indico_id: The Indico ID of the event
    :type indico_id: int
    :return: True if the event is protected. False otherwise.
    :rtype: bool
    """
    logger.info("Check webcast protected (Not cached)")
    event = Event.query.filter_by(indico_id=indico_id).order_by(Event.id.desc()).first()
    if indico_id == "0":
        is_access_restricted = False
    else:
        is_restricted = event.is_restricted()
        is_access_restricted = (
            True if event.is_protected or event.is_restricted() else False
        )
        logger.info(
            f"Event {event.title} is_restricted: {is_restricted} (is_protected:{event.is_protected} or is_restricted:{is_restricted})"
        )
    return is_access_restricted


@cache.memoize(THIRTY_SECONDS)
def get_webcast_if_public(indico_id, code=None, client_ip=None):
    """
    Get the webcast details of a webcast event if it is public

    :param code:
    :type code:
    :param indico_id: The Indico ID of the event
    :type indico_id: int
    :return: The webcast details as dict or an empty dict
    :rtype: dict
    """
    logger.info("Get public webcast (Not cached)")
    event = Event.query.filter_by(indico_id=indico_id).order_by(Event.id.desc()).first()

    if indico_id == "0":
        event = EventHandler().create_test_channel()
        result = event.to_json_v2_with_streams(use_indico_date=True)
        return_dict = {"success": True, "result": result}
    else:
        return_dict = {"success": False, "result": {}}
        is_valid_preview = False
        event_has_restrictions = event.is_protected or event.is_restricted()
        if (
            code
            and event.random_string == code
            and event.status == EventStatus.UPCOMING
        ):
            is_valid_preview = True

        if event_has_restrictions and not is_valid_preview:
            return return_dict
        else:
            result = event.to_json_v2_with_streams(use_indico_date=True)
            return_dict = {"success": True, "result": result}
    if client_ip:
        logger.info(f"IP Of client is: {client_ip}")

    logger.info(f"Original {return_dict}")

    _buildSecureTokenURLs(return_dict["result"], 1, client_ip)

    logger.info(f"After secureToken {return_dict}")
    return return_dict


@cache.memoize(THIRTY_SECONDS)
def _getUTCSecs(datedict, delta=0):
    """return seconds of the time Now or Now + delta

    Args:
        delta ([type]): number of days to add
    """
    dt = datetime.datetime.strptime(
        "{} {}".format(datedict["day"], datedict["time"]), "%Y-%m-%d %H:%M:%S"
    )
    timezone = pytz.timezone(datedict["tz"])
    dt_withtz = timezone.localize(dt)
    if delta:
        dt_withtz = dt_withtz + datetime.timedelta(days=delta)
    return round(dt_withtz.timestamp())


@cache.memoize(THIRTY_SECONDS)
def _buildSecureTokenURLs(record, delta, ip=None):
    """_summary_

    Args:
        record: dictionary with all the event metadata including URL(s): https://wowza.cern.ch/livehd/smil:1106223_camera_all.smil/playlist.m3u8
        delta (int): number of days
        ip (str): client IP, none if we dont want to considere it

    Returns:
        _type_: _description_
    """
    if (
        record
        and record["stream"]
        and record["stream"]["camera_src"]
        and record["stream"]["slides_src"]
    ):
        urls = [record["stream"]["camera_src"], record["stream"]["slides_src"]]
        for i in (0, 1):
            ismatch = re.match(r"http.?://wowza.*/playlist\.m3u8.*", urls[i], re.I)
            if not ismatch:
                logger.info(f"url {urls[i]} is not matching secureToken pattern")
                return

            arr = urls[i].split("/")
            subset_arr = arr[3:-1]
            finalUrl = "/".join(subset_arr) + "?"

            # if it's not a wowza app configured for secureTokens leave
            if subset_arr[-2].lower() not in config.WOWZA_SECURETOKEN.keys():
                return

            # increase endDate by delta day(s)
            endtime = _getUTCSecs(record["endDate"], delta)

            logger.info(f"End secs: {endtime}")

            listOfParams = [
                "wowzatokenendtime={}".format(endtime),
                config.WOWZA_SECURETOKEN[subset_arr[-2].lower()],
            ]
            if ip:
                listOfParams.append(str(ip))
            listOfParams = sorted(listOfParams)
            logger.info(f"All params considered for SecureToken: {listOfParams}")

            total_string = "{}{}".format(finalUrl, "&".join(listOfParams))
            logger.info("string to be hashed: {}".format(total_string))

            prewowzatokenhash = hashlib.sha256(total_string.encode("utf-8")).digest()

            wowzatokenhash = str(base64.urlsafe_b64encode(prewowzatokenhash), "utf-8")

            newlist = [
                "wowzatokenendtime={}".format(endtime),
                "wowzatokenhash={}".format(wowzatokenhash),
            ]

            if i == 0:
                record["stream"]["camera_src"] = "{}?{}".format(
                    urls[i], "&".join(newlist)
                )
            else:
                record["stream"]["slides_src"] = "{}?{}".format(
                    urls[i], "&".join(newlist)
                )

            logger.info(f"final url is: {urls[i]}?{'&'.join(newlist)}")


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class WebcastsEndpoint(Resource):
    @namespace.doc("webcasts")
    def get(self):
        """
        Get all the webcasts of a given type

        This endpoint expects a 'type' on the query string with one of the following values:
        - recent
        - upcoming
        - live

        :return: A list with all the events or an empty dict if type is not correct
        :rtype: dict
        """
        parser = reqparse.RequestParser()
        parser.add_argument("type")

        args = parser.parse_args()
        return_dict = {}
        logger.info("Get webcasts endpoint")
        if args.get("type", None):
            if args.get("type", None) == "recent":
                return_dict = get_recent_webcasts_for_user()
            if args.get("type", None) == "upcoming":
                return_dict = get_upcoming_webcasts_for_user()
            if args.get("type", None) == "live":
                return_dict = get_live_webcasts_for_user()
        else:
            return "Webcast type not specified", 400

        return return_dict


@namespace.doc(security=["Bearer Token"])
@namespace.route("/cds-records")
class CdsRecordsEndpoint(Resource):
    @namespace.doc("cds_recors")
    def get(self):
        """
        Get all the recent CDS records
        :return: A list of CDS records
        :rtype: list
        """
        logger.info("Get CDS records endpoint")
        return_dict = get_cds_records()
        return return_dict


@namespace.doc(security=["Bearer Token"])
@namespace.route("/permanent-categories/")
class PermanentWebcastsCategoriesEndpoint(Resource):
    @namespace.doc("categories")
    def get(self):
        """
        Get all the permanent categories
        :return: A list of Category as dict
        :rtype: dict
        """
        results_list = []
        logger.info("Get permanent categories endpoint")
        categories = Category.query.all()
        for cat in categories:
            results_list.append(cat.to_json_v2())

        return_dict = {
            "success": True,
            "count": len(categories),
            "results": results_list,
        }

        return return_dict


@namespace.doc(security=["Bearer Token"])
@namespace.route("/permanent-webcasts/<int:webcast_id>")
class PermanentWebcastsEndpoint(Resource):
    @namespace.doc("permanent_webcast")
    @require_token
    def get(self, webcast_id):
        """
        Get a permanent webcast event (protected endpoint)
        :param webcast_id: The webcast internal ID
        :type webcast_id: int
        :return: A dict with the Permanent Webcast details
        :rtype: dict
        """
        logger.info("Get permanent webcast for user endpoint")
        user = g.user
        if user:
            return_dict = get_permanent_webcast_for_user(user, webcast_id)

            return return_dict
        else:
            return "No user set", 401


def get_code_and_indico_id(indico_id):
    code = None
    url_params = indico_id
    if indico_id.startswith("p"):
        indico_id = url_params[1:].split("-")[1]
        code = url_params[1:].split("-")[0]
    else:
        indico_id = indico_id[1:]
    return code, indico_id


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:indico_id>")
class PrivateWebcastDetailsAPI(Resource):
    decorators = [require_token]

    def get(self, indico_id):
        """
        Get the webcast by Indico ID (protected endpoint)

        :return: The JSON response
        """
        logger.info("Get webcast endpoint")
        code, indico_id = get_code_and_indico_id(indico_id)
        return_dict = get_webcast_for_user(
            g.user,
            indico_id,
            code=code,
            client_ip=request.environ.get("HTTP_X_FORWARDED_FOR", request.remote_addr),
        )

        return return_dict, 200


@namespace.route("/<string:indico_id>/is-protected")
class PrivateWebcastIsProtectedApi(Resource):
    def get(self, indico_id):
        """
        Check if a webcast by Indico ID is protected or restricted

        :return: The JSON response
        """
        logger.info("Check webcast is protected endpoint")
        indico_id = indico_id[1:]
        is_restricted = check_webcast_protected(indico_id)
        return_dict = {"success": True, "result": {"is_restricted": is_restricted}}

        return return_dict, 200

    @namespace.route("/public/<string:indico_id>")
    class PrivateWebcastDetailsPublicAPI(Resource):
        def get(self, indico_id):
            """
            Get the webcast by Indico ID if it's not restricted

            :return: The JSON response
            """
            logger.info("Get public webcast details endpoint")
            code, indico_id = get_code_and_indico_id(indico_id)

            logger.info(code)
            logger.info(indico_id)

            client_remote_ip = request.environ.get(
                "HTTP_X_FORWARDED_FOR", request.remote_addr
            )
            logger.info(f"client ip is {client_remote_ip}")
            return_dict = get_webcast_if_public(
                indico_id,
                code=code,
                client_ip=request.environ.get(
                    "HTTP_X_FORWARDED_FOR", request.remote_addr
                ),
            )

            return return_dict, 200
