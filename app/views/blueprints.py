from flask import Blueprint


def _blueprint_factory(partial_module_string, url_prefix):
    """
    Generates blueprint objects for view modules.
    
    :param partial_module_string:  String representing a view module without the absolute path (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    name = partial_module_string
    import_name = 'app.views.{}'.format(partial_module_string)
    template_folder = 'templates'
    blueprint = Blueprint(name, import_name, url_prefix=url_prefix)
    return blueprint


# Put admin blueprints here
admin_blueprint = _blueprint_factory('admin', '/admin')
events_blueprint = _blueprint_factory('admin.events', '/admin/events')
authorization_blueprint = _blueprint_factory('admin.authorization', '/admin/authorization')
permanent_streams_blueprint = _blueprint_factory('admin.streams', '/admin/permanent-streams')
api_blueprint = _blueprint_factory('admin.api', '/admin/api-keys')

# Put user's frontend blueprints here
users_blueprint = _blueprint_factory('users', '/old')
utils_blueprint = _blueprint_factory('users.utils', '/utils')
feeds_blueprint = _blueprint_factory('users.feeds', '/feeds')

all_blueprints = (admin_blueprint,
                  events_blueprint,
                  permanent_streams_blueprint,
                  authorization_blueprint,
                  users_blueprint,
                  utils_blueprint,
                  api_blueprint,
                  feeds_blueprint
                  )
