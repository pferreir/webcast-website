from os import path
import logging
from flask import (current_app, render_template, request)
from flask import redirect
from flask import session
from flask import url_for
from flask_login import (login_required, logout_user)
import sqlalchemy
from werkzeug.urls import url_encode

from app.common.authentication.internal_authorization import (is_user_authorized,
                                                              is_user_authorized_for_permanent_stream)
from app.common.datetime_helper import add_suffix_to_date
from app.handlers.cds import CdsHandler
from app.handlers.events import EventHandler

from app.models.streams import Category, PermanentStream
from app.views.blueprints import users_blueprint

logger = logging.getLogger('webapp.users')


@users_blueprint.route('/')
def index():
    """
    Homepage view
    :return: 
    """
    next_live_events = None
    limit_of_upcoming_to_fetch = 6
    limit_of_cds_records = 6

    event_handler = EventHandler()
    live_events = event_handler.get_live_events()

    # Fetch 1 upcoming event more if there are no live events.
    if not live_events:
        limit_of_upcoming_to_fetch = 7
        next_live_events = event_handler.get_first_upcoming_event()

    upcoming_events = event_handler.get_upcoming_events(limit_of_upcoming_to_fetch)

    if not live_events:
        upcoming_events = upcoming_events[1:]

    recent_events = CdsHandler().get_most_recent_cds_records(limit_of_cds_records)

    return render_template('users/index.html',
                           live_events=live_events,
                           upcoming_events=upcoming_events,
                           recent_events=recent_events,
                           next_live_events=next_live_events
                           )


@users_blueprint.route('/permanent-webcasts')
def permanent_webcasts():
    """
    Permanent Webcasts view
    :return: 
    """
    images_path = path.join(current_app.config['IMAGES_FOLDER'], current_app.config['PERMANENT_STREAM_IMAGES_FOLDER'])
    categories = Category.query.all()

    return render_template('users/permanent_webcasts.html', categories=categories, images_path=images_path)


@users_blueprint.route('/permanent-webcasts/category/<category_id>')
def permanent_webcasts_category_details(category_id):
    """
    Permanent Webcasts category details view
    :param category_id: The category ID
    :return: 
    """
    streams = PermanentStream.query.filter_by(category_id=category_id)
    images_path = path.join(current_app.config['IMAGES_FOLDER'], current_app.config['PERMANENT_STREAM_IMAGES_FOLDER'])
    try:
        category = Category.query.get_or_404(category_id)
    except (sqlalchemy.exc.DataError, sqlalchemy.exc.InternalError) as e:
        logger.warning("Error getting Category ID: {}: {}".format(category_id, str(e)))
        return render_template('404.html'), 404

    return render_template('users/permanent_webcasts_details.html', streams=streams, images_path=images_path,
                           category=category, event_type='permanent')


@users_blueprint.route('/language/<language>')
def set_language(language=None):
    """
    View that changes the language of the site by changing the language value in the session attributes
    :param language: Language code
    :return: Redirects to the index
    """
    session['language'] = language
    return redirect(url_for('users.index'))


@users_blueprint.route('/logout')
@login_required
def logout():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    logout_user()
    session["roles"] = []

    if not current_app.config['OIDC_LOGOUT_URL']:
        return redirect(url_for('index'))

    query = url_encode({'post_logout_redirect_uri': current_app.config['OIDC_LOGOUT_URL']})
    oid_logout_url = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout"

    return redirect("{}?{}".format(oid_logout_url,query))


@users_blueprint.route('/permanent-webcasts/test-channel')
def permanent_webcast_test_channel():
    """
    View for the Test channel
    :return: 
    """
    logger.debug("Loaded test channel view")

    event = EventHandler().create_test_channel()

    return render_template('users/view-modes/camera_slides_view.html', event=event)


@users_blueprint.route('/screen-share')
def screen_share():
    """
    View that opens a popup for one of the existing streams
    :return: 
    """
    return render_template('users/screen-share.html')


@users_blueprint.route('/play-webcast/<stream_id>')
def play_webcast(stream_id):
    """
    View for the PermanentStream details
    :param stream_id: 
    :return: 
    """
    try:
        stream = PermanentStream.query.get_or_404(stream_id)

        is_authorized = is_user_authorized_for_permanent_stream(stream)
    except (sqlalchemy.exc.DataError, sqlalchemy.exc.InternalError) as e:
        logger.warning("Error getting permanent stream ID: {}: {}".format(stream_id, str(e)))
        return render_template('404.html'), 404

    return render_template('users/webcast_player.html', stream=stream, is_authorized=is_authorized)


@users_blueprint.route('/event/<event_id>/camera-slides')
def events_details_camera_slides(event_id):
    """
    View for the event details
    :param event_id: Event ID
    :return: 
    """

    event, event_found = EventHandler().get_event_or_404(event_id)

    if not event_found:
        return render_template('404.html'), 404

    formated_date = add_suffix_to_date(event.start_date, '%A {th} %B %Y at %H:%M')

    is_authorized = is_user_authorized(event)

    return render_template('users/view-modes/camera_slides_view.html', event=event,
                           formated_date=formated_date, is_authorized=is_authorized)


@users_blueprint.route('/event/<event_id>')
def events_details(event_id):
    """
    View for the event details
    :param event_id: Event ID
    :return: 
    """

    event, event_found = EventHandler().get_event_or_404(event_id)

    if not event_found:
        return render_template('404.html'), 404

    if event.live_stream.type == 'camera':
        return redirect(url_for('users.events_details_camera', event_id=event.id))

    is_authorized = is_user_authorized(event)
    formated_date = add_suffix_to_date(event.start_date, '%A {th} %B %Y at %H:%M')

    return render_template('users/event_details.html', event=event,
                           formated_date=formated_date, is_authorized=is_authorized)


@users_blueprint.route('/event/<event_id>/camera')
def events_details_camera(event_id):
    """
    View for the event details
    :param event_id: Event ID
    :return: 
    """
    event, event_found = EventHandler().get_event_or_404(event_id)

    if not event_found:
        return render_template('404.html'), 404

    is_authorized = is_user_authorized(event)
    formated_date = add_suffix_to_date(event.start_date, '%A {th} %B %Y at %H:%M')

    return render_template('users/view-modes/camera_view.html', event=event,
                           formated_date=formated_date, is_authorized=is_authorized)
