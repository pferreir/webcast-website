import logging
from urllib.parse import urlparse, urljoin
from flask import redirect, current_app
from flask import request, url_for

logger = logging.getLogger('webapp.views')

def is_safe_url(target):
    """
    Checks if a URL is safe to redirect to it.
    :param target: Url to redirect to
    :return: Boolean if the urls is safe or not
    """
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


def get_redirect_target():
    """
    Gets the redirect target for the current view. This is used after a form is submited.
    
    :return: The redirect url to be redirected to.
    """
    for target in request.values.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


def redirect_back(endpoint, **values):
    """
    Redirects to the 'next' value in the form field if present. If not, to the endpoint.
    
    :param endpoint:  Endpoint to redirect to.
    :param values: URL query parameters
    :return: The redirection to the target
    """
    target = request.form['next']
    if not target or not is_safe_url(target):
        target = url_for(endpoint, **values)
    logger.debug("Redirecting to {}".format(target))
    return redirect(target)
