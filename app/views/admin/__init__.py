import glob
import os

from flask import Blueprint, request, url_for, jsonify
from flask import render_template, abort
from jinja2 import TemplateNotFound
from flask import current_app
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root

from app.common.utils import get_images_from_folder, is_subdir
from app.decorators import requires_login, admin_required

from app.views.blueprints import admin_blueprint

default_breadcrumb_root(admin_blueprint, '.')


@admin_blueprint.route('/')
@requires_login
@admin_required
@register_breadcrumb(admin_blueprint, '.', 'Home')
def index():
    """
    Admin home view
    :return: 
    """
    return render_template('admin/home.html')


@admin_blueprint.route('_get_images_path')
@requires_login
@admin_required
def _get_images_path():
    """
    Gets a relative path from the query args and fins all the images in that path, checking if they 
    are in a subdirectory of the application default images folder
    :return: A list with relative images in the path or an empty list
    """
    images_path = request.args.get('images_path', 0, type=str)
    files_found = []

    """
    We check if It is a subdir for reasons of security
    """
    if is_subdir(images_path, os.path.join(current_app.config.get('STATIC_FILES_PATH'),
                                           current_app.config.get('DEFAULT_IMAGES_FOLDER'))):
        files_found = get_images_from_folder(images_path)
        return jsonify(result=files_found)

    return jsonify(result=files_found)
