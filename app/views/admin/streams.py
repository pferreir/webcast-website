import os
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask import current_app
from sqlalchemy.orm.exc import NoResultFound
import logging
from app.decorators import requires_login, admin_required
from app.extensions import db

from app.forms.admin.streams import CategoryForm, PermanentStreamForm, process_permanent_stream_form
from app.models.streams import Category, PermanentStream
from app.models.audiences import Audience, AuthorizedUser

from app.views.blueprints import permanent_streams_blueprint
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root

default_breadcrumb_root(permanent_streams_blueprint, '.')

logger = logging.getLogger('webapp.admin')

def view_category_dlc(*args, **kwargs):
    """
    Will load the information needed for the breadcrums in category edit view.
    
    :param args: Must include category_id
    :param kwargs: Not used
    :return: A list with a dictionary that includes the text of the breadcrumb link
    """
    category_id = request.view_args['category_id']
    category = Category.query.get(category_id)
    return [{'text': 'Edit: ' + str(category.id)}]


def view_stream_dlc(*args, **kwargs):
    """
    Will load the information needed for the breadcrums in permanent stream edit view.
    
    :param args: Must include permanent_stream_id
    :param kwargs: Not used
    :return: A list with a dictionary that includes the text of the breadcrumb link
    """
    permanent_stream_id = request.view_args['permanent_stream_id']
    permanent_stream = PermanentStream.query.get(permanent_stream_id)
    return [{'text': 'Edit: ' + str(permanent_stream.id)}]


@permanent_streams_blueprint.route('/', methods=['GET', 'POST'])
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.permanent_streams_home', 'Permanent Streams')
def permanent_streams_home():
    """
    View with a list with all the permanent streams
    
    :return: 
    """
    permanent_streams = PermanentStream.query.all()

    return render_template('admin/streams/permanent-streams.html', permanent_streams=permanent_streams)


@permanent_streams_blueprint.route('/categories')
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.categories', 'Categories')
def categories():
    """
    View with the list of all the permanent streams categories.
    
    :return: 
    """
    logger.debug("Loading permanent streams!")

    categories_all = Category.query.all()

    return render_template('admin/streams/categories.html', categories=categories_all)


@permanent_streams_blueprint.route('/categories/add', methods=['GET', 'POST'])
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.categories.categories_add', 'Add new')
def categories_add():
    """
    View to add  permanent streams categories
    
    :return: 
    """
    form = CategoryForm()

    default_images_folders = [x[0] for x in os.walk(
        os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                     current_app.config.get('DEFAULT_CATEGORIES_FOLDER')))]

    if form.validate_on_submit():
        logger.debug("Category form is valid")

        category = Category(name=form.name.data, priority=form.priority.data, default_img=form.default_img.data)

        db.session.add(category)
        db.session.commit()

        flash('Category added successfully', category='success')
        return redirect(url_for('admin.streams.categories'))

    return render_template('admin/streams/categories-add.html', form=form,
                           default_images_folders=default_images_folders)


@permanent_streams_blueprint.route('/categories/<category_id>/edit', methods=['GET', 'POST'])
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.categories.categories_edit', '',
                     dynamic_list_constructor=view_category_dlc)
def categories_edit(category_id):
    """
    View for permanent stream category edit
    
    :param category_id: Integer with the id of the category to edit.
    :return: 
    """
    category = Category.query.get(category_id)
    form = CategoryForm(name=category.name,
                        priority=category.priority,
                        use_default_img=category.use_default_img,
                        default_img=category.default_img,
                        custom_img=category.custom_img)
    default_images_folders = [x[0] for x in os.walk(
        os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                     current_app.config.get('DEFAULT_CATEGORIES_FOLDER')))]

    if form.validate_on_submit():
        logger.debug("Category form is valid")

        category.name = form.name.data
        category.priority = form.priority.data
        category.default_img = form.default_img.data

        """
        bools have to be treated manually
        """
        category.use_default_img = form.use_default_img.data

        db.session.commit()

        flash('Category updated successfully', category='success')
        return redirect(url_for('admin.streams.categories_edit', category_id=category_id))

    return render_template('admin/streams/categories-edit.html', form=form, category=category,
                           default_images_folders=default_images_folders)


@permanent_streams_blueprint.route('/categories/<category_id>/delete', methods=['POST'])
@requires_login
@admin_required
def categories_delete(category_id):
    """
    View to delete permanent streams categories
    
    :param category_id: Integer with the id of the category to delete.
    :return: 
    """
    try:
        category = Category.query.get(category_id)
        db.session.delete(category)
        db.session.commit()
        flash("Category (#{}) deleted successfully".format(category.id), "success")

    except NoResultFound:
        flash("The selected Category does not exist", "error")

    return redirect(url_for('admin.streams.categories'))


@permanent_streams_blueprint.route('/add', methods=['GET', 'POST'])
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.permanent_streams_home.permanent_streams_add', 'Add new')
def permanent_streams_add():
    """
    View where a new permanent stream is added
    
    :return: 
    """

    logger.debug("Setting default images folders")

    default_images_folders = [x[0] for x in os.walk(
        os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                     current_app.config.get('DEFAULT_CATEGORIES_FOLDER')))]

    form = PermanentStreamForm()
    if request.method == 'POST':
        if form.validate_on_submit():

            new_stream = PermanentStream(title=form.title.data,
                                         player_src=form.player_src.data,
                                         player_width=form.player_width.data,
                                         player_height=form.player_height.data,
                                         is_sharable=form.is_sharable.data,
                                         is_visible=form.is_visible.data,
                                         is_embeddable=form.is_embeddable.data,
                                         use_default_img=form.use_default_img.data,
                                         default_img=form.default_img.data,
                                         custom_img=form.custom_img.data,
                                         )

            """
            Set the AuthorizedUsers
            """
            new_stream.authorized_users = []
            users_array = []
            for user in form.authorized_users.data:
                found_user = AuthorizedUser.query.get(user)
                if found_user:
                    users_array.append(found_user)

            new_stream.authorized_users = users_array
            db.session.add(new_stream)
            db.session.commit()

            """
            Set the Audience
            """
            if form.audience.data:
                new_stream.audience = Audience.query.get(form.audience.data)
                new_stream.category = Category.query.get(form.category.data)

                db.session.commit()

            flash('Permanent Stream added properly', category='success')
            return redirect(url_for('admin.streams.permanent_streams_home'))
        else:
            logger.debug(form.errors)

    return render_template('admin/streams/permanent-streams-add.html', form=form,
                           default_images_folders=default_images_folders)


@permanent_streams_blueprint.route('/<permanent_stream_id>/delete', methods=['POST'])
@requires_login
@admin_required
def permanent_streams_delete(permanent_stream_id):
    """
    View used to delete a permanent stream
    
    :param permanent_stream_id: Integer with the ID of the permanent stream.
    :return: 
    """
    try:
        stream = PermanentStream.query.get(permanent_stream_id)
        db.session.delete(stream)
        db.session.commit()
        flash("Stream (#{}) deleted successfully".format(stream.id), "success")

    except NoResultFound:
        flash("The selected Stream does not exist", "error")

    return redirect(url_for('admin.streams.permanent_streams_home'))


@permanent_streams_blueprint.route('/<permanent_stream_id>/edit', methods=['GET', 'POST'])
@requires_login
@admin_required
@register_breadcrumb(permanent_streams_blueprint, '.permanent_streams_home.permanent_streams_edit', '',
                     dynamic_list_constructor=view_stream_dlc)
def permanent_streams_edit(permanent_stream_id):
    """
    View used to edit a permanent stream
    
    :param permanent_stream_id: 
    :return: 
    """
    stream = PermanentStream.query.get(permanent_stream_id)

    default_images_folders = [x[0] for x in os.walk(
        os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                     current_app.config.get('DEFAULT_CATEGORIES_FOLDER')))]

    form = PermanentStreamForm()

    if request.method == 'POST':
        form = PermanentStreamForm()
        logger.debug("POST Edit PermanentStream")

        if form.validate_on_submit():
            logger.debug("Edit permanent stream form is valid")
            """
            Update the existing PermanentStream with the form data
            """
            logger.debug("Edit Form is valid")
            process_permanent_stream_form(stream, permanent_stream_id, form)
            db.session.commit()
            flash("Stream #{} was updated successfully".format(permanent_stream_id), "success")
            return redirect(url_for('admin.streams.permanent_streams_edit', permanent_stream_id=permanent_stream_id))
    else:
        category_id = None
        if stream.category:
            category_id = stream.category.id

        audience_id = None
        if stream.audience:
            audience_id = stream.audience.id

        form = PermanentStreamForm(title=stream.title,
                                   player_src=stream.player_src,
                                   player_width=stream.player_width,
                                   player_height=stream.player_height,
                                   is_embeddable=stream.is_embeddable,
                                   is_sharable=stream.is_sharable,
                                   is_visible=stream.is_visible,
                                   authorized_users=[authorized_user.id for authorized_user in
                                                     stream.authorized_users],
                                   audience=audience_id,
                                   category=category_id,
                                   default_img=stream.default_img,
                                   custom_img=stream.custom_img,
                                   use_default_img=stream.use_default_img
                                   )

    return render_template('admin/streams/permanent-streams-edit.html', form=form, stream=stream,
                           default_images_folders=default_images_folders)
