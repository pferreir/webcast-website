from flask import render_template, flash, redirect, url_for
from flask_breadcrumbs import register_breadcrumb

from app.decorators import requires_login, admin_required
from app.extensions import db
from app.models.events import Event, EventStatus
from app.views.blueprints import events_blueprint


@events_blueprint.route('/archived')
@requires_login
@admin_required
@register_breadcrumb(events_blueprint, '.index.archived', 'Archived')
def events_archived():
    """
    View for listing Archived Events

    :return: 
    """
    events = Event.query.filter(Event.status == EventStatus.ARCHIVED).order_by(Event.start_date.desc())
    return render_template('admin/events/archived.html', events=events)


@events_blueprint.route('/archived/<event_id>/restore', methods=['POST'])
@requires_login
@admin_required
def restore_event(event_id):
    """
    View for listing Archived Events

    :return: 
    """
    event = Event.query.get_or_404(event_id)
    event.status = EventStatus.UPCOMING
    event.is_visible = False
    event.is_synchronized = False
    db.session.commit()

    flash("Event {} has been restored. It is now an Upcoming Event".format(event.id))

    return redirect(url_for('admin.events.events_archived'))
