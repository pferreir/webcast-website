import datetime
import os
import logging
from flask import current_app
from flask import flash, jsonify
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root
from flask_login import current_user
from sqlalchemy.orm.exc import NoResultFound

from app.decorators import requires_login, admin_required
from app.extensions import db
from app.forms.admin.events import EventForm, process_event_form, CDSRecordForm
from app.forms.admin.streams import LiveStreamForm
from app.handlers.follow_ups import FollowUpHandler
from app.models.events import Event, EventStatus, FollowUp
from app.models.streams import VideoQuality
from app.services.wowza import WowzaService
from app.views import redirect_back
from app.views.blueprints import events_blueprint

default_breadcrumb_root(events_blueprint, '.')

logger = logging.getLogger('webapp.admin')

def view_event_dlc(*args, **kwargs):
    """
    Displays the breadcrumb text for the edit event view
    
    :param args:  Must contain the event_id
    :param kwargs: 
    :return: 
    """
    event_id = request.view_args['event_id']
    event = Event.query.get(event_id)
    return [{'text': 'Edit: ' + str(event.id)}]


@events_blueprint.route('/')
@requires_login
@admin_required
@register_breadcrumb(events_blueprint, '.index', 'Events')
def index():
    """
    Admin events index page
    
    :return: The index page
    """
    return render_template('admin/events/events.html')


@events_blueprint.route('/<event_id>/delete/', methods=['POST'])
@requires_login
@admin_required
def event_delete(event_id):
    """
    Deletes an event by ID
    
    :param event_id: Internal Event ID
    :return: 
    """
    event = Event.query.get_or_404(event_id)
    if event:
        if event.status == EventStatus.UPCOMING or event.status == EventStatus.LIVE:
            event.status = EventStatus.ARCHIVED
            logger.debug("Event {} is now ARCHIVED".format(event_id))
            flash("Event archived successfully", "success")
        elif event.status == EventStatus.ARCHIVED:
            if event.live_stream:
                db.session.delete(event.live_stream)
            db.session.delete(event)
            logger.debug("Event {} is now DELETED".format(event_id))
            flash("Event deleted successfully", "success")
        db.session.commit()
    else:
        flash("The selected event does not exist", "error")

    logger.debug("Redirecting to admin.events.index...")
    return redirect_back('admin.events.index')


def build_event_form(event):
    """
    Creates an Event from using the parameter Event attributes.
    
    :param event: Event that will be used to populate the form
    :return: Generated populated form
    """
    form = EventForm(title=event.title,
                     speakers=event.speakers,
                     abstract=event.abstract,
                     room=event.room,
                     link=event.link,
                     link_label=event.link_label,
                     display_message=event.display_message,
                     extra_html=event.extra_html,
                     indico_id=event.indico_id,
                     indico_category_id=event.indico_category_id,
                     timezone=event.timezone,
                     start_date=event.start_date,
                     end_date=event.end_date,
                     is_synchronized=event.is_synchronized,
                     is_embeddable=event.is_embeddable,
                     is_sharable=event.is_sharable,
                     is_visible=event.is_visible,
                     is_protected=event.is_protected,
                     authorized_users=[authorized_user.id for authorized_user in
                                       event.authorized_users],
                     audience=event.audience.id,
                     use_default_img=event.use_default_img,
                     default_img=event.default_img
                     )
    return form


def process_stream_form(existing_stream, form_prefix=''):
    """
    When a LiveStreamForm is Post, It will be processed here.
    This function will update the fields of the existing stream using the data in the request.
    
    :param existing_stream: 
    :param form_prefix: 
    :return: 
    """

    """
    Get the form fields
    """
    stream_title = request.form.get('{}-title'.format(form_prefix), type=str)
    stream_enabled = True if request.form.get('{}-is_enabled'.format(form_prefix), type=str) == 'yes' else False
    type = request.form.get('{}-type'.format(form_prefix), type=str)
    is_360 = True if request.form.get('{}-is_360'.format(form_prefix), type=str) == 'yes' else False
    use_smil = True if request.form.get('{}-use_smil'.format(form_prefix), type=str) == 'yes' else False

    current_app.logger.info(use_smil)

    logger.debug("Use smil: {}".format(use_smil))

    """
    Set the form fields
    """
    existing_stream.is_enabled = stream_enabled if stream_enabled else False
    existing_stream.type = type
    existing_stream.is_360 = True if is_360 else False
    existing_stream.use_smil = True if use_smil else False

    """
    Set some fields depending on the LiveStream type
    """
    if existing_stream.type == 'embed':
        logger.debug("Is embed")
        embed_code = request.form.get('{}-embed_code'.format(form_prefix), type=str)
        existing_stream.embed_code = embed_code
    else:
        room_video_quality = request.form.get(
            '{}-room_video_quality'.format(form_prefix), type=VideoQuality)
        app_name = request.form.get('{}-app_name'.format(form_prefix), type=str)
        camera_src = request.form.get('{}-camera_src'.format(form_prefix), type=str)
        camera_fetch_id = request.form.get('{}-stream_camera_name'.format(form_prefix),
                                           type=str)
        camera_smil = request.form.get('{}-smil_camera'.format(form_prefix), type=str)

        existing_stream.room_video_quality = room_video_quality
        existing_stream.app_name = app_name
        existing_stream.camera_src = camera_src
        existing_stream.stream_camera_name = camera_fetch_id
        existing_stream.smil_camera = camera_smil

    if existing_stream.type == 'camera_slides':
        slides_src = request.form.get('{}-slides_src'.format(form_prefix), type=str)
        slides_fetch_id = request.form.get('{}-stream_slides_name'.format(form_prefix),
                                           type=str)
        slides_smil = request.form.get('{}-smil_slides'.format(form_prefix), type=str)

        existing_stream.slides_src = slides_src
        existing_stream.stream_slides_name = slides_fetch_id
        existing_stream.smil_slides = slides_smil


def process_live_stream_forms(event):
    """
    Processes all the LiveStream objects of an Event, both existing and new when an Event is edited.
    
    :param event: 
    :param number_of_new_streams: 
    :return: 
    """
    # for existing_stream in event.live_streams:

    process_stream_form(event.live_stream, form_prefix='existing-stream-{}'.format(event.live_stream.id))
    event.is_synchronized = False
    db.session.commit()


@events_blueprint.route('/toggle-visibility/<event_id>/')
@requires_login
@admin_required
def toggle_visibility(event_id):
    """
    
    Changes the is_visible attribute of an event to True/False
    
    :param event_id: Id of the Event to change visibility
    :return: 
    """
    try:
        event = Event.query.get(event_id)

        event.is_visible = not event.is_visible
        db.session.commit()
        flash("Visibility changed to {} for event {}".format(event.is_visible, event_id), "success")

    except NoResultFound:
        flash("The selected event does not exist", "error")

    return redirect(request.referrer)


@events_blueprint.route('/toggle-follow-up/<event_id>/')
@requires_login
@admin_required
def toggle_follow_up(event_id):
    """
    Changes the is_following Event attribute to on/off and deletes/creates the Event FolloUps if needed.
    
    A new FollowUp will be generated for each LiveStream in the Event.
    The FolloUp will change the status of the Event from Upcoming to Live if the status of the on_air attribute is True.
    
    <Incomplete>

    :param event_id: 
    :return: 
    """
    try:
        event = Event.query.get(event_id)

        """
        If the event already has a follow_up we remove it and create a new one
        """
        if event.follow_ups:
            logger.debug("Event has follow ups")
            for follow_up in event.follow_ups:
                db.session.delete(follow_up)
            event.is_follow_up = False
            db.session.commit()
        else:
            """
            We create and assign the FollowUp to the event.
            If the event is Live, the follow up will be live.
            """
            followup_handler = FollowUpHandler()
            followup_handler.create_event_follow_ups(event)

        flash("Follow up changed to {} for event {}".format(event.is_visible, event_id), "success")

    except NoResultFound:
        flash("The selected event does not exist", "error")

    return redirect(request.referrer)


@events_blueprint.route('/toggle-is-synchronized/<event_id>/')
@requires_login
@admin_required
def toggle_is_synchronized(event_id):
    """
    Changes the is_following Event attribute to on/off and deletes/creates the Event FolloUps if needed.

    A new FollowUp will be generated for each LiveStream in the Event.
    The FolloUp will change the status of the Event from Upcoming to Live if the status of the on_air attribute is True.

    <Incomplete>

    :param event_id: 
    :return: 
    """
    try:
        event = Event.query.get(event_id)

        event.is_synchronized = not event.is_synchronized
        db.session.commit()
        flash("Syncrhonized changed to {} for event {}".format(event.is_synchronized, event_id), "success")

    except NoResultFound:
        flash("The selected event does not exist", "error")

    return redirect(request.referrer)


@events_blueprint.route('/_fetch_camera_smil/')
@requires_login
@admin_required
def _fetch_camera_smil():
    """
    Works using an AJAX call.
    Uses the LiveStreamForm form attributes to fetch the camera SMIL file. It will be managed using the WowzaService
    
    :return: JSON data with the status of the fetch and the path of the fetched file.
    """
    event_id = request.args.get('event_id', 0, type=int)
    indico_id = request.args.get('indico_id', 0, type=int)
    app_name = request.args.get('app_name', 0, type=str)
    stream_camera_name = request.args.get('stream_camera_name', 0, type=str)
    stream_camera_name = stream_camera_name if stream_camera_name != '' else None

    camera_result, camera_path = WowzaService().get_camera_smil(app_name, stream_camera_name=stream_camera_name)

    return jsonify(result={'camera': {'result': camera_result, 'path': camera_path}})


@events_blueprint.route('/_fetch_camera_slides_smil/')
@requires_login
@admin_required
def _fetch_camera_slides_smil():
    """
    Works using an AJAX call.
    Uses the LiveStreamForm form attributes to fetch the camera and slides SMIL files.
    It will be managed using the WowzaService

    :return: JSON data with the status of the fetch and the path of the fetched files.
    """
    event_id = request.args.get('event_id', 0, type=int)
    indico_id = request.args.get('indico_id', 0, type=int)
    app_name = request.args.get('app_name', 0, type=str)
    stream_id = request.args.get('stream_id', 0, type=str)

    stream_camera_name = request.args.get('stream_camera_name', 0, type=str)
    stream_camera_name = stream_camera_name if stream_camera_name != '' else None

    stream_slides_name = request.args.get('stream_slides_name', 0, type=str)
    stream_slides_name = stream_slides_name if stream_slides_name != '' else None

    slides_result, slides_path, camera_result, camera_path = WowzaService().get_camera_slides_smil(app_name,
                                                                                                   stream_id=stream_id,
                                                                                                   stream_camera_name=stream_camera_name,
                                                                                                   stream_slides_name=stream_slides_name)

    return jsonify(result={'camera': {'result': camera_result, 'path': camera_path},
                           'slides': {'result': slides_result, 'path': slides_path}})


def build_edit_event_page(event_id, event_type, go_live=None):
    event = Event.query.get_or_404(event_id)

    form = EventForm()
    save_before_go_live = form.save_before_go_live.data or None
    save_and_go_live = form.save_and_go_live.data or None

    logger.debug(
        "Save and preview is: {}, Save and go live is: {}".format(save_before_go_live, save_and_go_live))

    form = build_event_form(event)
    """
    We need to build the images folder
    """
    default_images_folders = [x[0] for x in os.walk(
        os.path.join(current_app.config.get('STATIC_FILES_PATH'), current_app.config.get('DEFAULT_IMAGES_FOLDER'),
                     current_app.config.get('DEFAULT_EVENTS_FOLDER')))]
    logger.debug("Event default image")
    logger.debug(form)
    """
    Generating a LiveStreamForm for every existing stream on the Event
    """
    streams_forms = []
    stream_form = LiveStreamForm(obj=event.live_stream, prefix='existing-stream-{}'.format(event.live_stream.id))
    stream_form.stream_id = event.live_stream.id
    stream_form.stream_type = event.live_stream.type
    streams_forms.append(stream_form)

    """
    Generating the CDS Record form
    """
    cds_record_form = CDSRecordForm(obj=event.cds_record)

    if request.method == 'POST':
        """
        Processing the streams
        """
        number_of_new_streams = request.form.get('number-of-streams', type=int)
        process_live_stream_forms(event)

        if form.validate_on_submit():
            logger.debug("Edit Form is valid")
            process_event_form(event.status, form, event_id=event_id)
        else:
            logger.debug(form.errors)

        """
        If go_live is True, we need to check if SMIL use is enabled and SMIL files were fetched.

        - Set the TimestampStarted to the current date.
        - Set the Event status to LIVE

        """
        # TODO GO_LIVE
        logger.debug(go_live)
        if go_live:
            event.timestamp_started = datetime.datetime.now()
            live_stream = event.live_stream
            logger.debug("Use smil process: {}".format(live_stream))
            if live_stream.use_smil:
                everything_is_fine = False
                if live_stream.type == 'camera_slides':
                    wowza_service = WowzaService()
                    if live_stream.slides_smil_fetched \
                            and live_stream.camera_smil_fetched \
                            and wowza_service.smil_file_exists(live_stream.stream_camera_name,
                                                               app_name=live_stream.app_name, stream_type='camera') \
                            and wowza_service.smil_file_exists(live_stream.stream_slides_name,
                                                               app_name=live_stream.app_name, stream_type='slides'):
                        everything_is_fine = True
                if live_stream.type == 'camera':
                    wowza_service = WowzaService()
                    if live_stream.camera_smil_fetched \
                            and wowza_service.smil_file_exists(live_stream.stream_camera_name,
                                                               app_name=live_stream.app_name,
                                                               stream_type='camera'):
                        everything_is_fine = True

                if save_before_go_live:
                    db.session.commit()
                    return redirect(
                        url_for('admin.events.{}_event_edit'.format(event_type), event_id=event.id,
                                go_live=save_before_go_live))

                if everything_is_fine:
                    if save_and_go_live:
                        try:
                            logger.info(
                                "User {} publishing event({}) indico_id: {} with SMIL: True".format(
                                    current_user.username,
                                    event.id,
                                    event.indico_id))
                        except AttributeError:
                            logger.info(
                                "User {} publishing event({}) indico_id: {} with SMIL: True".format("Annonymous user",
                                                                                                    event.id,
                                                                                                    event.indico_id))

                        event.status = EventStatus.LIVE
                        db.session.commit()
                        logger.info("[METRIC] Event went live")
                        return redirect(url_for('admin.events.events_live'))
                    else:
                        db.session.commit()
                else:
                    flash("Unable to go live: use_smil is True but SMIL files were not found.", "error")
                    return redirect(url_for('admin.events.upcoming_event_edit', event_id=event.id, go_live=True))
            else:

                if save_and_go_live:
                    try:
                        logger.info(
                            "User {} publishing event({}) indico_id: {} with SMIL: False".format(current_user.username,
                                                                                                 event.id,
                                                                                                 event.indico_id))
                    except AttributeError:
                        logger.info(
                            "User {} publishing event({}) indico_id: {} with SMIL: False".format("Annonymous user",
                                                                                                 event.id,
                                                                                                 event.indico_id))

                    event.status = EventStatus.LIVE
                    db.session.commit()
                    return redirect(url_for('admin.events.events_live'))
                else:
                    db.session.commit()

    return render_template('admin/events/events-edit.html',
                           event=event,
                           event_type=event_type,
                           form=form,
                           cds_record_form=cds_record_form,
                           streams_forms=streams_forms,
                           default_images_folders=default_images_folders,
                           wowza_edge_url=current_app.config.get('WOWZA_EDGE_URL'),
                           go_live=go_live)
