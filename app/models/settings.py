from app.extensions import db


class Settings(db.Model):
    """
    Represents the application settings
    """
    # TODO It's not used yet
    __tablename__ = 'settings'

    id = db.Column(db.String(255), primary_key=True)
    value = db.Column(db.Text, nullable=False)
    lastUpdate = db.Column(db.DateTime, nullable=False)
