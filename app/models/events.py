import datetime
import logging
import random
import string

import enum
import operator

import pytz
import sqlalchemy_utils
from flask import request, url_for
from wtforms import widgets

from app.common.datetime_helper import add_suffix_to_date
from app.extensions import db
from app.models.streams import build_vod_embed, build_webcast_embed

logger = logging.getLogger('webapp.models.events')

events_authorized_users = db.Table('events_authorized_users',

                                   db.Column('event_id', db.Integer, db.ForeignKey('events.id'),
                                             nullable=False),
                                   db.Column('authorized_user_id', db.Integer, db.ForeignKey('authorized_users.id'),
                                             nullable=False),
                                   db.PrimaryKeyConstraint('event_id', 'authorized_user_id'))


class DateTimeMixin(object):
    pytz_timezones = {x: x for x in sorted(pytz.all_timezones)}
    available_timezones = sorted(pytz_timezones.items(), key=operator.itemgetter(0))

    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    start_date = db.Column(db.DateTime, nullable=False, info={'label': 'Start date', 'widget': widgets.TextInput()})
    end_date = db.Column(db.DateTime, nullable=False, info={'label': 'End date', 'widget': widgets.TextInput()})
    timezone = db.Column(sqlalchemy_utils.types.choice.ChoiceType(available_timezones), nullable=False,
                         info={'label': 'Timezone'})

    timestamp_started = db.Column(db.TIMESTAMP, nullable=True)
    timestamp_ended = db.Column(db.TIMESTAMP, nullable=True)


class EventStatus(enum.Enum):
    UPCOMING = "Upcoming"
    LIVE = "Live"
    RECENT = "Recent"
    ARCHIVED = "Archived"


class Event(db.Model, DateTimeMixin):
    __tablename__ = 'events'

    required_attributes = ('indico_id', 'link', 'title', 'audience', 'timezone', 'status')

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, default='', nullable=False, info={'label': 'Title', 'widget': widgets.TextInput()})
    speakers = db.Column(db.Text, default='', info={'label': 'Speakers', 'widget': widgets.TextInput()})
    abstract = db.Column(db.Text, default='', info={'label': 'Abstract', 'widget': widgets.TextArea()})
    room = db.Column(db.String(100), nullable=True)

    link = db.Column(db.Text, info={'label': 'Link', 'description': 'leave it blank and won\'t be displayed',
                                    'widget': widgets.TextInput()})
    link_label = db.Column(db.Text, default='', info={'label': 'Link Label', 'widget': widgets.TextInput()})
    default_img = db.Column(db.String(255), nullable=True)
    custom_img = db.Column(db.String(255), nullable=True)
    use_default_img = db.Column(db.Boolean, nullable=False, default=True)

    extra_html = db.Column(db.Text, default='', info={'label': 'Extra HTML'})
    status = db.Column(db.Enum(EventStatus), default=EventStatus.UPCOMING)

    indico_id = db.Column(db.String(50), default='', nullable=True)
    indico_category_id = db.Column(db.String(50), default='', nullable=True)
    display_message = db.Column(db.Text, default='', nullable=True,
                                info={'label': 'Display Message', 'widget': widgets.TextInput()})
    is_synchronized = db.Column(db.Boolean, nullable=False, default=True)
    is_embeddable = db.Column(db.Boolean, nullable=False, default=True, info={'label': 'Embeddable'})
    is_sharable = db.Column(db.Boolean, nullable=False, default=True, info={'label': 'Sharable'})
    is_visible = db.Column(db.Boolean, nullable=False, default=True, info={'label': 'Visible'})
    is_follow_up = db.Column(db.Boolean, nullable=False, default=False)
    is_protected = db.Column(db.Boolean, nullable=False, default=False)

    # define relationship
    live_stream = db.relationship('LiveStream', backref='event', uselist=False)

    audience_id = db.Column(db.Integer, db.ForeignKey('audiences.id'), nullable=False)
    authorized_users = db.relationship('AuthorizedUser', secondary=events_authorized_users, backref='events')
    cds_record_id = db.Column(db.Integer, db.ForeignKey('cds_records.id'), nullable=True)

    follow_ups = db.relationship('FollowUp', backref='event', cascade="all, delete-orphan")

    random_string = db.Column(db.String(255))

    def __init__(self, room=None, indico_id=None, link=None, title=None, audience=None, timezone=None, status=None):
        self.room = room
        self.indico_id = indico_id
        self.link = link
        self.title = title
        self.audience = audience
        self.timezone = timezone
        self.status = status

        self.random_string = self.get_random_string(16)

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    def __repr__(self):
        return '<Event({}, indico_id: {}>'.format(self.id, self.indico_id)

    def get_random_string(self, length):
        """
         Get a random string of length

        :param length: The length of the string
        :type length: int
        :return: The random string
        :rtype: str
        """
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        # print("Random string of length", length, "is:", result_str)
        return result_str

    def get_date_as_indico(self, date):

        try:
            timezone = self.timezone.value
        except:
            timezone = self.timezone

        return {
            'day': date.strftime("%Y-%m-%d"),
            'tz': timezone,
            'time': date.strftime("%H:%M:%S")
        }

    def to_json(self, use_indico_date=False):

        result = {'id': self.id,
                  'title': self.title,
                  'speakers': self.speakers,
                  'abstract': self.abstract,
                  'room_name': self.room,
                  'event_link': self.link,
                  'img': self.get_image_full_url(),
                  'restricted': self.is_protected or self.is_restricted(),
                  'indico_category': self.indico_category_id,
                  'indico_id': self.indico_id,
                  'type': self.status.value,
                  'webcast_link': self.get_event_url(),
                  'ical_link': self.get_event_ical_link(),
                  'embed_link_camera': self.get_event_embed_link(),
                  'embed_link_slides': self.get_event_embed_link('slides')}

        if use_indico_date:
            result['startDate'] = self.get_date_as_indico(self.start_date)
            result['endDate'] = self.get_date_as_indico(self.end_date)
        else:
            result['start_date'] = self.start_date
            result['end_date'] = self.end_date
            result['timezone'] = self.timezone.value

        return result

    def to_json_v2(self, use_indico_date=False):

        result = {'id': self.id,
                  'title': self.title,
                  'speakers': self.speakers,
                  'abstract': self.abstract,
                  'room_name': self.room,
                  'extra_html': self.extra_html,
                  'img': self.get_image_full_url(),
                  'restricted': self.is_protected or self.is_restricted(),
                  'indico_id': self.indico_id,
                  'type': self.status.value,
                  'indico_link': self.link,
                  'ical_link': self.get_event_ical_link(),
                  }

        if use_indico_date:
            result['startDate'] = self.get_date_as_indico(self.start_date)
            result['endDate'] = self.get_date_as_indico(self.end_date)
        else:
            result['start_date'] = self.start_date
            result['end_date'] = self.end_date
            result['timezone'] = self.timezone.value

        return result

    def to_json_v2_with_streams(self, use_indico_date=False):

        if self.status.value == 'Archived' and not self.is_older_than(7):
            event_type = 'recent'
        else:
            event_type = self.status.value

        result = {'id': self.id,
                  'title': self.title,
                  'speakers': self.speakers,
                  'abstract': self.abstract,
                  'room_name': self.room,
                  'extra_html': self.extra_html,
                  'indico_link': self.link,
                  'img': self.get_image_full_url(),
                  'restricted': self.is_protected or self.is_restricted(),
                  'indico_id': self.indico_id,
                  'type': event_type,
                  'ical_link': self.get_event_ical_link(),
                  'stream': self.get_live_stream_json(),
                  'embed_code': self.get_event_embed_code()
                  }

        if use_indico_date:
            result['startDate'] = self.get_date_as_indico(self.start_date)
            result['endDate'] = self.get_date_as_indico(self.end_date)
        else:
            result['start_date'] = self.start_date
            result['end_date'] = self.end_date
            result['timezone'] = self.timezone.value

        return result

    def get_live_stream_json(self):
        return self.live_stream.get_json()

    def get_formated_date(self):
        return add_suffix_to_date(self.start_date, '%A {th} %B %Y at %H:%M')

    def get_image_full_url(self):
        url_root = request.url_root[:-1]
        return "{}{}".format(url_root, self.default_img)

    def get_event_url(self):
        url_root = request.url_root[:-1]

        return "{}{}".format(url_root, url_for('users.events_details', event_id=self.id))

    def get_event_embed_code(self):

        if "/vod/" in self.live_stream.camera_src or "/vod/" in self.live_stream.slides_src:
            # It's a vod
            player_url = build_vod_embed(stream=self.live_stream)
        else:
            # It's a embed
            player_url = build_webcast_embed(stream=self.live_stream)

        if self.status is EventStatus.LIVE:
            player_url = player_url + '&live=true'
        else:
            player_url = player_url + '&dvr=true'

        result = '<iframe type="text/html" width="720" height="360" src="{}" allowfullscreen="" frameborder="0"</iframe>'.format(
            player_url)
        return result

    def get_event_embed_link(self, stream='camera'):
        return ""

    def get_event_ical_link(self):
        url_root = request.url_root[:-1]

        return "{}{}".format(url_root, url_for('users.utils.get_ical', event_id=self.id))

    def is_restricted(self):
        """
        Check if the event is restricted or not
        If the event category is "No Restriction" or there are not authorized users in the event, it means the event
        is public
        """
        has_authorized_users = (len(self.authorized_users) > 0)
        has_restricted_audience = self.audience and self.audience.name.lower() != 'no restriction'

        if has_authorized_users or has_restricted_audience:
            return True
        return False

    def is_older_than(self, days=7):
        delta = datetime.datetime.now() - self.start_date
        if delta.days > days:
            return True
        return False


class FollowUp(db.Model):
    """
    Model for FollowUp
    """
    __tablename__ = 'follow_ups'

    id = db.Column(db.Integer, primary_key=True)
    app_name = db.Column(db.String(255), nullable=True)
    origin_server = db.Column(db.String(255), nullable=True)
    stream_name = db.Column(db.String(255), nullable=True)
    stream_type = db.Column(db.String(255), nullable=True)
    on_air = db.Column(db.Boolean, nullable=False, default=False)
    date = db.Column(db.DateTime, default=datetime.datetime.now)

    """
    The follow up will be associated to  a event
    """
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'))

    def __repr__(self):
        return '<FollowUp({}, {}, stream_name={}): {}>'.format(self.id, self.app_name, self.stream_name,
                                                               self.origin_server, self.on_air)


class CDSRecord(db.Model):
    __tablename__ = 'cds_records'
    required_attributes = ('cds_record_id', 'indico_link')

    id = db.Column(db.Integer, primary_key=True)
    cds_record_id = db.Column(db.Integer)
    indico_id = db.Column(db.String(50), default='', nullable=False)
    published_date = db.Column(db.DateTime)
    indico_link = db.Column(db.Text)
    video_file = db.Column(db.Text)
    mobile_file = db.Column(db.Text)
    image_url = db.Column(db.Text)
    speakers = db.Column(db.Text)
    featured = db.Column(db.Boolean, default=False)
    is_synchronized = db.Column(db.Boolean, default=True)
    room = db.Column(db.String(255))
    title = db.Column(db.String(255))
    is_protected = db.Column(db.Boolean, nullable=False, default=False)

    audience_id = db.Column(db.Integer, db.ForeignKey('audiences.id'), nullable=True)
    events = db.relationship('Event', backref="cds_record", lazy='dynamic')

    def __init__(self, cds_record_id=None, indico_link=None):
        self.cds_record_id = cds_record_id
        self.indico_link = indico_link

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    def get_cds_link(self):
        return 'https://cds.cern.ch/record/' + str(self.cds_record_id)

    def to_json_v2(self):

        result = {'id': self.id,
                  'title': self.title,
                  'image_url': self.image_url,
                  'speakers': self.speakers,
                  'published_date': self.published_date.strftime("%Y-%m-%d"),
                  'link': self.get_cds_link()
                  }

        return result
