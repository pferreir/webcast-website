from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.audiences import AuthorizedUser, Audience
import click


def add_authorized_user(user):
    try:
        authorized_user = AuthorizedUser.query.filter_by(name=user).one()
        click.echo("Authorized User {} already exists. Skipping.".format(user))
    except NoResultFound:
        authorized_user = AuthorizedUser(name=user)
        db.session.add(authorized_user)
        db.session.commit()
        click.echo("Added Authorized User {}. Ok.".format(user))
    return authorized_user


def add_initial_authorized_users_to_database():
    authorized_users = ['atlas-readaccess-current-physicists', 'webcast-team', 'it-dep-cis-avc', 'all-cern-users',
                        'cern-accounts-primary', 'cms-web-access', 'CDS Invenio']

    for user in authorized_users:
        add_authorized_user(user)


def add_initial_audiences_to_database():
    audiences = [
        {
            'name': 'ATLAS collaborators only',
            'default_application': 'atlas',
            'authorized_users': ['atlas-readaccess-current-physicists', 'webcast-team', 'it-dep-cis-avc']
        },
        {
            'name': 'CERN users only',
            'default_application': 'live',
            'authorized_users': ['all-cern-users', 'cern-accounts-primary']
        },
        {
            'name': 'CMS collaborators only',
            'default_application': 'cms',
            'authorized_users': ['webcast-team', 'it-dep-cis-avc', 'cms-web-access']
        },
        {
            'name': 'No restriction',
            'default_application': 'live',
            'authorized_users': []
        },
        {
            'name': 'public',
            'default_application': '',
            'authorized_users': []
        },
        {
            'name': 'recording-restricted',
            'default_application': '',
            'authorized_users': ['CDS Invenio']
        },
        {
            'name': 'atlas',
            'default_application': 'permatlas',
            'authorized_users': ['atlas-readaccess-active-members', 'atlas-com-external-support', 'it-dep-uds-avc',
                                 'it-dep-cis-avc']
        },
        {
            'name': 'atlas public',
            'default_application': 'AtlasLive',
            'authorized_users': []
        },
        {
            'name': 'cast',
            'default_application': 'cast',
            'authorized_users': ['satan-forum', 'it-dep-cis-avc']
        },
        {
            'name': 'Public_cern',
            'default_application': 'scalascreens',
            'authorized_users': []
        },
        {
            'name': 'tiina',
            'default_application': 'tiina',
            'authorized_users': ['tiina.benson@cern.ch', 'Paola.Catapano@cern.ch', 'Fabiola.Gianotti@cern.ch',
                                 'Beniamino.Di.Girolamo@cern.ch', 'Lucio.Rossi@cern.ch']
        },

    ]

    for audience in audiences:
        try:
            Audience.query.filter_by(name=audience['name']).one()
            click.echo("Audience {} already exists. Skipping.".format(audience['name']))
        except NoResultFound:
            new_audience = Audience(name=audience['name'], default_application=audience['default_application'])

            users_array = []
            for authorized_user in audience['authorized_users']:
                user = add_authorized_user(authorized_user)
                users_array.append(user)
            #
            new_audience.authorized_users = users_array

            db.session.add(new_audience)
            db.session.commit()
            click.echo("Added Audience {}. Ok.".format(audience['name']))
