from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.audiences import Audience
from app.models.initializers.audiences import add_authorized_user
from app.models.streams import Category, PermanentStream
from flask import current_app
import click


def add_initial_categories_to_database():
    categories = [
        {
            'name': 'Atlas',
            'priority': 1,
            'default_img': '/static-files/images/default/categories/atlas/category_atlas.png'
        },
        {
            'name': 'CAST',
            'priority': 2,
            'default_img': '/static-files/images/default/categories/cast/category_cast.png'
        },
        {
            'name': 'Development',
            'priority': 3,
            'default_img': '/static-files/images/default/categories/Development/permanent_noimage.jpg'
        },
        {
            'name': 'Microcosm',
            'priority': 4,
            'default_img': '/static-files/images/default/categories/Microcosm/Microcosm.jpg'
        },
    ]

    for category in categories:
        try:
            Category.query.filter_by(name=category['name']).one()
            click.echo("Category {} already exists. Skipping.".format(category['name']))
        except NoResultFound:
            pass
            new_category = Category(name=category['name'],
                                    priority=category['priority'],
                                    default_img=category['default_img'])
            db.session.add(new_category)
            db.session.commit()
            click.echo("Added Category {}. Ok.".format(category['name']))


def add_initial_permanent_streams_to_database():
    streams = [
        {
            'title': 'Control Room',
            'audience': 'atlas',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/Control_room.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream2.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': True,
            'authorized_users': ['atlas-readaccess-active-members', 'atlas-com-external-support', 'it-dep-cis-avc',
                                 'it-dep-uds-avc']
        },
        {
            'title': 'LHC & Machinery',
            'audience': 'atlas public',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/MachineInfo.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream3.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Point 1',
            'audience': 'atlas public',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/CollabInfo.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream1.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Portuguese',
            'audience': 'atlas public',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/Institution.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream4.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Public Outreach',
            'audience': 'atlas public',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/PublicOut.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream5.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Russian',
            'audience': 'atlas public',
            'category': 'atlas',
            'default_img': '/static-files/images/default/categories/atlas/Institution.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:stream6.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Camera 1',
            'audience': 'cast',
            'category': 'CAST',
            'default_img': '/static-files/images/default/categories/cast/camera1.jpg',
            'player_src': 'http://wowza.cern.ch:1935/cast/camera1/playlist.m3u8',
            'player_width': 640,
            'player_height': 480,
            'is_protected': True,
            'authorized_users': ['satan-forum', 'it-dep-cis-avc']
        },
        {
            'title': 'Camera 2',
            'audience': 'cast',
            'category': 'CAST',
            'default_img': '/static-files/images/default/categories/cast/camera2.jpg',
            'player_src': 'http://wowza.cern.ch:1935/cast/camera2/playlist.m3u8',
            'player_width': 640,
            'player_height': 480,
            'is_protected': True,
            'authorized_users': ['satan-forum', 'it-dep-cis-avc']
        },
        {
            'title': 'Camera 3',
            'audience': 'cast',
            'category': 'CAST',
            'default_img': '/static-files/images/default/categories/cast/camera3.jpg',
            'player_src': 'http://wowza.cern.ch:1935/cast/camera3/playlist.m3u8',
            'player_width': 640,
            'player_height': 480,
            'is_protected': True,
            'authorized_users': ['satan-forum', 'it-dep-cis-avc']
        },
        {
            'title': 'Channel 1',
            'audience': 'Public_cern',
            'category': 'Development',
            'default_img': '/static-files/images/default/categories/Development/permanent_noimage.jpg',
            'player_src': 'https://wowza.cern.ch/AtlasLive/smil:channel1.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
        {
            'title': 'Cloud Chamber',
            'audience': 'Public_cern',
            'category': 'Microcosm',
            'default_img': '/static-files/images/default/categories/Microcosm/Cloud_Chamber.jpg',
            'player_src': 'https://wowza.cern.ch/microcosm/cloud_chamber.smil/playlist.m3u8',
            'player_width': 1280,
            'player_height': 720,
            'is_protected': False,
            'authorized_users': []
        },
    ]

    for stream in streams:
        try:
            PermanentStream.query.filter_by(title=stream['title']).one()
            click.echo("Stream {} already exists. Skipping.".format(stream['title']))
        except NoResultFound:
            new_stream = PermanentStream(title=stream['title'],
                                         player_src=stream['player_src'],
                                         player_width=stream['player_width'],
                                         player_height=stream['player_height'],
                                         default_img=stream['default_img'])

            try:
                audience = Audience.query.filter_by(name=stream['audience']).one()
                new_stream.audience_id = audience.id
            except NoResultFound:
                click.echo(
                    "Audience {} doesn't exist. Unable to add it to PermanentStream {}".format(stream['audience'],
                                                                                               stream['title']))

            try:
                category = Category.query.filter_by(name=stream['category']).one()
                new_stream.category_id = category.id
            except NoResultFound:
                click.echo(
                    "Category {} doesn't exist. Unable to add it to PermanentStream {}".format(stream['category'],
                                                                                               stream['title']))

            users_array = []
            for authorized_user in stream['authorized_users']:
                user = add_authorized_user(authorized_user)
                users_array.append(user)
            #
            new_stream.authorized_users = users_array

            db.session.add(new_stream)
            db.session.commit()
            click.echo("Added PermanentStream {}. Ok.".format(stream['title']))
