# Translations template for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-02-13 10:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: app/templates/admin/base.html:3 app/templates/admin/base.html:7
msgid "Home"
msgstr ""

#: app/templates/admin/base.html:14
msgid "Logout"
msgstr ""

#: app/templates/admin/base.html:18
msgid "Login"
msgstr ""

#: app/templates/admin/base.html:56
msgid "Webcast Admin Panel"
msgstr ""

#: app/templates/users/base.html:60
msgid "Live Webcasts"
msgstr ""

#: app/templates/users/base.html:61
msgid "Permanent Webcasts"
msgstr ""

#: app/views/users/__init__.py:37
msgid "You have logged out"
msgstr ""

