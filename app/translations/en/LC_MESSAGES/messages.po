# English translations for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-02-13 10:27+0000\n"
"PO-Revision-Date: 2017-02-13 10:28+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: en\n"
"Language-Team: en <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: app/templates/admin/base.html:3 app/templates/admin/base.html:7
msgid "Home"
msgstr "home"

#: app/templates/admin/base.html:14
msgid "Logout"
msgstr "Logout"

#: app/templates/admin/base.html:18
msgid "Login"
msgstr "Login"

#: app/templates/admin/base.html:56
msgid "Webcast Admin Panel"
msgstr "Webcast Admin Panel"

#: app/templates/users/base.html:60
msgid "Live Webcasts"
msgstr "live Webcasts"

#: app/templates/users/base.html:61
msgid "Permanent Webcasts"
msgstr "Permanent Webcasts"

#: app/views/users/__init__.py:37
msgid "You have logged out"
msgstr "you have logged out"

