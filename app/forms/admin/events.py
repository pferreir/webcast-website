import os
import logging
from flask import flash

from app.common.utils import get_event_random_image
from app.handlers.events import EventHandler
from app.models.events import Event, CDSRecord
from flask_wtf.file import FileAllowed
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, FileField, SelectMultipleField, BooleanField
from wtforms.widgets.core import html_params
from wtforms.widgets import HTMLString

from app.extensions import images, db
from app.models.audiences import Audience, AuthorizedUser

from wtforms_alchemy import ModelForm

logger = logging.getLogger('webapp.events')

class AuthorizedUserForm(ModelForm, FlaskForm):
    """
    ModelForm for AuthorizedUser
    """

    class Meta:
        model = AuthorizedUser


class AudienceForm(ModelForm, FlaskForm):
    """
    ModelForm for Audience
    """

    class Meta:
        model = Audience

    authorized_users = SelectMultipleField('Authorized users', coerce=int)

    def __init__(self, formdata=None, *args, **kwargs):
        super().__init__(**kwargs)
        self.authorized_users.choices = [(authorized_user.id, authorized_user.name) for authorized_user in
                                         AuthorizedUser.query.all()]


class FetchEventForm(FlaskForm):
    event_id = StringField()


class UnpublishEventForm(FlaskForm):

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.event_id = kwargs.get('event_id')
        self.indico_id = kwargs.get('indico_id')


class InlineButtonWidget(object):
    """
    Render a basic ``<button>`` field.
    """
    input_type = 'submit'
    html_params = staticmethod(html_params)

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('type', self.input_type)
        kwargs.setdefault('value', field.name)
        kwargs.setdefault('name', field.name)
        return HTMLString('<button %s>%s</button>' % (
            html_params(**kwargs),
            field.label.text
        ))


class InlineSubmitField(BooleanField):
    """
    Represents an ``<button type="submit">``.  This allows checking if a given
    submit button has been pressed.
    """
    widget = InlineButtonWidget()


class EventForm(ModelForm, FlaskForm):
    class Meta:
        model = Event
        exclude = ['custom_img']

    audience = SelectField('Audience', choices=[], coerce=int)
    authorized_users = SelectMultipleField('Authorized users', coerce=int)

    custom_img = FileField('Custom Image', validators=[
        FileAllowed(images, 'Images only!')
    ])

    save_and_go_live = InlineSubmitField(label='<i class="ui upload icon"></i> Save and Go Live')
    save_before_go_live = InlineSubmitField(label='<i class="ui save icon"></i> Save')

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.audience.choices = [(audience.id, audience.name) for audience in Audience.query.all()]
        self.authorized_users.choices = [(authorized_user.id, authorized_user.name) for authorized_user in
                                         AuthorizedUser.query.all()]


class CDSRecordForm(ModelForm, FlaskForm):
    """
    ModelForm for CDSRecord
    """

    class Meta:
        model = CDSRecord


def process_event_form(status, form, event_id=None):
    """
    Processes the Event form and populates an event with it's data, creating the related audiences if necessary.

    :param status: Status of the event to be added or updated
    :param form: Form to get the data from
    :param event_id: Only needed to tell the user which event was updated.
    :return: 
    """
    event = None
    if event_id:
        event = Event.query.get(event_id)
    audience = Audience.query.get(form.audience.data)

    logger.debug("This is the custom_img")
    logger.debug(form.custom_img.data)
    if event:
        if event.title != form.title.data \
                or event.abstract != form.abstract.data \
                or event.speakers != form.speakers.data \
                or event.room != form.room.data \
                or event.link != form.link.data \
                or event.link_label != form.link_label.data \
                or event.display_message != form.display_message.data \
                or event.extra_html != form.extra_html.data \
                or event.indico_id != form.indico_id.data \
                or event.indico_category_id != form.indico_category_id.data \
                or event.timezone != event.timezone \
                or event.start_date != event.start_date \
                or event.end_date != event.end_date:
            event.is_synchronized = False

    if not event:
        event = Event(room=form.room.data, indico_id=form.indico_id, link=form.link.data, title=form.title.data,
                      audience=audience, timezone=form.timezone.data, status=status)

    event.title = form.title.data
    event.speakers = form.speakers.data
    event.abstract = form.abstract.data
    event.room = form.room.data
    event.link = form.link.data
    event.link_label = form.link_label.data
    event.display_message = form.display_message.data
    event.extra_html = form.extra_html.data
    event.indico_id = form.indico_id.data
    event.indico_category_id = form.indico_category_id.data
    event.timezone = form.timezone.data
    event.start_date = form.start_date.data
    event.end_date = form.end_date.data

    event.is_embeddable = form.is_embeddable.data or False
    event.is_sharable = form.is_sharable.data or False
    event.is_visible = form.is_visible.data or False
    event.is_protected = form.is_protected.data or False
    event.use_default_img = form.use_default_img.data or False

    event.audience = audience

    event.authorized_users = []
    users_array = []
    for user in form.authorized_users.data:
        found_user = AuthorizedUser.query.get(user)
        if found_user:
            users_array.append(found_user)

    """
    If not default_img=form.default_img.data then we have to get automatically an image.
    Get the latest 3 events for the current audience.
    """
    default_img = form.default_img.data
    default_img = get_event_random_image(audience.id, default_img)
    event.default_img = default_img

    if form.custom_img.data:
        f = form.custom_img.data
        filename = "{}_custom.{}".format(str(event_id), f.filename.split(".")[1])
        img_path = os.path.join(
            "/opt/app-root/src/app/static-files/images/custom_events", filename
        )
        f.save(img_path)
        logger.debug("Photo saved.")
        event.custom_img = os.path.join("/static-files/images/custom_events", filename)

    if event_id:
        db.session.commit()
        flash("Event #{} was updated successfully".format(event_id), "success")
    else:
        db.session.add(event)
        event_handler = EventHandler()
        event_handler.add_default_live_stream(event)

        db.session.commit()
        flash("Event #{} was created successfully".format(event.id), "success")

    event.authorized_users = users_array
    db.session.commit()

    return event
