from app.models.api import ApiKey
from flask_wtf import FlaskForm

from wtforms_alchemy import ModelForm, ModelFormField, ModelFieldList


class ApiKeyForm(ModelForm, FlaskForm):
    """
    ModelForm for ApiKey Model
    """
    class Meta:
        model = ApiKey
        exclude = ['access_key', 'secret_key']