import logging

import hashlib
import hmac
import time
from urllib.parse import urlencode
from contextlib import closing

import requests
from werkzeug.datastructures import MultiDict
from flask import current_app

logger = logging.getLogger('webapp.indico')

class IndicoAPIClient(object):
    """
    Class and methods to connect to and use Indico HTTP API.
    Check Indico docs for more info: http://indico.readthedocs.org/en/master/http_api/
    """
    API_BASE = '/api/'
    EXPORT_EVENT_PATH = '/export/event/{event_id}.json'
    EXPORT_WEBCAST_PATH = '/export/webcast-recording.json'

    def get_webcasts(self, from_date=None, to_date=None):
        """
        Returns all the webcasts between the parameters dates
        :param from_date: Date in isoformat without the seconds (3 last characters)
        :param to_date: Date in isoformat without the seconds (3 last characters)
        :return: 
        """

        arg_params = (('from', from_date), ('to', to_date), ('detail', 'event'), ('service', 'webcast'),)
        path = self.EXPORT_WEBCAST_PATH

        return self._indico_request('GET', path, arg_params=arg_params)

    def get_event(self, event_id):
        """
        Returns the details of an Event
        :param event_id: Event ID
        :return: 
        """
        path = self.EXPORT_EVENT_PATH.format(event_id=event_id)
        return self._indico_request('GET', path, arg_params=())

    def _build_indico_request(self, path, params, api_key=None, secret_key=None, only_public=False):
        """
        Generates an Indico API request with the hmac signature.
        :param path: Relative path to the indico API. I.E. /api/
        :param params: Parameters of the request
        :param api_key: Api key
        :param secret_key: Secret Key
        :param only_public: Wether should access public data only or not
        :return: The generated Indico request parameters
        """
        items = params.items() if hasattr(params, 'items') else list(params)
        if api_key:
            items.append(('ak', api_key))
        if only_public:
            items.append(('onlypublic', 'yes'))
        if secret_key:
            items.append(('timestamp', str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            url = '%s?%s' % (path, urlencode(items))
            signature = hmac.new(bytearray(secret_key, "UTF-8"), bytearray(url, "UTF-8"), hashlib.sha1).hexdigest()
            items.append(('signature', signature))
        return items

    def _indico_request(self, method, path, arg_params):
        """
        Makes a request to the  Indico API
        
        :param method: The method type of the request (GET or POST)
        :param path: The JSON path of the request.
        :param arg_params: Extra parameters of the request. They depend on the path.
        :return: The content type of the response and the response text
        """
        indico_url = current_app.config.get('INDICO_URL') + path
        request_values = MultiDict(arg_params)
        method = method.upper()
        params = request_values.items(multi=True)

        data = self._build_indico_request(path, params, current_app.config.get('INDICO_API_KEY'),
                                          current_app.config.get('INDICO_API_SECRET_KEY'))
        request_args = {'params': data} if method == 'GET' else {'data': data}
        try:
            logger.debug(
                "Making a {} request to Indico: {}".format(method, indico_url))
            response = requests.request(method, indico_url, verify=False, **request_args)
        except requests.HTTPError as e:
            response = e.response
        except requests.ConnectionError as e:
            return 'text/plain', str(e)
        content_type = response.headers.get('Content-Type', '').split(';')[0]
        with closing(response):
            return content_type, response.text
