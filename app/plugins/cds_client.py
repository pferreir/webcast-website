import logging

import os

import datetime
import pymarc
from pathlib2 import Path

import requests
from flask import current_app
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.audiences import Audience
from app.models.events import CDSRecord, Event, EventStatus


class CdsClient(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger('webapp.cds')
        else:
            self.logger = logger

    def get_recent_events(self):
        cds_events_dicts = self.fetch_recent_events()

        """
        We want to delete the existing events and add the new ones.
        """
        self.logger.info("Fetching recent CDS events -> Start")

        for cds_event_dict in cds_events_dicts:

            new_cds_events = CDSRecord.query.filter_by(indico_id=cds_event_dict['indico_id']).all()
            for cds_event in new_cds_events:
                db.session.delete(cds_event)
                db.session.commit()

            """
            Find the audience that matches
            """
            try:
                audience = Audience.query.filter_by(name=cds_event_dict['audience']).one()
            except NoResultFound:
                audience = Audience(name=cds_event_dict['audience'])
                db.session.add(audience)
                db.session.commit()

            python_date = datetime.datetime.strptime(cds_event_dict['published_date'],
                                                     '%Y-%m-%dT%H:%M:%S')  # 2017-05-30T11:00:00

            new_cds_event = CDSRecord(cds_record_id=cds_event_dict['cds_record_id'],
                                      indico_link=cds_event_dict['indico_link'])
            new_cds_event.title = cds_event_dict['title']
            new_cds_event.indico_id = cds_event_dict['indico_id']
            new_cds_event.cds_record_id = cds_event_dict['cds_record_id']
            new_cds_event.room = cds_event_dict['room']
            new_cds_event.published_date = python_date
            new_cds_event.image_url = cds_event_dict['image_url']

            self.replace_http_with_https(new_cds_event)

            new_cds_event.video_file = cds_event_dict['video']
            new_cds_event.mobile_file = cds_event_dict['mobile']
            new_cds_event.audience = audience
            new_cds_event.speakers = cds_event_dict['speakers']

            db.session.add(new_cds_event)
            db.session.commit()

            """
            There can be more than one event with the same id. If the cds_record indico_id match with any of it, we add it
            """
            events = Event.query.filter_by(indico_id=cds_event_dict['indico_id']).all()

            for event in events:
                event.status = EventStatus.RECENT
                event.cds_record = new_cds_event
                new_cds_event.events.append(event)
                db.session.commit()

        self.logger.info("Fetching recent CDS events -> Finished")

    @staticmethod
    def replace_http_with_https(new_cds_event):
        if new_cds_event.image_url:
            if new_cds_event.image_url.startswith("http://"):
                new_cds_event.image_url = "https://" + new_cds_event.image_url[7:]

    def fetch_recent_events(self):
        response = requests.get(current_app.config.get('CDS_API_URL'))

        temp_file_path = os.path.join(current_app.config.get('TEMP_FILES_DEST'), 'cds.xml')

        p = Path(temp_file_path)
        response.encoding = 'utf-8'

        p.write_text(response.text)

        records = []
        for record in pymarc.parse_xml_to_array(temp_file_path):
            parsed_record = self._parse_marc21_record(record)
            if parsed_record['indico_id'] is not None:
                records.append(parsed_record)

        return records

    def _parse_marc21_record(self, record_marc21):

        title2 = record_marc21.title()
        title = record_marc21['111']['a']

        cds_record_id = record_marc21['001'].value()

        try:
            indico_id = record_marc21['970']['a']
            indico_id_split = indico_id.split(".")
            indico_id = indico_id_split[1]
        except TypeError:
            indico_id = None

        room = record_marc21['111']['c']
        input_date_time = record_marc21['111']['9']
        indico_link = record_marc21['856']['u']
        try:
            audience = record_marc21['506']['a']
            if audience == 'Restricted':
                audience = 'recording-public'
        except TypeError:
            audience = 'public'

        temp_media = record_marc21.get_fields('856')
        image = None
        video = None
        mobile = None
        try:
            speakers = record_marc21['700']['a']
        except TypeError:
            speakers = ""

        for temp in temp_media:
            if '7' == temp.indicator1:
                if temp['x'] == 'pngthumbnail':
                    image = temp['u']
                if temp['x'] == 'jpgposterframe':
                    image = temp['u']
                elif temp['x'] == 'mp4master.mp4':
                    video = temp['u']
                elif temp['x'] == 'mp4mobile':
                    mobile = temp['u']

        result = {
            'title': title,
            'indico_id': indico_id,
            'cds_record_id': cds_record_id,
            'room': room,
            'published_date': input_date_time,
            'indico_link': indico_link,
            'image_url': image,
            'video': video,
            'mobile': mobile,
            'audience': audience,
            'speakers': speakers,
        }

        self.logger.debug(result)

        return result
