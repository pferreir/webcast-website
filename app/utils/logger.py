from datetime import datetime
import logging
from pytz import timezone, utc
import sys
from logging import handlers
from app.utils.uma_logger import UMAHandler


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_logs(app, logger_name, to_stdout=True, to_remote=False, to_file=False):
    logger = logging.getLogger(logger_name)

    if app.config['LOG_LEVEL'] == 'DEV':
        logger.setLevel(logging.DEBUG)

    if app.config['LOG_LEVEL'] == 'PROD':
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(pathname)s | %(funcName)s():%(lineno)d')

    logging.Formatter.converter = zurich_time

    if to_stdout:
        configure_stdout_logging(logger=logger, formatter=formatter, log_level=app.config['LOG_LEVEL'])

    if to_remote:
        configure_remote_logging(logger=logger, app=app)

    if to_file:
        configure_file_logging(logger=logger, formatter=formatter, app=app, file_name=logger_name)


def configure_stdout_logging(logger=None, formatter=None, log_level="DEV"):
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if log_level == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)
    print("Logging {logger_name} to stdout -> True".format(logger_name=str(logger)))


def configure_remote_logging(logger=None, app=None, formatter=None):

    if not app.config.get('LOG_REMOTE_PRODUCER', None) or not app.config.get('LOG_REMOTE_TYPE', None):
        print("ERROR -> Unable to configure the remote logging. Attributes not set.")

    handler = UMAHandler(app.config.get('LOG_REMOTE_PRODUCER', None), app.config.get('LOG_REMOTE_TYPE', None))

    handler.setFormatter(formatter)

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception

    logger.addHandler(handler)

    if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
        handler.setLevel(logging.DEBUG)

    if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
        handler.setLevel(logging.INFO)

    logger.addHandler(handler)
    print("Logging {logger_name} to remote -> True".format(logger_name=str(logger)))

def configure_file_logging(logger=None, app=None, formatter=None, file_name="application"):
    log_file_name = '{log_file_path}/{name}.log'.format(
        log_file_path=app.config.get("LOG_FILE_PATH", "/opt/app-root/src/logs"), name=file_name)
    try:
        file_handler = handlers.TimedRotatingFileHandler(log_file_name,
                                                                 when='midnight', interval=7, backupCount=5)
        file_handler.setFormatter(formatter)
        if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
            file_handler.setLevel(logging.DEBUG)
        if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
            file_handler.setLevel(logging.INFO)

        logger.addHandler(file_handler)
        print("Logging {logger_name} to file ({file_name}) -> True".format(logger_name=str(logger),
                                                                           file_name=log_file_name))

    except Exception as e:
        print("It seems there is a problem with the volume. Logs will be only on stdout. Error: {}".format(e))