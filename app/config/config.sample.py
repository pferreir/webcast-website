import os

# This line is only required during development if not using SSL
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

ADMIN_EGROUP = "webcast-team"
APP_PORT = 8080
SECRET_KEY = (
    "E\xf0\xd2G\xd5\x0bJ\xfd\x0b\xc7\xdc\x8c\xfb\xbd\xf7\xcd&C\xa3\xbc\xc8\xf7\xeb5"
)

# Debug and logging configuration
DEBUG = True
IS_LOCAL_INSTALLATION = True
TESTING = False

# LOGGING
# Values:
# DEV: For debug level
# PROD: For info level
LOG_LEVEL = "DEV"
# To use remote logging (Central Monitoring)
LOG_REMOTE_ENABLED = False
# Type for the remote logging (webcast-prod|webcast-test)
LOG_REMOTE_TYPE = ""
# Producer for the remote logging (webcast)
LOG_REMOTE_PRODUCER = ""
# FILE LOGGING
LOG_FILE_ENABLED = True
LOG_FILE_PATH = "/opt/app-root/src/logs"

# Whether or not to use the wsgi Proxy Fix
USE_PROXY = True

# For the follow up
API_URL_PREFIX = "/api"
ALLOWED_API_IPS = [
    "188.184.36.202",
    "188.184.36.203",
    "128.142.200.46",
    "137.138.108.121",
    "188.185.65.83",
]
FETCH_UPCOMING_API_KEY = ""

# OAUth/OpenID config
# https://application-portal.web.cern.ch/
# This configuration is for the login in the website, not for using a client
CERN_OPENID_CLIENT_ID = "<CLIENT ID in the CERN Applications portal>"
CERN_OPENID_CLIENT_SECRET = "<CLIENT SECRET>"


# DB Configuration
# Values:
# postgresql: To use a postgres database
# mysql: To use a mysql database
# sqlite: Fallback if any of the previous values is not set
DB_SERVICE_NAME = "postgresql"
DB_NAME = "postgres"
DB_PASS = "postgres"
DB_PORT = 5432
DB_USER = "postgres"

# Indico API
# Used to retrieve events
INDICO_URL = "https://indico.cern.ch"
INDICO_API_KEY = ""
INDICO_API_SECRET_KEY = ""

# WOWZA Streaming
# Origin server URL list
WOWZA_ORIGIN_URLS = ["https://wowzaqaorigin.cern.ch", "https://wowzaqaedge.cern.ch"]
# Origin server URL
WOWZA_EDGE_URL = "https://wowzaqaedge.cern.ch"
# Location where the SMIL files will be generated
WOWZA_SMIL_FOLDER = "/eos/media/av-sorenson/website/test/smil"
# Connection parameters to retrieve the SMIL files
WOWZA_USERNAME = ""
WOWZA_PASSWORD = ""

# CDS API URL to retrieve recent events
CDS_SERVER = "https://cds.cern.ch/record/"
CDS_API_URL = "https://cds.cern.ch/search?p=340__a%3A%22Streaming+video%22+and+collection%3AIndico+and+%28collection%3ATALK+or+collection%3ARestricted_ATLAS_Talks+or+collection%3ARestricted_CMS_Talks%29+AND+8567_x%3Apngthumbnail&f=&action_search=Search&c=CERN+Document+Server&rg=12&sc=0&of=xm&sf=269__c&so=d%22"

# Images
IMAGES_FOLDER = "images"
PERMANENT_STREAM_IMAGES_FOLDER = "permanent"
EVENTS_IMAGES_FOLDER = "events"

# Location of the uploaded files
UPLOADED_EVENTS_IMAGES_DEST = "/opt/app-root/src/app/static-files/images/custom_events"
TEMP_FILES_DEST = "/opt/app-root/src"

# Location of the default images
STATIC_FILES_PATH = "/opt/app-root/src/app/static-files"
STATIC_URL_PATH = "/static-files"
DEFAULT_IMAGES_FOLDER = "	 images/default"
DEFAULT_CATEGORIES_FOLDER = "categories"
DEFAULT_EVENTS_FOLDER = "events"

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_OAUTH_TIMEOUT = 300

#
# Analytics
#
ANALYTICS_PIWIK_ID = "3722"

BUILD_ASSETS = False


"""
Add here the model modules to be loaded automatically on the application factory
"""
DB_MODELS_IMPORTS = (
    "app.models.api",
    "app.models.events",
    "app.models.streams",
    "app.models.users",
    "app.models.settings",
)

WOWZA_SECURETOKEN = {"app1": "XXXXXXXXXX", "app2": "YYYYYYYYYY"}
