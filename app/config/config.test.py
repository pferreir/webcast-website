import os

_basedir = os.path.abspath(os.path.dirname(__file__))

TESTING = True
WTF_CSRF_ENABLED = False
WTF_CSRF_SECRET_KEY = "somethingimpossibletoguess"
SECRET_KEY = "This string will be replaced with a proper key in production."
APPLICATION_ROOT = "/"
CERN_OAUTH_CLIENT_ID = "test"
CERN_OAUTH_CLIENT_SECRET = "test"
CERN_OAUTH_TOKEN_URL = ""
CERN_OAUTH_AUTHORIZE_URL = ""

SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(_basedir, "test_app.db")
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_RECORD_QUERIES = False

# Images
IMAGES_FOLDER = "images"
PERMANENT_STREAM_IMAGES_FOLDER = "permanent"
EVENTS_IMAGES_FOLDER = "events"
UPLOADED_FILES_DEST = "/opt/app-root/src/uploads"
UPLOADED_IMAGES_DEST = "/opt/app-root/src/uploads/images"

UPLOADED_EVENTS_IMAGES_DEST = "/opt/app-root/src/uploads"

CONF_DIRPATH_JSON = "/opt/app-root/src/uploads"

APP_PORT = 8080
DEBUG = True
# Available values:
# DEV: Development purposes with DEBUG level
# PROD: Production purposes with INFO level
LOG_LEVEL = "PROD"

# Needed to use Oauth
USE_PROXY = True

IS_LOCAL_INSTALLATION = True
API_URL_PREFIX = "/api"

STATIC_FILES_PATH = "/opt/app-root/src/app/static-files"
DEFAULT_IMAGES_FOLDER = "images/default"
DEFAULT_CATEGORIES_FOLDER = "categories"
DEFAULT_EVENTS_FOLDER = "events"

# available languages
BABEL_DEFAULT_LOCALE = "en"

INDICO_URL = "http://indico.corn"
INDICO_API_KEY = "42"
INDICO_API_SECRET_KEY = "42"

WOWZA_ORIGIN_URLS = ["https://wowzatest.test.cern", "https://wowzatest.test.cern"]
WOWZA_EDGE_URL = "https://wowzatest.test.cern"
WOWZA_SMIL_FOLDER = "/opt/app-root/src/uploads"

CDS_SERVER = "https://cdsweb.cern.ch/record/"
CDS_API_URL = "https://cdsweb.cern.ch/search?p=300__a%3A%22Streaming+video%22+and+collection%3AIndico+and+%28collection%3ATALK+or+collection%3ARestricted_ATLAS_Talks+or+collection%3ARestricted_CMS_Talks%29+AND+8567_x%3Apngthumbnail&f=&action_search=Search&c=CERN+Document+Server&rg=12&sc=0&of=xm&sf=269__c&so=d"

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = ""
CACHE_REDIS_PORT = ""
CACHE_OAUTH_TIMEOUT = 300

ANALYTICS_PIWIK_ID = ""

# Video Player
VIDEO_PLAYER_URL = "https://video-player.web.cern.ch"

BUILD_ASSETS = False
if os.environ.get("CI_PROJECT_DIR", None):
    TEMP_FILES_DEST = os.path.join(os.environ.get("CI_PROJECT_DIR"), "tests", "samples")
else:
    TEMP_FILES_DEST = os.path.join("/", "opt", "app-root", "src", "tests", "samples")

WOWZA_SECURETOKEN = {"app1": "XXXXXXXXXX", "app2": "YYYYYYYYYY"}
