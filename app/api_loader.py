from app.api.private.v1.load_api import load_api_namespaces as load_api_private_namespaces

def load_api_private_blueprints():
    """
    Loads all the blueprints, adding the development ones if needed
    :param app: Flask application instance
    :return: A tuple with all the blueprints
    """
    all_blueprints = (
        load_api_private_namespaces(),
    )

    return all_blueprints
