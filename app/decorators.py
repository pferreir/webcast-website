import logging

from functools import wraps
from flask import redirect, abort, url_for
from flask import current_app
from flask_login import current_user
from oauthlib.oauth2 import TokenExpiredError

from app.common.authentication.cern_openid import get_user_egroups_from_session
from app.extensions import db

logger = logging.getLogger('webapp.decorators')

def requires_login(f):
    """
    Add this decorator to the views that require a logged in user.
    
    :param f: Function that will be wrapped with this decorator.
    :return: The decorated function
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated and not current_app.config['TESTING']:
            return redirect(url_for('cern_openid.login'))
        return f(*args, **kwargs)
    return decorated_function


def admin_required(f):
    """
    Add this decorator to the views that require a logged in Admin user.

    :param f: Function that will be wrapped with this decorator.
    :return: The decorated function
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        testing = False
        if current_app.config['TESTING']:
            testing = True

        logger.debug("Checking if is admin...")
        if not testing:

            # First check if the user is authenticated to try to give him
            # admin rights
            if current_user.is_authenticated and not current_user.is_admin:
                try:
                    egroups = get_user_egroups_from_session()

                    if current_app.config.get('ADMIN_EGROUP') in egroups:
                        current_user.is_admin = True
                    else:
                        current_user.is_admin = False

                    db.session.commit()
                except TokenExpiredError:
                    return redirect(url_for('cern_openid.login'))
            if not current_user.is_authenticated or not current_user.is_admin:
                abort(401)
        return f(*args, **kwargs)
    return decorated_function
