import random
import sys
import jwt
import logging

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

from app.models.users import OAuth, User
from flask_login import current_user, LoginManager, login_user

from app.extensions import db, cache
from flask import session
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend

FIFTEEN_MINUTES_IN_SECONDS = 900
THIRTY_MINUTES_IN_SECONDS = FIFTEEN_MINUTES_IN_SECONDS * 2

logger = logging.getLogger("webapp.openid")


def get_user_egroups_from_session():
    """
    Retrieves the egroups of the user from the oauth api or from the cache
    :type username: string used to identify the user. Used for caching purposes
    :return:
    """

    logger.debug("Getting egroups from session ")
    try:
        roles = session["roles"]
        return roles
    except KeyError as error:
        logger.info(f"User does not have roles. He is annonymous ({error})")
        return []


def load_cern_openid(app):
    """
    Loads the CERN Openid into the application

    :param app: Flask application where the CERN Openid will be loaded
    :return:
    """
    openid = OAuth2ConsumerBlueprint(
        "cern_openid",
        __name__,
        url_prefix="/openid",
        # openid specific settings
        token_url="https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token",
        authorization_url="https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth",
        # local urls
        login_url="/cern",
        authorized_url="/cern/authorized",
        client_id=app.config.get("CERN_OPENID_CLIENT_ID", ""),
        client_secret=app.config.get("CERN_OPENID_CLIENT_SECRET", ""),
    )

    app.register_blueprint(openid)

    openid.backend = SQLAlchemyBackend(
        OAuth, db.session, user=current_user, cache=cache
    )

    # setup login manager
    login_manager = LoginManager()
    login_manager.login_view = "cern_openid.login"

    @login_manager.user_loader
    def load_user(user_id):
        try:
            return User.query.get(int(user_id))
        except sqlalchemy.exc.InternalError as error:
            logger.warning(str(error))
            return None

    login_manager.init_app(app)

    @oauth_authorized.connect_via(openid)
    def cern_logged_in(blueprint, token):
        # it anymore after getting the data here.
        # response = openid.session.get('https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo')
        payload = jwt.decode(token["access_token"], verify=False)

        query = User.query.filter_by(username=payload["cern_upn"].strip())

        try:
            existing_user = query.one()
            logger.info(
                f"User {existing_user.username} was found with person_id {existing_user.person_id}"
            )
        except NoResultFound:
            try:
                first_name = (payload["given_name"].strip(),)
            except AttributeError:
                first_name = "Unknown"

            try:
                last_name = (payload["family_name"].strip(),)
            except AttributeError:
                last_name = "Unknown"
            logger.debug(payload)
            try:
                person_id = payload["cern_person_id"]
            except KeyError:
                # Generate a random person_id since service accounts don't have one
                person_id = str(random.randint(0, round(sys.maxsize)))

            existing_user = User(
                username=payload["cern_upn"].strip(),
                person_id=person_id,
                email=payload["email"],
                last_name=last_name,
                first_name=first_name,
            )

            db.session.add(existing_user)

        is_admin = False
        if "admins" in payload["cern_roles"]:
            is_admin = True
        logger.debug(payload)
        existing_user.is_admin = is_admin
        db.session.commit()

        session["roles"] = payload["cern_roles"]

        if login_user(existing_user):
            logger.info(
                f"Login user {existing_user.username} with person_id: {existing_user.person_id}"
            )

    return openid
