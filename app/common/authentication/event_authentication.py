import re
import logging

from flask import g
from flask_login import current_user
from oauthlib.oauth2 import TokenExpiredError

from app.common.authentication.cern_openid import get_user_egroups_from_session

logger = logging.getLogger('webapp.auth')


def is_user_authorized(event):
    """
    Checks if a user is authorized to view an Event

    :param event: Event to be checked
    :return: True | False if the user it allowed to view the resource
    """
    # {'email': 'rene.fernandez@cern.ch',
    # 'first_name': 'Rene', 'last_name': 'Fernandez Sanchez', 'uid': 'fernanre',
    # 'roles': ['admins', 'cern-accounts-primary']}
    if event.is_protected or event.is_restricted():
        user = g.user
        # If user is admin
        if user is None:
            return False

        if "admins" in user["roles"]:
            logger.debug(
                "Current user allowed to access the event: is_admin=True")
            return True

        user_egroups = user["roles"]

        if user is not None:
            # If user identity is: cern registered or cern shared
            # AND event.audience.name is CERN users only
            if "cern-accounts-primary" in user_egroups and event.audience.name.lower() == 'cern users only':
                logger.info(
                    "User ({}) is a CERN User -> Allowed.".format(user["uid"]))
                return True

            # Check email in authorized users
            is_email_authorized = is_email_in_authorized_users(email=user["email"],
                                                               authorized_users=event.authorized_users)
            if is_email_authorized:
                return True
            logger.info(
                "User ({}) email is not in authorized users.".format(user["uid"]))

            # Check on the audience authorized users
            audience_authorized_users = event.audience.authorized_users

            is_email_authorized = is_email_in_authorized_users(email=user["email"],
                                                               authorized_users=audience_authorized_users)
            if is_email_authorized:
                logger.info(
                    "User ({}) owns email {} -> Authorized".format(user["uid"], user["email"]))
                return True

            # Check egroups in authorized users
            for egroup in user_egroups:
                is_egroup_authorized = is_egroup_in_authorized_users(egroup=egroup,
                                                                     authorized_users=event.authorized_users)
                if is_egroup_authorized:
                    logger.info(
                        "User ({}) owns egroup {} -> Authorized".format(user["uid"], egroup))
                    return True

            for egroup in user_egroups:
                is_egroup_authorized = is_egroup_in_authorized_users(egroup=egroup,
                                                                     authorized_users=audience_authorized_users)
                if is_egroup_authorized:
                    logger.info(
                        "User ({}) owns egroup {} -> Authorized".format(user["uid"], egroup))
                    return True

            logger.info(
                "User ({}) egroups are not in authorized users egroups.".format(user["uid"]))

        return False
    return True


def is_user_authorized_for_permanent_stream(stream):
    """
    Checks if a user is authorized to view a PermanentStream

    :param stream: PermanentStream to be checked
    :return: True | False if the user it allowed to view the resource
    """

    if stream.audience:
        audience_authorized_users = stream.audience.authorized_users
    else:
        audience_authorized_users = []
    audicence_empty = audience_authorized_users == []
    authorized_users_empty = stream.authorized_users == []

    if audicence_empty and authorized_users_empty:
        return True
    logger.debug(audicence_empty)
    logger.debug(authorized_users_empty)
    if current_user.is_authenticated and current_user.is_admin:
        return True

    if current_user.is_authenticated:
        # Check email in authorized users
        is_email_authorized = is_email_in_authorized_users(email=current_user.email,
                                                           authorized_users=stream.authorized_users)
        if is_email_authorized:
            return True

        # Check on the audience authorized users
        audience_authorized_users = stream.audience.authorized_users

        is_email_authorized = is_email_in_authorized_users(email=current_user.email,
                                                           authorized_users=audience_authorized_users)
        if is_email_authorized:
            return True

        try:
            logger.debug("Checking egroups for user {}".format(current_user.username))
            egroups_info = get_user_egroups_from_session()
        except TokenExpiredError:
            return False

        # for egroup in current_user.egroups:
        for egroup_name in egroups_info.json()['groups']:
            is_egroup_authorized = is_egroup_in_authorized_users(egroup=egroup_name,
                                                                 authorized_users=audience_authorized_users)
            if is_egroup_authorized:
                return True

    return False


def is_email_in_authorized_users(email=None, authorized_users=None):
    """
    Checks all the authorized_users names for emails and verifies if the current user email is among them.

    :param email: Current user email
    :param authorized_users: List with all the authorized users
    :return: True|False if the email is found
    """
    for authorized_user in authorized_users:
        if re.match(r"[^@]+@[^@]+\.[^@]+", authorized_user.name):  # If it is an email
            if authorized_user.name == email:
                return True
    return False


def is_egroup_in_authorized_users(egroup=None, authorized_users=None):
    """
    Checks all the authorized_users names for egroups and verifies if the current user egroup is among them.

    :param egroup: Current egroup to search
    :param authorized_users:  List with all the authorized users
    :return:  True|False if the egroup is found
    """
    for authorized_user in authorized_users:
        if not re.match(r"[^@]+@[^@]+\.[^@]+", authorized_user.name):  # If it is not an email
            if egroup in [authorized_user.name for authorized_user in authorized_users]:
                return True
    return False
