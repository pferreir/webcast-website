import datetime
import logging
from dateutil import tz
from flask import current_app

logger = logging.getLogger('webapp.datetime_helper')

def get_date_day_suffix(day):
    return str(day) + ("th" if 4 <= day % 100 <= 20 else {1: "st", 2: "nd", 3: "rd"}.get(day % 10, "th"))


def add_suffix_to_date(dt, f):
    return dt.strftime(f).replace("{th}", get_date_day_suffix(dt.day))


def extract_future_events(events, after_date=None):
    """
    Timezones might be different for each event, so we need to convert the dates to our current timezone and
    verify that the events we are processing are in the future.
    
    :param events: 
    :param after_date: 
    :return: 
    """
    logger.debug("Extracting future events ({} found)...".format(len(events)))
    not_passed_events = []
    to_zone = tz.gettz('Europe/Zurich')

    if not after_date:
        today = datetime.datetime.now(to_zone)
        after_date = datetime.datetime(today.year, today.month, today.day, 0, 0, tzinfo=today.tzinfo)
    else:
        after_date = datetime.datetime(year=after_date.year, month=after_date.month,
                                       day=after_date.day,
                                       hour=after_date.hour, minute=after_date.minute,
                                       tzinfo=to_zone)

    for event in events:
        logger.debug("Checking date for Indico ID: {}".format(event['event_id']))
        timezone = event['startDate']['tz']
        start_date = event['startDate']['date']
        start_time = event['startDate']['time']

        event_start_date = generate_timezone_aware_datetime(start_date, start_time, timezone)

        logger.debug("Event start date is {}".format(event_start_date))

        if timezone != 'Europe/Zurich':
            event_start_date = event_start_date.astimezone(tz=to_zone)

        if event_start_date > after_date:
            logger.debug(
                "Event date {} is newer than origin data {}. Adding to events...".format(event_start_date, after_date))
            not_passed_events.append(event)
        else:
            logger.debug(
                "Event date {} is older than origin data {}. Adding to events...".format(event_start_date, after_date))

    logger.debug("{} future events found".format(len(not_passed_events)))

    return not_passed_events


def generate_timezone_aware_datetime(date, time, timezone):
    """
    Returns a datetime on the selected timezone
    
    :param date: 
    :param time: 
    :param timezone: 
    :return: 
    """

    logger.debug("Generating timezone aware datetime for event...")

    date_splitted = date.split("-")
    time_splitted = time.split(":")
    event_start_date = datetime.datetime(year=int(date_splitted[0]), month=int(date_splitted[1]),
                                         day=int(date_splitted[2]),
                                         hour=int(time_splitted[0]), minute=int(time_splitted[1]),
                                         tzinfo=tz.gettz(name=timezone))
    return event_start_date


def generate_date_from_string(date, time):
    date_and_time = '{date} {time}'.format(date=date, time=time)
    return datetime.datetime.strptime(date_and_time, "%Y-%m-%d %H:%M:%S")


def generate_time_from_string(time):
    return datetime.datetime.strptime(time, "%H:%M:%S")
