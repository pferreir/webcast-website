import logging
from app.models.events import CDSRecord

class CdsHandler(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger('webapp.cds')
        else:
            self.logger = logger

    def get_most_recent_cds_records(self, limit):
        cds_records = CDSRecord.query.filter_by(is_protected=False).order_by(CDSRecord.published_date.desc()).limit(limit)
        return cds_records

    def __repr__(self):
        return "%s" % self.__class__.__name__
