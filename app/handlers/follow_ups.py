import re
import logging
from flask import current_app

from app.extensions import db
from app.handlers.events import EventFollowUpHandler
from app.models.events import FollowUp, EventStatus


class FollowUpHandler(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger('webapp.follow_up')
        else:
            self.logger = logger

    def create_follow_up_from_stream_name(self, event, stream_name=None, stream_type=None, app_name=None, on_air=None,
                                          origin_server=None):
        if not stream_name:
            return None
        self.logger.debug("Creating FollowUp with stream_name: {}".format(stream_name))
        follow_up = FollowUp()
        self.fill_follow_up_from_params(follow_up, stream_name=stream_name,
                                        stream_type=stream_type,
                                        event=event,
                                        app_name=app_name,
                                        origin_server=origin_server,
                                        on_air=on_air)
        """
            Persisting...
        """
        db.session.add(follow_up)
        follow_up.event = event
        event.follow_ups.append(follow_up)
        db.session.commit()

    def create_event_follow_ups(self, event):
        """
        Adds a new event to be followed: insert one row per stream/origin server
        id is IndicoID, streams array of streams for the event, status 0 or 1

        :param event: Event to add the FollowUps
        :return: 
        """

        """
        Get the livestream data
        """
        first_livestream = event.live_stream

        """
        Generate a followup for each of the stream sources
        - camera
        - slides (if livestream type is camera_slides)
        """

        # Camera
        camera_src = first_livestream.camera_src
        follow_up = FollowUp()
        self._fill_follow_up_data_from_src(follow_up, stream_src=camera_src, stream_type='camera',
                                           event=event)
        follow_up.stream_name = first_livestream.stream_camera_name
        """
        Persisting...
        """
        db.session.add(follow_up)
        follow_up.event = event
        event.follow_ups.append(follow_up)
        db.session.commit()

        # Slides
        if first_livestream.type == 'camera_slides':
            slides_src = first_livestream.slides_src
            follow_up = FollowUp()
            if event.status == EventStatus.LIVE:
                follow_up.on_air = True
            else:
                follow_up.on_air = False

            self._fill_follow_up_data_from_src(follow_up, stream_src=slides_src, stream_type='slides',
                                               event=event)
            follow_up.stream_name = first_livestream.stream_slides_name
            """
            Persisting...
            """
            db.session.add(follow_up)
            follow_up.event = event
            event.follow_ups.append(follow_up)

        event.is_follow_up = True
        db.session.commit()

        return

    def notify(self, follow_up, on_air=False):

        self.logger.debug("FollowUp -> notify()")

        self.logger.debug("on_air will be {}".format(on_air))
        if not on_air:
            follow_up.on_air = False
            db.session.commit()
            self.logger.debug("Will unPublish the event: {}".format(follow_up.event_id))
            event_followup_handler = EventFollowUpHandler(logger=self.logger)
            event_followup_handler.unpublish(follow_up.event, stream_name=follow_up.stream_name)
            # TODO Send email

            """
            $emailText = "The follow-up feature is enabled for the event " . $eventArray['id'] . " and it has been automatically removed now";
            $emailSubject = '[WWW Webcast] Event '.$eventArray['id'].' automatically removed';
            mail(CONF_MAIL_TO, $emailSubject, $emailText, $emailHeaders);
            """

    def fill_follow_up_from_params(self, follow_up, stream_name=None, stream_type=None, event=None, origin_server=None,
                                   app_name=None, on_air=False):

        follow_up.app_name = app_name
        follow_up.stream_name = stream_name
        follow_up.date = event.start_date
        follow_up.stream_type = stream_type
        follow_up.on_air = on_air
        follow_up.origin_server = origin_server

        event.is_follow_up = True

    def _fill_follow_up_data_from_src(self, follow_up, stream_src=None, stream_name=None, stream_type=None, event=None,
                                      app_name=None, on_air=None):
        """
        Get app and stream from the video source: ex: 
          - app -> 'livehd'
          - stream -> '446536_camera_all'

        :param slides_src: 
        :return: 
        """
        if not stream_src and not stream_name:
            raise ValueError("stream_camera_name or stream_src must be defined")
        if not event:
            raise ValueError("event must be defined")

        if on_air is None:
            if event.status == EventStatus.LIVE:
                follow_up.on_air = True
            else:
                follow_up.on_air = False

        event.is_follow_up = True

        if stream_src:
            proccess_this = '{wowza_url}/(\w+)/smil:(\w+).smil/playlist.m3u8'.format(
                wowza_url=current_app.config.get('WOWZA_EDGE_URL'))
            """
                Get app and stream from the video source: ex: app->'livehd', stream->'446536_camera_all'
            """
            search_results = re.search(proccess_this, stream_src)
            if search_results:
                app_name = search_results.group(1)
                if not stream_name:
                    stream_name = search_results.group(2)

        follow_up.app_name = app_name
        follow_up.stream_name = stream_name
        follow_up.date = event.start_date
        follow_up.stream_type = stream_type

        splitted_origin = current_app.config.get('WOWZA_EDGE_URL').split("/")

        follow_up.origin_server = splitted_origin[2]
