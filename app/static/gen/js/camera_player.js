'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 *
 * @param {String }message
 * @constructor
 */
function PlayerFactoryException(message) {
    this.message = message;
    this.name = "PlayerFactoryException";
}

function isMobileDevice() {
    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

    return isMobile;
};

function isDesktopSafari() {
    return navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1 && !isMobileDevice();
};

function getCurrentDateString() {
    return '[' + new Date().toUTCString() + '] ';
}

var PlayerFactory = function () {
    function PlayerFactory() {
        var slidesConfig = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var cameraConfig = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, PlayerFactory);

        this.slidesConfig = slidesConfig;
        this.cameraConfig = cameraConfig;

        this.shareWindow = null;
        this.cameraConfig.isReady = false;
        this.slidesConfig.isReady = false;

        this.slidesPlayer = null;
        this.cameraPlayer = null;
    }

    _createClass(PlayerFactory, [{
        key: 'info',
        value: function info(message) {
            console.log(getCurrentDateString(), message);
        }
    }, {
        key: 'debug',
        value: function debug(message) {
            console.debug(getCurrentDateString(), message);
        }
    }, {
        key: 'error',
        value: function error(message) {
            console.error(getCurrentDateString(), message);
        }
    }, {
        key: 'warning',
        value: function warning(message) {
            console.warning(getCurrentDateString(), message);
        }
    }, {
        key: '_createPlayer',
        value: function _createPlayer(config) {
            var element = document.getElementById(config.divElementId);
            this.debug("Creating player for element: " + config.divElementId);
            element.className = 'video-js theoplayer-skin theo-seekbar-above-controls ' + config.additionalClasses + ' ' + config.divElementId;

            var player = new THEOplayer.Player(element, {
                libraryLocation: '/static-files/js/libs/theoplayer/',
                ui: {
                    width: '100%',
                    fluid: true
                },
                plugins: {
                    googleAnalytics: true
                },
                isEmbeddable: true,
                initialRendition: 'low'
            });

            if (!config.displayControls) {
                var controlBar = element.querySelector('.vjs-control-bar');
                controlBar.style.display = 'none';
                var pause_on_click = element.querySelector('.vjs-tech');
                pause_on_click.style.pointerEvents = 'none';
            }
            player.muted = config.muted;
            player.source = {
                sources: [{
                    src: config.url,
                    type: 'application/x-mpegurl'
                }]
            };
            player.autoplay = true;

            return player;
        }
    }, {
        key: 'detachShare',
        value: function detachShare(screenshare_url) {
            var _this = this;

            this.info("Detaching View");

            var height = screen.height / 2;
            var width = screen.width / 2;

            var left = screen.width / 2 - width / 2;
            var top = screen.height / 2 - height / 2;

            $(".meeting-self-view").hide();

            this.shareWindow = window.open(screenshare_url, "mywindow", "toolbar=false,menubar=false,resizable=1,width=" + width + ",height=" + height + ", top=" + top + ",left=" + left + "");
            window.addEventListener("message", function (evt) {
                _this.shareWindow.postMessage(['src', _this.cameraPlayer.src], "*");
                _this.shareWindow.postMessage(['position', _this.slidesPlayer.currentTime], "*");
                _this.shareWindow.postMessage(['pause', _this.slidesPlayer.paused], "*");
            }, false);

            this.shareWindow.addEventListener('beforeunload', function () {
                _this.info("External Window Closed");
                _this.shareWindow = null;
                $(".meeting-self-view").show();
            });
        }
    }, {
        key: 'slidesOnPause',
        value: function slidesOnPause() {
            this.info("Slides -> Pause");

            if (this.slidesPlayer) {
                this.cameraPlayer.pause();
                if (this.shareWindow) {
                    this.shareWindow.postMessage(['pause', this.slidesPlayer.paused], "*");
                }
            }
        }
    }, {
        key: 'slidesCanPlay',
        value: function slidesCanPlay() {
            this.info("Event -> slidesCanPlay");

            this.slidesConfig.isReady = true;

            if (this.slidesConfig.isReady && this.cameraConfig.isReady) {
                if (this.slidesPlayer) {
                    var webcastLoader = $('.webcast-loader');
                    webcastLoader.removeClass("active");
                }
            }
        }
    }, {
        key: 'cameraCanPlay',
        value: function cameraCanPlay() {
            this.info("Event -> cameraCanPlay");

            this.cameraConfig.isReady = true;

            if (this.slidesConfig.isReady && this.cameraConfig.isReady) {
                if (this.slidesPlayer) {
                    var webcastLoader = $('.webcast-loader');
                    webcastLoader.removeClass("active");
                }
            }
        }
    }, {
        key: 'getElementByClassName',
        value: function getElementByClassName(_ref) {
            var divElementId = _ref.divElementId;

            return document.getElementsByClassName(divElementId)[0];
        }
    }, {
        key: 'slidesOnPlay',
        value: function slidesOnPlay() {
            this.info("Slides -> Play");

            if (this.slidesPlayer) {
                this.cameraPlayer.play();

                if (this.shareWindow) {
                    this.shareWindow.postMessage(['play'], "*");
                }
            }
        }
    }, {
        key: 'slidesOnSeek',
        value: function slidesOnSeek() {
            this.info("Slides -> Seek");

            if (this.slidesPlayer) {
                this.cameraPlayer.currentTime = this.slidesPlayer.currentTime;
                if (this.shareWindow) {
                    this.shareWindow.postMessage(['seek', this.slidesPlayer.currentTime], "*");
                }
            }
        }
    }, {
        key: 'cameraOnPause',
        value: function cameraOnPause() {
            this.info("Camera -> Pause");

            if (this.slidesPlayer) {
                this.slidesPlayer.pause();
                if (this.shareWindow) {
                    this.shareWindow.postMessage(['pause', this.slidesPlayer.paused], "*");
                }
            }
        }
    }, {
        key: 'cameraOnPlay',
        value: function cameraOnPlay() {
            this.info("Camera -> Play");
            // if (isDesktopSafari()) {
            //     if (this.slidesPlayer) {
            //         this.slidesPlayer.play();
            //     }
            // }
        }
    }, {
        key: 'isCameraWaiting',
        value: function isCameraWaiting() {
            this.info("Event -> cameraWaiting");
            this.cameraConfig.isReady = false;
            var webcastLoader = $('.webcast-loader');
            webcastLoader.addClass("active");
        }
    }, {
        key: 'areSlidesWaiting',
        value: function areSlidesWaiting() {
            this.info("Event -> slidesWaiting");
            this.slidesConfig.isReady = false;
            var webcastLoader = $('.webcast-loader');
            webcastLoader.addClass("active");
        }
    }, {
        key: 'areSlidesPlaying',
        value: function areSlidesPlaying() {
            this.info("Event -> slidesPlaying");
            var webcastLoader = $('.webcast-loader');
            webcastLoader.removeClass("active");
        }
    }, {
        key: 'isCameraPlaying',
        value: function isCameraPlaying() {
            this.info("Event -> cameraPlaying");
            var webcastLoader = $('.webcast-loader');
            webcastLoader.removeClass("active");
        }
    }, {
        key: 'prepareUI',
        value: function prepareUI() {

            if (this.cameraPlayer) {
                var cameraElement = $('.' + this.cameraConfig.divElementId);
                var slidesElement = $('.' + this.slidesConfig.divElementId);

                slidesElement.find('.vjs-volume-control').css("display", "none");
                slidesElement.find('.vjs-mute-control').css("display", "none");

                cameraElement.find('.vjs-progress-control').css("display", "none");
                cameraElement.find('.vjs-time-controls').css("display", "none");
                cameraElement.find('.vjs-time-divider').css("display", "none");
                cameraElement.find('.vjs-play-control').css("display", "none");
            }

            if (isMobileDevice()) {
                var webcastLoader = $('.webcast-loader');
                webcastLoader.removeClass("active");
            }
        }
    }, {
        key: 'keepPlayersinSync',
        value: function keepPlayersinSync() {

            if (this.slidesPlayer) {
                this.slidesPlayer.addEventListener('pause', this.slidesOnPause.bind(this));
                this.slidesPlayer.addEventListener('play', this.slidesOnPlay.bind(this));
                this.slidesPlayer.addEventListener('seeked', this.slidesOnSeek.bind(this));
                this.slidesPlayer.addEventListener('canplay', this.slidesCanPlay.bind(this));
                this.slidesPlayer.addEventListener('waiting', this.areSlidesWaiting.bind(this));
                this.slidesPlayer.addEventListener('playing', this.areSlidesPlaying.bind(this));
            }

            if (this.cameraPlayer) {
                this.cameraPlayer.addEventListener('pause', this.cameraOnPause.bind(this));
                this.cameraPlayer.addEventListener('play', this.cameraOnPlay.bind(this));
                this.cameraPlayer.addEventListener('canplay', this.cameraCanPlay.bind(this));
                this.cameraPlayer.addEventListener('waiting', this.isCameraWaiting.bind(this));
                this.cameraPlayer.addEventListener('playing', this.isCameraPlaying.bind(this));
            }
        }
    }, {
        key: 'createCameraSlidesPlayers',
        value: function createCameraSlidesPlayers() {
            this.info("Creating players for camera and slides");
            this.createSlidesPlayer();
            if (!isMobileDevice()) {
                this.createCameraPlayer();
                this.keepPlayersinSync();
            }
            this.prepareUI();
        }
    }, {
        key: 'createSlidesPlayer',
        value: function createSlidesPlayer() {

            this.slidesPlayer = this._createPlayer(this.slidesConfig);
            if (this.slidesConfig.useAnalytics) {
                this.addAnalyticsToPlayer(this.slidesPlayer);
            }
        }
    }, {
        key: 'createCameraPlayer',
        value: function createCameraPlayer() {
            this.cameraPlayer = this._createPlayer(this.cameraConfig);
            if (this.cameraConfig.useAnalytics) {
                this.addAnalyticsToPlayer(this.cameraPlayer);
            }
        }
    }, {
        key: 'addAnalyticsToPlayer',
        value: function addAnalyticsToPlayer(player) {
            this.info("Adding analytics to player");
            // Add Google Analytics
            var TRACK_EVENTS = ['ended', 'error', 'pause', 'play', 'playing', 'seeked', 'seeking', 'stalled', 'warning'],
                sendEvent = function sendEvent(event) {
                ga('send', 'event', {
                    hitType: 'event',
                    eventCategory: 'video',
                    eventAction: event.type,
                    eventLabel: this.src,
                    eventValue: Math.floor(this.currentTime)
                });
            },
                event_index = void 0;

            for (event_index = 0; event_index < TRACK_EVENTS.length; event_index += 1) {
                player.addEventListener(TRACK_EVENTS[event_index], sendEvent);
            }
        }
    }]);

    return PlayerFactory;
}();


'use strict';

var cameraConfig = {
    url: cameraUrl,
    muted: false,
    divElementId: 'bigVideoPlaceholder',
    additionalClasses: 'bgVid',
    displayControls: true,
    useAnalytics: true
};

var playerFactory = new PlayerFactory({}, cameraConfig);

playerFactory.createCameraPlayer();

