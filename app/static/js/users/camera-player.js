
let cameraConfig = {
    url: cameraUrl,
    muted: false,
    divElementId: 'bigVideoPlaceholder',
    additionalClasses: 'bgVid',
    displayControls: true,
    useAnalytics: true
};

let playerFactory = new PlayerFactory({}, cameraConfig);

playerFactory.createCameraPlayer();