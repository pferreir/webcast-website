$(document).ready(function () {

    $('.menu .item').tab();

    $('.ui.accordion').accordion();

    $('#foot-abstract').readmore({
        lessLink: '<a href="#"><i class="caret up icon"></i> Read Less</a>',
        moreLink: '<a href="#"><i class="dropdown icon"></i> Read More</a>',
        collapsedHeight: 100
    });

    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        });
    $(".embed-button").click(function () {
        $(".embed-modal").modal('show');
    });
});
