/**
 * Main JS.
 */

$(document).ready(function () {
    $('.ui.dropdown').dropdown();

    $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
    ;


});

function openSocialShareWindow(linkUrl) {
    console.debug("Open facebook share window");
    var height = screen.height / 2;
    var width = screen.width / 2;
    var left = (screen.width / 2) - (width / 2);
    var top = (screen.height / 2) - (height / 2);
    window.open(linkUrl, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600,top=' + top + ',left=' + left);
}
