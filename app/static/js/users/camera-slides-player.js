let cameraConfig = {
    url: cameraUrl,
    muted: false,
    divElementId: 'player',
    additionalClasses: 'permanent-stream embed',
    displayControls: true,
    useAnalytics: true

};

cameraConfig.displayControls = true;

let slidesConfig = {
    url: slidesUrl,
    muted: true,
    divElementId: 'slides-player',
    additionalClasses: 'permanent-stream embed',
    displayControls: true,
    useAnalytics: true
};

let playerFactory = new PlayerFactory(slidesConfig, cameraConfig);
playerFactory.createCameraSlidesPlayers();