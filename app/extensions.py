from flask_breadcrumbs import Breadcrumbs
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, IMAGES
from flask_caching import Cache
from flask_migrate import Migrate

from app.config import config

db = SQLAlchemy()
migrate = Migrate()

breadcrumbs = Breadcrumbs()

"""
We define here the image formats that we allow for uploading.
We add '' to the UploadSet so uploading an image will be optional, otherwise it will return an error on form submit.
"""
images = UploadSet("images", ["png", "jpg", ""])

"""
Event images handling.
We define the default destination of the uploaded images
"""
events_images = UploadSet(
    "images", IMAGES, default_dest=config.UPLOADED_EVENTS_IMAGES_DEST
)

#
# Cahing: By default, caching is disabled, but this can be changed using the configuration parameters in config
#
cache = Cache(config={"CACHE_TYPE": "null"})
