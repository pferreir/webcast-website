import re

from flask import current_app
from ldap3 import Server, Connection, MOCK_SYNC, ALL, SUBTREE, ALL_ATTRIBUTES, SIMPLE

import logging

from app.extensions import cache

logger = logging.getLogger("webapp.services.ldap")

class CernLdapHandler:

    LDAP_TIMEOUT = 10800 # seconds. 3 hours

    @staticmethod
    @cache.memoize(LDAP_TIMEOUT)
    def get_egroups_from_user(employee_id):
        logger.debug("Not using a cached method")
        ldap_client = CernLdapClient()
        egroups = ldap_client.get_user_egroups_by_employee_id(employee_id)
        return egroups

    @staticmethod
    @cache.memoize(LDAP_TIMEOUT)
    def is_user_admin(employee_id):
        logger.debug("Not using a cached method")
        ldap_client = CernLdapClient()
        is_admin = ldap_client.is_user_admin_by_employee_id(employee_id)
        return is_admin


class CernLdapUserAttributes:
    egroups = 'memberOf'
    cn = 'cn'
    group = 'cernGroup'
    department = 'division'
    section = 'cernSection'
    uid = 'uid'
    office = 'physicalDeliveryOfficeName'
    email = 'mail'
    display_name = 'displayName'
    phone = 'telephoneNumber'
    phone_secondary = 'otherTelephone'
    employee_id = 'employeeID'
    account_type = 'cernAccountType'
    surname = 'sn'
    phone_mobile = 'mobile'
    first_name = 'givenName'


class CernLdapClient(object):
    server_url = None
    use_test_server = None
    server = None
    base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
    user_dn = "o=cern,c=ch"
    conn = None

    sample_server_info_path = 'app/services/ldap/sample_queries/my_real_server_info.json'
    sample_server_schema_path = 'app/services/ldap/sample_queries/my_real_server_schema.json'
    sample_query_path = 'app/services/ldap/sample_queries/my_real_server_entries.json'

    def __init__(self, server_url="xldap.cern.ch"):
        if current_app.config['TESTING']:
            self.configure_test_server()
        else:
            self.server_url = server_url
            """
            In order to get the server info 
            the parameter `get_info=ALL` must be added to Server 
            i.e.: Server(self.server_url, get_info=ALL)
            """

            self.server = Server(self.server_url,
                                 port=636,
                                 use_ssl=True,
                                 # get_info=ALL
                                 )

    def configure_test_server(self):
        """
        Creates a mock server for testing purposes
        :return:
        """
        self.server_url = 'fake_server'
        server_info = self.sample_server_info_path
        server_schema = self.sample_server_schema_path

        logger.debug("Loading test LDAP server")

        # Create a fake server from the info and schema json files
        self.server = Server.from_definition('my_fake_server', server_info, server_schema)

        conn = Connection(self.server,
                          client_strategy=MOCK_SYNC)
        # Populate the DIT of the fake server
        # conn.strategy.entries_from_json(self.sample_query_path)
        self._set_connection(conn)
        self.conn.bind()

    def _set_connection(self, connection):
        self.conn = connection

    def _make_connection(self):
        logger.debug("Making LDAP connection")
        if not self.conn:
            self.conn = Connection(self.server,
                                   auto_bind=True
                                   )

    def _get_server_info(self):
        """
        Saves two files, one with the server info and the other with an example of a query
        for testing purposes.
        :return:
        """
        self.server.info.to_file(self.sample_server_info_path)
        self.server.schema.to_file(self.sample_server_schema_path)
        self.conn.response_to_file(self.sample_query_path, raw=True)

    def get_user_egroups_by_employee_id(self, employee_id):
        """
        Obtains a list of all the egroups from a user by his user id.
        :param employee_id: Employee ID of the person
        :return: 
        """
        logger.debug("Get user egroups by employee id: {}".format(employee_id))
        self._make_connection()
        search_filter = "(&(employeeID={})(cernAccountType=Primary))".format(employee_id)
        self.conn.search(search_base=self.base_dn,
                         search_filter=search_filter,
                         attributes=[CernLdapUserAttributes.egroups])

        ldap_groups = self.conn.response[0]["attributes"]["memberOf"]
        self.conn.unbind()

        parsed_groups = self._parse_user_egroups(ldap_groups)

        return parsed_groups

    def is_user_admin_by_employee_id(self, employee_id):
        """
        Checks if a user is admin by fetching the ADMIN_EGROUP from his egroups
        :param employee_id: Employee ID of the person
        :return: (True|False) Boolean
        """
        logger.debug("Checking if user is admin: {}".format(employee_id))
        self._make_connection()
        member_of='CN={},OU=e-groups,OU=Workgroups,DC=cern,DC=ch'.format(current_app.config['ADMIN_EGROUP'])
        search_filter = "(&(employeeID={})(cernAccountType=Primary)(memberOf={}))".format(employee_id, member_of)
        self.conn.search(search_base=self.base_dn,
                         search_filter=search_filter,
                         attributes=[CernLdapUserAttributes.cn])

        is_admin = False
        try:
            username = self.conn.response[0]
            if username:
                is_admin = True
        except IndexError:
            is_admin = False
        finally:
            self.conn.unbind()

        return is_admin

    def _parse_user_egroups(self, ldap_groups):
        """
        Parses all the egroups in ldap format obtaining a list with their names
        :param egroups: egroups attribute in ldap format
        :return: A list of strings
        """
        found_egroups = []
        for egroup in ldap_groups:
            match = re.search('CN=(.+?),OU=e-groups,OU=Workgroups,DC=cern,DC=ch', egroup)
            if match:
                found = match.group(1)
                found_egroups.append(found)
        return found_egroups
