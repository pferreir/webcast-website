from datetime import datetime
import os

import click
import logging
from apscheduler.schedulers.blocking import BlockingScheduler

from app.handlers.events import EventUpcomingHandler

from app.extensions import db
from app.models.initializers.audiences import add_initial_authorized_users_to_database, \
    add_initial_audiences_to_database
from app.models.initializers.streams import add_initial_categories_to_database, \
    add_initial_permanent_streams_to_database
from app.plugins.cds_client import CdsClient


def initialize_cli(app):

    """
    Add additional commands to the CLI. These are loaded automatically on the main.py
    
    :param app: App to attach the cli commands to
    :return: None
    """
    @app.cli.command()
    def init_db():
        """
        Initialize the database.
        """
        click.echo('Initializing the db')
        db.create_all()

    @app.cli.command()
    def clear_db():
        """
        Removes all the tables of the database and their content
        """
        click.echo('Clearing the db')
        db.drop_all()
        click.echo('Initializing the db')
        db.create_all()

    @app.cli.command()
    def initialize_data():
        """
        Removes all the tables of the database and their content
        """
        click.echo('Filling the database with sample data')
        add_initial_categories_to_database()
        add_initial_authorized_users_to_database()
        add_initial_audiences_to_database()
        add_initial_permanent_streams_to_database()

    @app.cli.command()
    def update_cds_records():
        with app.app_context():
            cron_logger = logging.getLogger('webapp.cron')
            cron_logger.info("Updating CDS Records from CLI")
            CdsClient(logger=cron_logger).get_recent_events()
            cron_logger.info("Finished Updating CDS Records from CLI")

    @app.cli.command()
    def update_upcoming_events():
        with app.app_context():
            cron_logger = logging.getLogger('webapp.cron')
            cron_logger.info("Fetching upcoming events")
            EventUpcomingHandler(logger=cron_logger).fetch_events()
            cron_logger.info("Finished Fetching upcoming events")

    @app.cli.command()
    def run_daemon():
        """
        Runs a job that prints a message.
        Run it like: "RUN_MODE=JOB flask log-job"
        """
        logger = logging.getLogger("webapp.job")

        def upcoming_sensor():
            with app.app_context():
                EventUpcomingHandler(logger=logger).fetch_events()

        def cds_sensor():
            with app.app_context():
                CdsClient(logger=logger).get_recent_events()

        def tick():
            logger.info(
                'Tick! The time is: {} RUN_MODE: {}'.format(datetime.now(),
                                                            os.environ.get("RUN_MODE", "UNDEFINED")))

        scheduler = BlockingScheduler()
        scheduler.add_job(tick, 'interval', seconds=20)
        scheduler.add_job(upcoming_sensor, 'cron', hour=6, minute=30)
        scheduler.add_job(cds_sensor, 'cron', hour=6, minute=45)
        logger.debug('Press Ctrl+c to exit')

        try:
            scheduler.start()
        except (KeyboardInterrupt, SystemExit):
            logger.info("Daemon has been stopped")


