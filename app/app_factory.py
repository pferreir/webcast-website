import logging
import sys
from datetime import datetime
from importlib import import_module
from urllib import parse

import os
from flask import Flask
from flask import render_template
from flask_cors import CORS
from flask_uploads import configure_uploads
from werkzeug.contrib.fixers import ProxyFix

from app.api.v1 import API_VERSION_V1, api_v1_bp
from app.api.v1_1 import api_v1_1_bp, API_VERSION_V1_1
from app.api_loader import load_api_private_blueprints
from app.cli import initialize_cli
from app.common.authentication.cern_openid import load_cern_openid
from app.extensions import db, breadcrumbs, images, events_images, cache, migrate
from app.models.events import EventStatus
from app.utils.auth.blueprints import auth
from app.utils.auth.oidc import oauth
from app.utils.logger import setup_logs
from app.views.blueprints import all_blueprints

# from secret import config

logger = logging.getLogger("webapp.app_factory")


def create_app(
    config_filename: str = "config/config.py",
    static_url_path: str = "/static-files",
):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__, static_url_path=static_url_path)
    app.config.from_pyfile(config_filename)

    if app.config["USE_PROXY"]:
        app.wsgi_app = ProxyFix(app.wsgi_app)

    print("Initializing logs and cli...", end=" ")

    setup_logs(
        app,
        "webapp",
        to_remote=app.config.get("LOG_REMOTE_ENABLED", False),
        to_file=app.config.get("LOG_FILE_ENABLED", False),
    )
    initialize_cli(app)
    print("ok")

    print("Initializing CORS...", end=" ")
    CORS(app)
    print("ok")

    @app.template_filter("url_decode")
    def url_decode(string):
        return parse.unquote(string)

    _initialize_models(app)

    # Enable the CERN Oauth Authentication
    app.openid = load_cern_openid(app)

    # Initialize the database and the migrations
    setup_database(app)

    _initialize_views(app)

    _initialize_api(app)

    oauth.init_app(app)
    load_private_api_v1(app)
    app.register_blueprint(auth)

    # Initialize Flask-Breadcrumbs
    breadcrumbs.init_app(app=app)

    # Initialize Uploads
    configure_uploads(
        app,
        (
            images,
            events_images,
        ),
    )

    # Initialize Cache
    init_redis_cache(app)

    # add whitenoise
    # app.wsgi_app = WhiteNoise(app.wsgi_app, root='app/static-files/')

    @app.context_processor
    def inject_conf_var():
        """
        Injects all the application's global configuration variables

        :return: A dict with all the environment variables
        """

        return dict(
            #
            # Analytics
            #
            ANALYTICS_PIWIK_ID=app.config["ANALYTICS_PIWIK_ID"],
            IS_LOCAL_INSTALLATION=app.config["IS_LOCAL_INSTALLATION"],
            DEBUG=app.config["DEBUG"],
            LIVE_STATUS=EventStatus.LIVE,
            ARCHIVED_STATUS=EventStatus.ARCHIVED,
            VIDEO_PLAYER_URL=app.config["VIDEO_PLAYER_URL"],
            NOW=datetime.utcnow(),
        )

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("404.html"), 404

    logger.debug("Application finished loading.")

    @app.errorhandler(500)
    def uncaught_exception(error):
        exc_info = sys.exc_info()
        logger.error(f"Uncaught exception: {str(error)}", exc_info=exc_info)
        return "Uncaught exception"

    return app


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

        app.config["SQLALCHEMY_RECORD_QUERIES"] = False
        app.config["SQLALCHEMY_POOL_SIZE"] = int(
            app.config.get("SQLALCHEMY_POOL_SIZE", 5)
        )
        app.config["SQLALCHEMY_POOL_TIMEOUT"] = int(
            app.config.get("SQLALCHEMY_POOL_TIMEOUT", 10)
        )
        app.config["SQLALCHEMY_POOL_RECYCLE"] = int(
            app.config.get("SQLALCHEMY_POOL_RECYCLE", 120)
        )
        app.config["SQLALCHEMY_MAX_OVERFLOW"] = int(
            app.config.get("SQLALCHEMY_MAX_OVERFLOW", 3)
        )

    elif app.config.get("DB_ENGINE", None) == "mysql":
        service_name = app.config["DB_SERVICE_NAME"]
        print(f"Initializing Mysql Database on {service_name}...", end="")
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = "mysql+pymysql://{0}:{1}@{2}:{3}/{4}?charset=utf8mb4".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            _basedir, "webapp.db"
        )

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    db.app = app
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(app, db)
    print("ok")


def _initialize_models(app):
    """
    Load all the available models
    """

    db_models_imports = (
        "app.models.api",
        "app.models.events",
        "app.models.streams",
        "app.models.users",
        "app.models.settings",
    )

    with app.app_context():
        for module in db_models_imports:
            import_module(module)


def _initialize_views(application):
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """

    for blueprint in all_blueprints:
        import_module(blueprint.import_name)
        application.register_blueprint(blueprint)


def _initialize_api(application):
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    prefix = application.config["API_URL_PREFIX"]
    api_v1_version = API_VERSION_V1

    application.register_blueprint(
        api_v1_bp,
        url_prefix=f"{prefix}/v{api_v1_version}",
    )

    api_v1_1_version = API_VERSION_V1_1

    application.register_blueprint(
        api_v1_1_bp,
        url_prefix=f"{prefix}/v{api_v1_1_version}",
    )


def init_redis_cache(app):
    cache_config = {"CACHE_TYPE": "null"}
    if app.config.get("CACHE_ENABLE", False):
        print("Initializing Redis Caching... ", end="")
        cache_config = {
            "CACHE_TYPE": "redis",
            "CACHE_REDIS_HOST": app.config.get("CACHE_REDIS_HOST", None),
            "CACHE_REDIS_PASSWORD": app.config.get("CACHE_REDIS_PASSWORD", None),
            "CACHE_REDIS_PORT": app.config.get("CACHE_REDIS_PORT", None),
        }
    else:
        print("Caching disabled... ", end="")
    cache.init_app(app, config=cache_config)
    print("ok")


def load_private_api_v1(application):
    app_blueprints = load_api_private_blueprints()

    prefix = "/api"
    version = "v1"
    for blueprint in app_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/private/{version}",
        )
