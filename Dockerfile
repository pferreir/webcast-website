FROM centos/python-38-centos7

EXPOSE 8080

# Install yum packages with the ROOT user
USER root

ENV PYTHONPATH /opt/app-root/src
ENV FLASK_APP /opt/app-root/src/wsgi.py

# Set the permissions for the app-user user
RUN chgrp -R 0 /opt/app-root && chmod -R ug+rwx /opt/app-root

RUN pip install --upgrade pip

USER 1001

WORKDIR /opt/app-root/src

# Install pip requirements
COPY requirements.txt /opt/app-root/src/requirements.txt
RUN pip install -r requirements.txt

# Dev
COPY ./requirements.dev.txt /opt/app-root/src/requirements.dev.txt
RUN pip install -r requirements.dev.txt

# Copy the application files
COPY app /opt/app-root/src/app
COPY etc /opt/app-root/src/etc
COPY server /opt/app-root/src/server
COPY wsgi.py /opt/app-root/src/wsgi.py

CMD ["/opt/app-root/src/etc/entrypoint.sh"]
